#! /usr/bin/env python
# coding: utf-8

import os
import socket
import click
from factories import create_app
import flask
from flask.cli import FlaskGroup
from flask_migrate import Migrate, MigrateCommand

from factories import create_app, initialize_api, initialize_blueprints

from flask_script import Manager, prompt, prompt_pass, prompt_bool, prompt_choices

from multiprocessing import Process

FLASK_CONFIG = os.getenv("FLASK_CONFIG", "config.Config")
FLASK_HOST = os.getenv("FLASK_HOST", "0.0.0.0")
FLASK_PORT = os.getenv("FLASK_PORT", "5000")


# Required to map create_app function for FlaskGroup in cli
def make_app(*args, **kwargs):
    return create_app('stew', FLASK_CONFIG, *args, **kwargs)


app = create_app('stew', 'config.Config')

logger = app.logger

# Initializing script manager
manager = Manager(app)
# add assets command to it
# manager.add_command("assets", ManageAssets(app.assets))


migrate = Migrate(app, app.db)

manager.add_command('db', MigrateCommand)


@manager.command
def alembic(action, message=""):
    """ alembic integration using Flask-Alembic. Should provide us with more control over migrations """
    from stew import alembic as _alembic
    import stew.models
    from stew import app

    if action == "migrate":
        app.logger.info("Generating migration")
        _alembic.revision(message)
        app.logger.info("Migration complete")

    elif action == "upgrade":
        app.logger.info("Executing upgrade")
        _alembic.upgrade()
        app.logger.info("Upgrade complete")

    elif action == 'update':
        app.logger.info("Executing upgrade")
        _alembic.upgrade()
        _alembic.revision("Generating migration")
        _alembic.upgrade()
        app.logger.info("Upgrade complete")


@manager.command
def db_create():
    """  Intitiating sqlalchemy database create"""
    import stew.models
    from stew import db
    db.create_all()


@manager.command
def install_assets(name):
    """ load startup data for a particular module """

    from stew import db, models, logger, app
    from stew.tools import loader

    setup_dir = app.config.get("SETUP_DIR")  # Should be present in config

    filename = "%s.json" % name

    src = os.path.join(setup_dir, filename)
    logger.info(src)

    loader.load_data(models, db, src)


@manager.command
def create_user():
    """ Admin setup to create """
    app = flask.current_app
    with app.app_context():

        from stew.services.management import UserService

        print("Enter the following parameters to created a new user account")

        username = prompt("Username(*)")
        email = prompt("Email(*)")
        password = prompt_pass("Password(*)")
        retype_password = prompt_pass("Retype Password(*)")
        first_name = prompt("First Name")
        last_name = prompt("Last Name")
        name = "%s %s" % (first_name, last_name)
        is_staff = prompt_bool("Staff Account (yes/no)", default=False)
        is_enabled = prompt_bool("Enable Account (yes/no)", default=False)
        roles = prompt("Roles (separate by commas)")

        data = dict(username=username, email=email, password=password,
                    first_name=first_name, last_name=last_name, name=name.strip(" "),
                    is_enabled=is_enabled, is_staff=is_staff, roles=roles)

        if username and email and password and retype_password and (password == retype_password):
            user = UserService.register_staff(**data)

            if user:
                return user.id

        else:
            print("Account could not be created. Please supply all required (*) parameters")


@manager.command
def setup_app():
    """ load startup data for a particular module """
    app = flask.current_app

    app.logger.info("Relax and Enjoy the Ride....")
    # For Database Update
    actions = ['upgrade', 'migrate', 'upgrade']
    for i in actions:
        alembic(action=i)

    with app.app_context():
        from stew import db, models, logger
        from stew.tools import loader

        SETUP_DIR = app.config.get("SETUP_DIR")  # Should be present in config

        files = ['countries', 'states', 'payment_options',
                 'payment_statuses', 'menu_conditions', 'proteins', 'transaction_statuses',
                 'delivery_options', 'menu_sizes', 'menu_frequencies', 'menu_categories', 'order_statuses']

        for name in files:
            filename = "%s.json" % name

            src = os.path.join(SETUP_DIR, filename)
            logger.info(src)

            loader.load_data(models, db, src)


def run_in_parallel(*fns):
    proc = []
    for fn in fns:
        p = Process(target=fn)
        p.start()
        proc.append(p)
    for p in proc:
        p.join()


@manager.command
def runserver():
    """ Start the server"""
    from stew.views.public import www
    from stew.resources import endpoints
    from stew import api

    initialize_api(app, api)

    initialize_blueprints(app, www)

    port = int(os.environ.get('PORT', 5551))
    app.run(host='0.0.0.0', port=port)


@manager.command
def runadmin():
    """ Start the backend server server"""
    from stew.views.admin import control
    from stew import api

    initialize_api(app, api)

    initialize_blueprints(app, control)

    port = int(os.environ.get('PORT', 5560))
    app.run(host='0.0.0.0', port=port)


@manager.command
def start_apps():
    run_in_parallel(runserver, runadmin)


if __name__ == "__main__":
    manager.run()
