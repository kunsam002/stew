import os
from sendgrid.helpers.mail import Mail
from stew import sg


def send(subject, sender=None, recipients=[], html="", **kwargs):
    message = Mail(
        from_email=sender,
        to_emails=",".join(recipients),
        subject=subject,
        html_content=html)

    try:
        response = sg.send(message)
        return True, str(response.status_code)
    except Exception as e:
        print("Email Message sending to recipient %s failed with error------%s"(recipients, e))

    return False, 403
