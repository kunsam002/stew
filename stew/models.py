"""
models.py

@Author: Olukunle Ogunmokun
@Date: 3rd Oct, 2018

"""
import hashlib
import inspect as pyinspect
import sys
from datetime import datetime
import json
from flask.helpers import url_for
from sqlalchemy import func, event, desc, asc
from sqlalchemy import inspect, UniqueConstraint, desc
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property, Comparator
from sqlalchemy.orm import dynamic
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.orm.collections import InstrumentedList
from stew.core.utils import slugify, id_generator, token_generator, whole_number_format
from socket import gethostname, gethostbyname
from werkzeug.security import generate_password_hash, check_password_hash
from crud_factory.utils import convert_dict
from stew import db, logger, bcrypt, app, jwt


def slugify_from_name(context):
    """
    An sqlalchemy processor that works with default and on update
    field parameters to automatically slugify the name parameters in the model
    """
    return slugify(context.current_parameters['name'])


def generate_token_code(context):
    key = "%s:%s:%s" % (
    str(context.current_parameters["user_id"]), context.current_parameters["email"], str(datetime.utcnow()))
    return hashlib.md5(key.encode()).hexdigest()


def generate_email_hash(context):
    key = "%s:%s" % (context.current_parameters["email"], datetime.utcnow())
    return hashlib.md5(key.encode()).hexdigest()


class AppMixin(object):
    """ Mixin class for general attributes and functions """

    @property
    def pk(self):
        """ generic way to retrieve the identity of a model object """
        pk_name = inspect(self.__class__).primary_key[0].name
        return getattr(self, pk_name)

    @classmethod
    def primary_key(cls):
        """ generic way to retrieve the identity of a model object """
        pk_name = inspect(cls).primary_key[0].name
        return getattr(cls, pk_name)

    @declared_attr
    def date_created(cls):
        return db.Column(db.DateTime, default=datetime.utcnow, index=True)

    @declared_attr
    def last_updated(cls):
        return db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow, index=True)

    def as_dict(self, include_only=None, exclude=[], extras=["pk"], child=None, child_include=[],
                level=0):
        """ Retrieve all values of this model as a dictionary """
        data = inspect(self)

        level = level + 1

        include_only = (include_only if include_only else data.attrs.keys()) + extras

        exclude = exclude + ["is_deleted", "pk"]

        _dict = dict([(k, getattr(self, k)) for k in include_only if isinstance(getattr(self, k),
                                                                                (hybrid_property, InstrumentedAttribute,
                                                                                 InstrumentedList,
                                                                                 dynamic.AppenderMixin)) is False and k not in exclude])

        for key, obj in _dict.items():

            if isinstance(obj, db.Model) and level < 1:
                _dict[key] = obj.as_dict(level=level)

            if isinstance(obj, (list, tuple)):
                items = []
                for item in obj:
                    inspect_item = inspect(item)
                    items.append(
                        dict([(k, getattr(item, k)) for k in inspect_item.attrs.keys() + extras if
                              k not in exclude and hasattr(item, k)]))

                for item in items:
                    obj = item.get(child)
                    if obj:
                        item[child] = obj.as_dict(extras=child_include)
        return _dict

    def level_dict(self, include_only=None, exclude=["is_deleted"], extras=[], child=None, child_include=[]):
        data = self.as_dict(include_only=include_only, exclude=exclude, extras=extras, child=child,
                            child_include=child_include)
        for key, value in data.items():
            if type(data.get(key)) == dict:
                data.pop(key)
        return data

    def as_json_dict(self, include_only=None, exclude=["is_deleted"], extras=[], child=None, child_include=[],
                     indent=None):
        data = self.level_dict(include_only=include_only, exclude=exclude, extras=extras, child=child,
                               child_include=child_include)
        data = convert_dict(data, indent=indent)
        return json.loads(data)

    def as_full_json_dict(self, include_only=None, exclude=["is_deleted"], extras=[], child=None, child_include=[],
                          indent=None):
        data = self.as_dict(include_only=include_only, exclude=exclude, extras=extras, child=child,
                            child_include=child_include)
        data = convert_dict(data, indent=indent)
        return json.loads(data)


class UserMixin(AppMixin):
    """ Mixin class for Shop related attributes and functions """

    @declared_attr
    def user_id(cls):
        return db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)

    @declared_attr
    def user(cls):
        return db.relationship("User", foreign_keys=cls.user_id)


class Country(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=False, index=True, unique=True, primary_key=True)
    name = db.Column(db.String(200), nullable=False, index=True, unique=True)
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name)
    phone_code = db.Column(db.String)
    enabled = db.Column(db.Boolean, default=False)

    longitude = db.Column(db.String(200), index=True)
    latitude = db.Column(db.String(200), index=True)

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<Country %r>' % self.name


class State(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=False, index=True, unique=True, primary_key=True)
    name = db.Column(db.String(200), index=True)
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name)
    numeric_code = db.Column(db.String(200), index=True)
    enabled = db.Column(db.Boolean, default=False)

    delivery_charge = db.Column(db.Float, default=0.0, index=True, nullable=True)

    longitude = db.Column(db.String(200), index=True)
    latitude = db.Column(db.String(200), index=True)

    country_code = db.Column(db.String(200), db.ForeignKey('country.code'), nullable=False)
    country = db.relationship("Country", foreign_keys="State.country_code")

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<State %r>' % self.name


class User(AppMixin, db.Model):
    extra_fields = ["full_name", "approx_name", "name"]

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), unique=True)
    email = db.Column(db.String(200), unique=True)
    first_name = db.Column(db.String(200), nullable=True)
    last_name = db.Column(db.String(200), nullable=True)
    name = db.Column(db.String(300), nullable=False)
    gender = db.Column(db.String(200), nullable=True)  # Male or Female
    phone = db.Column(db.String(200), nullable=True)
    date_of_birth = db.Column(db.Date, nullable=True)
    password = db.Column(db.Text, unique=False)
    is_enabled = db.Column(db.Boolean, default=False)
    is_verified = db.Column(db.Boolean, default=False)
    is_staff = db.Column(db.Boolean, default=False)
    designation = db.Column(db.String(200), nullable=True)

    residential_address = db.Column(db.Text, nullable=True)

    roles = db.Column(db.Text)  # comma separated list of all roles a user is allowed

    login_count = db.Column(db.Integer, default=0, index=True)
    last_login_at = db.Column(db.DateTime, index=True)
    current_login_at = db.Column(db.DateTime, index=True)
    last_login_ip = db.Column(db.String(200), index=True)
    current_login_ip = db.Column(db.String(200), index=True)

    referral_code = db.Column(db.String(200), index=True)
    order_count = db.Column(db.Integer, default=0, index=True)

    @property
    def approx_name(self):
        _name = self.name.split(" ")
        _first = _name[0]

        _last = " %s." % _name[1][0].upper() if len(_name) > 1 else ""

        return "%s%s" % (_first, _last)

    @property
    def full_name(self):
        return self.name

    def get_id(self):
        """ For login manager """
        return self.id

    def update_last_login(self):
        if self.current_login_at is None and self.last_login_at is None:
            self.current_login_at = self.last_login_at = datetime.now()
            self.current_login_ip = self.last_login_ip = gethostbyname(gethostname())

        if self.current_login_at != self.last_login_at:
            self.last_login_at = self.current_login_at
            self.last_login_ip = self.current_login_ip
            self.current_login_at = datetime.now()
            self.current_login_ip = gethostbyname(gethostname())

        if self.last_login_at == self.current_login_at:
            self.current_login_at = datetime.now()
            self.current_login_ip = gethostbyname(gethostname())

        self.login_count += 1
        db.session.add(self)
        db.session.commit()

    def is_authenticated(self):
        """ For login manager """
        return True

    def get_auth_token(self):
        """ Returns the user's authentication token """
        key = "%s:%s" % (self.email, self.password)
        return hashlib.sha256(key.encode()).hexdigest()

    def get_token(self):
        data = {"id": self.id, "aid": self.is_staff, "rol": self.roles}

        token = jwt.encode(data)
        return token

    @property
    def auth_token(self):
        "retrieve the auth token for a user within the platform"
        return self.get_token()

    def is_active(self):
        """ Returns if the user is active or not. Overridden from UserMixin """
        return self.is_enabled

    def encrypt_password(self, password):
        """
        Generates a password from the plain string

        :param password: plain password string
        """

        self.password = generate_password_hash(password)

    def check_password(self, password):
        """
        Checks the given password against the saved password

        :param password: password to check
        :type password: string

        :returns True or False
        :rtype: bool

        """
        return check_password_hash(self.password, password)

    def set_password(self, new_password):
        """
        Sets a new password for the user

        :param new_password: the new password
        :type new_password: string
        """

        self.encrypt_password(new_password)
        db.session.add(self)
        db.session.commit()

    @property
    def ordered_recipes(self):
        return list(set([i.menu_id for i in OrderEntry.query.filter(OrderEntry.user_id == self.id).all()]))

    def __repr__(self):
        return '<User %r>' % self.name


class PasswordResetToken(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=True)
    email = db.Column(db.String(200), nullable=False)
    code = db.Column(db.String(200), unique=False, default=generate_token_code)
    is_expired = db.Column(db.Boolean, default=False)


class PaymentChannel(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=True, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), index=True)

    def __repr__(self):
        return '<PaymentChannel %r>' % self.name


class PaymentOption(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=True, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), index=True)

    def __repr__(self):
        return '<PaymentOption %r>' % self.name


class DeliveryOption(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=True, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), index=True)

    def __repr__(self):
        return '<DeliveryOption %r>' % self.name


class PaymentStatus(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=True, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), index=True)

    def __repr__(self):
        return '<PaymentStatus %r>' % self.name


class DeliveryStatus(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=True, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), index=True)

    def __repr__(self):
        return '<DeliveryStatus %r>' % self.name


class OrderStatus(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code"]

    code = db.Column(db.String(200), nullable=True, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), nullable=False, unique=True)
    description = db.Column(db.String(200), index=True)

    def __repr__(self):
        return '<OrderStatus %r>' % self.name


class MenuCategory(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=True, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), index=True)

    description = db.Column(db.String(200), index=True)

    is_special = db.Column(db.Boolean, default=False, index=True)
    enabled = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<MenuCategory %r>' % self.name


class MenuSize(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=True, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), index=True)
    description = db.Column(db.String(200), index=True)

    is_cooked = db.Column(db.Boolean, default=False)
    enabled = db.Column(db.Boolean, default=True)

    value = db.Column(db.Integer, index=True)
    cost_multiplier = db.Column(db.Float, default=1)
    protein_cost_multiplier = db.Column(db.Float, default=1)
    side_dish_cost_multiplier = db.Column(db.Float, default=1)

    def __repr__(self):
        return '<MenuSize %r>' % self.name


class MenuCondition(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=True, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), index=True)
    description = db.Column(db.String(200), index=True)

    def __repr__(self):
        return '<MenuCondition %r>' % self.name


class MenuFrequency(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=True, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), index=True)
    description = db.Column(db.String(200), index=True)

    value = db.Column(db.Integer, index=True)

    def __repr__(self):
        return '<MenuFrequency %r>' % self.name


class Image(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)
    alt_text = db.Column(db.String(200), unique=False)
    url = db.Column(db.String(200), unique=False)
    old_url = db.Column(db.String(200), unique=False)
    width = db.Column(db.Float)
    height = db.Column(db.Float)

    public_id = db.Column(db.String(200), nullable=True)

    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'), nullable=True, index=True)
    menu = db.relationship("Menu", foreign_keys="Image.menu_id")

    ingredient_id = db.Column(db.Integer, db.ForeignKey('ingredient.id'), nullable=True, index=True)
    ingredient = db.relationship("Ingredient", foreign_keys="Image.ingredient_id")

    protein_id = db.Column(db.Integer, db.ForeignKey('protein.id'), nullable=True, index=True)
    protein = db.relationship("Protein", foreign_keys="Image.protein_id")

    side_dish_id = db.Column(db.Integer, db.ForeignKey('side_dish.id'), nullable=True, index=True)
    side_dish = db.relationship("SideDish", foreign_keys="Image.side_dish_id")

    # Migration
    imagekit_migrated = db.Column(db.Boolean, default=False)
    width_scan = db.Column(db.Boolean, default=False)
    size_scan = db.Column(db.Boolean, default=False)
    imagekit_url = db.Column(db.String(200), unique=False)
    thumbnail = db.Column(db.String(200), unique=False)
    imagekit_id = db.Column(db.String(200), unique=False)
    imagekit_imagePath = db.Column(db.String(200), unique=False)

    def delete_file(self):
        """ Deletes the actual image file from disk """
        raise NotImplementedError()

    @property
    def cover_image(self):
        return self

    def __repr__(self):
        return '<Image %r>' % self.name


class Menu(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=True, unique=True, index=True)
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name, index=True)

    name = db.Column(db.String(200), index=True)
    caption = db.Column(db.String(300), index=True)
    description = db.Column(db.Text, index=True)
    _cover_image_id = db.Column(db.Integer)

    ingredient_ids = db.Column(db.Text, index=True)
    ingredients = db.relationship('Ingredient', secondary="menu_ingredients",
                                  backref=db.backref('menus', lazy='dynamic'))

    side_dish_ids = db.Column(db.Text, index=True)
    side_dishes = db.relationship('SideDish', secondary="menu_side_dishes",
                                  backref=db.backref('menus', lazy='dynamic'))

    menu_sizes = db.Column(db.Text, index=True)  # Sting list of all possible Menu list options for the menu
    available_sizes = db.relationship('MenuSize', secondary="available_menu_sizes",
                                      backref=db.backref('menus', lazy='dynamic'))

    menu_frequencies = db.Column(db.Text,
                                 index=True)  # String list of all possible Menu frequencies for the menu

    production_time = db.Column(db.Integer, index=True,
                                nullable=False)  # How long it takes to prepare the menu in minutes

    default_cost = db.Column(db.Float, default=1.0, index=True, nullable=False)
    uncooked_cost = db.Column(db.Float, default=1.0, index=True, nullable=True)

    on_deal = db.Column(db.Boolean, default=False, index=True)
    deal_start = db.Column(db.DateTime, nullable=True, index=True)
    deal_end = db.Column(db.DateTime, nullable=True, index=True)
    deal_deduction_multiplier = db.Column(db.Float, default=0.0)

    menu_category_code = db.Column(db.String(200), db.ForeignKey('menu_category.code'), nullable=False)
    menu_category = db.relationship("MenuCategory", foreign_keys="Menu.menu_category_code")

    is_featured = db.Column(db.Boolean, default=False, index=True)
    featured_position = db.Column(db.Integer, index=True)

    is_visible = db.Column(db.Boolean, default=False, index=True)
    is_special = db.Column(db.Boolean, default=False, index=True)
    is_vegetarian = db.Column(db.Boolean, default=False, index=True)

    support_cooked = db.Column(db.Boolean, default=False, index=True, nullable=True)
    support_uncooked = db.Column(db.Boolean, default=False, index=True, nullable=True)

    allow_subscription = db.Column(db.Boolean, default=False, index=True)
    allow_one_off = db.Column(db.Boolean, default=False, index=True)

    view_count = db.Column(db.Integer, index=True, default=0, nullable=True)

    def __repr__(self):
        return '<Menu %r>' % self.name

    @property
    def cover_image(self):
        if self._cover_image_id:
            return Image.query.get(self._cover_image_id)
        else:
            return Image.query.filter(Image.menu_id == self.id).first()

    @hybrid_property
    def cover_image_id(self):
        return self.cover_image.id if self.cover_image else None

    @cover_image_id.setter
    def cover_image_id(self, value):
        self._cover_image_id = value
        db.session.add(self)
        db.session.commit()

    @property
    def cover_image_url(self):
        return self.cover_image.url if self.cover_image else None

    @property
    def thumbnail(self):
        return self.cover_image.thumbnail if self.cover_image else None

    @property
    def images(self):
        return Image.query.filter(Image.menu_id == self.pk)

    @property
    def url_slug(self):
        return "%s_%s" % (self.slug, self.id)

    @property
    def url(self):
        return "%s/recipes/%s/%s/"%(app.config.get("CANONICAL_URL"),self.id,self.slug)

    @property
    def avg_rating(self):
        tot_rating = db.session.query(func.sum(MenuReview.rating)).filter(MenuReview.menu_id == self.id).scalar()
        tot_rating_qty = MenuReview.query.filter(MenuReview.menu_id == self.id).count()

        if tot_rating_qty < 1:
            return 0

        avg = tot_rating / tot_rating_qty

        return int(avg)


# Association table for Menu and Ingredient
menu_ingredients = db.Table('menu_ingredients',
                            db.Column('menu_id', db.Integer,
                                      db.ForeignKey('menu.id')),
                            db.Column('ingredient_id', db.Integer,
                                      db.ForeignKey('ingredient.id'))
                            )

# Association table for Menu and SideDish
menu_side_dishes = db.Table('menu_side_dishes',
                            db.Column('menu_id', db.Integer,
                                      db.ForeignKey('menu.id')),
                            db.Column('side_dish_id', db.Integer,
                                      db.ForeignKey('side_dish.id'))
                            )

# Association table for Menu and MenuSize
available_menu_sizes = db.Table('available_menu_sizes',
                                db.Column('menu_id', db.Integer,
                                          db.ForeignKey('menu.id')),
                                db.Column('menu_size_code', db.String(200),
                                          db.ForeignKey('menu_size.code'))
                                )


class Ingredient(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=True, unique=False, index=True)

    name = db.Column(db.String(200), index=True)
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name, index=True)

    description = db.Column(db.Text, index=True)
    preservation = db.Column(db.Text, index=True)
    _cover_image_id = db.Column(db.Integer)

    quantity = db.Column(db.Integer, index=True)
    price = db.Column(db.Float, default=1.0, index=True)
    expiration_date = db.Column(db.DateTime, default=datetime.utcnow, index=True)

    def __repr__(self):
        return '<Ingredient %r>' % self.name

    @property
    def cover_image(self):
        if self._cover_image_id:
            return Image.query.get(self._cover_image_id)
        else:
            return Image.query.filter(Image.ingredient_id == self.id).first()

    @hybrid_property
    def cover_image_id(self):
        return self.cover_image.id if self.cover_image else None

    @cover_image_id.setter
    def cover_image_id(self, value):
        self._cover_image_id = value
        db.session.add(self)
        db.session.commit()

    @property
    def images(self):
        return Image.query.filter(Image.ingredient_id == self.pk)


class Protein(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=True, unique=False, index=True)

    name = db.Column(db.String(200), index=True)
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name, index=True)

    description = db.Column(db.Text, index=True)
    _cover_image_id = db.Column(db.Integer)

    quantity = db.Column(db.Integer, index=True)
    price = db.Column(db.Float, default=1.0, index=True)
    expiration_date = db.Column(db.DateTime, default=datetime.utcnow, index=True)

    def __repr__(self):
        return '<Protein %r>' % self.name

    @property
    def cover_image(self):
        if self._cover_image_id:
            return Image.query.get(self._cover_image_id)
        else:
            return Image.query.filter(Image.protein_id == self.pk).first()

    @hybrid_property
    def cover_image_id(self):
        return self.cover_image.id if self.cover_image else None

    @cover_image_id.setter
    def cover_image_id(self, value):
        self._cover_image_id = value
        db.session.add(self)
        db.session.commit()

    @property
    def images(self):
        return Image.query.filter(Image.protein_id == self.pk)


class SideDish(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=True, unique=False, index=True)

    name = db.Column(db.String(200), index=True)
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name, index=True)

    description = db.Column(db.Text, index=True)
    _cover_image_id = db.Column(db.Integer)

    quantity = db.Column(db.Integer, index=True)
    price = db.Column(db.Float, default=1.0, index=True)
    expiration_date = db.Column(db.DateTime, default=datetime.utcnow, index=True)

    def __repr__(self):
        return '<Side Dish %r>' % self.name

    @property
    def cover_image(self):
        if self._cover_image_id:
            return Image.query.get(self._cover_image_id)
        else:
            return Image.query.filter(Image.side_dish_id == self.id).first()

    @hybrid_property
    def cover_image_id(self):
        return self.cover_image.id if self.cover_image else None

    @cover_image_id.setter
    def cover_image_id(self, value):
        self._cover_image_id = value
        db.session.add(self)
        db.session.commit()

    @property
    def images(self):
        return Image.query.filter(Image.side_dish_id == self.id)


class Like(db.Model, UserMixin, AppMixin):
    id = db.Column(db.Integer, primary_key=True)
    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'), nullable=False, index=True)
    menu = db.relationship("Menu", foreign_keys="Like.menu_id")

    def __repr__(self):
        return '<Like %r>' % self.name


class AdminMessage(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    email = db.Column(db.String(200), nullable=False)
    phone = db.Column(db.String(200), nullable=False)
    subject = db.Column(db.Text)
    is_read = db.Column(db.Boolean, default=False)
    user_read = db.Column(db.Boolean, default=False)
    has_parent = db.Column(db.Boolean, default=False)
    is_replied = db.Column(db.Boolean, default=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True, index=True)
    user = db.relationship("User", foreign_keys="AdminMessage.user_id")

    date_replied = db.Column(db.DateTime, nullable=True)
    body = db.Column(db.Text)  # for plain text messages

    def __repr__(self):
        return '<AdminMessage %r>' % self.name


class AdminMessageResponse(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)

    admin_message_id = db.Column(db.Integer, db.ForeignKey('admin_message.id'), nullable=False, index=True)
    admin_message = db.relationship("AdminMessage", foreign_keys="AdminMessageResponse.admin_message_id")

    body = db.Column(db.Text)  # for plain text messages

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)
    user = db.relationship("User", foreign_keys="AdminMessageResponse.user_id")

    def __repr__(self):
        return '<AdminMessageResponse %r>' % self.id


# Shopping carts belong to customers, not to users


class DeliveryAddress(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(200), nullable=True)
    phone = db.Column(db.String(200), nullable=True)
    address = db.Column(db.String(300), nullable=True)
    city = db.Column(db.String(200))
    description = db.Column(db.String(200))

    state_code = db.Column(db.String(200), db.ForeignKey('state.code'), nullable=True)
    state = db.relationship("State", foreign_keys="DeliveryAddress.state_code")

    country_code = db.Column(db.String(200), db.ForeignKey('country.code'), nullable=True)
    country = db.relationship("Country", foreign_keys="DeliveryAddress.country_code")

    def __repr__(self):
        return '<DeliveryAddress %r>' % self.name


class Cart(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=True)

    _total = db.Column(db.Float, default=0.0)
    discount = db.Column(db.Float, default=0.0)
    delivery_charge = db.Column(db.Float, default=0.0)

    phone = db.Column(db.String(200), nullable=True)
    email = db.Column(db.String(200), nullable=True)
    address = db.Column(db.String(500), nullable=True)
    city = db.Column(db.String(200), nullable=True)
    additional_info = db.Column(db.Text)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)
    user = db.relationship("User", foreign_keys="Cart.user_id")

    state_code = db.Column(db.String(200), db.ForeignKey('state.code'), nullable=True)
    state = db.relationship("State", foreign_keys="Cart.state_code")

    country_code = db.Column(db.String(200), db.ForeignKey('country.code'), nullable=True)
    country = db.relationship("Country", foreign_keys="Cart.country_code")

    payment_option_code = db.Column(db.String(200), db.ForeignKey('payment_option.code'), nullable=True)
    payment_option = db.relationship("PaymentOption", foreign_keys="Cart.payment_option_code")

    delivery_option_code = db.Column(db.String(200), db.ForeignKey('delivery_option.code'), nullable=True)
    delivery_option = db.relationship("DeliveryOption", foreign_keys="Cart.delivery_option_code")

    purchase_session_token = db.Column(db.String(100), nullable=True)  # used to id a purchase session on checkout
    transactions = db.relationship('Transaction', backref='cart', lazy='dynamic')
    cart_items = db.relationship('CartItem', cascade="all,delete-orphan", backref='cart', lazy='dynamic')
    quantity = db.Column(db.Integer, default=0)
    checkout_type = db.Column(db.String(200), nullable=True)

    def __repr__(self):
        return '<Cart %r>' % self.name

    @property
    def token(self):
        s = "StcTgeAnwY" + "_" + token_generator(size=4) + "_" + id_generator(size=6) + "_" + str(self.id)
        return s.lower()

    def item(self, variant_id):
        return CartItem.query.filter(CartItem.cart == self, CartItem.variant_id == variant_id).first()

    def items(self):
        return CartItem.query.filter(CartItem.cart == self).order_by(asc(CartItem.variant_id)).all()

    def items_count(self):
        return CartItem.query.filter(CartItem.cart == self).order_by(asc(CartItem.variant_id)).count()

    @property
    def cart_total(self):
        _tot = db.session.query(func.sum(CartItem.total)).filter(CartItem.cart_id == self.id).scalar()

        if not _tot:
            _tot = 0

        return _tot

    @property
    def cart_discount(self):
        discount = db.session.query(func.sum(CartItem.discount)).filter(CartItem.cart_id == self.id).scalar()

        if not discount:
            discount = 0

        self.discount = discount
        db.session.add(self)
        db.session.commit()

        return discount

    def calc_delivery(self):
        self.delivery_charge = self.state.delivery_charge if self.state else 0.0
        db.session.add(self)
        db.session.commit()
        return self.delivery_charge

    @property
    def total(self):
        dc = self.delivery_charge if self.delivery_charge else 0.0
        _total = (dc + self.cart_total) - self.cart_discount

        if self._total != _total:
            self._total = float(_total)
            db.session.add(self)
            db.session.commit()

        return _total

    @property
    def total_quantity(self):
        q = db.session.query(CartItem).filter(CartItem.cart_id == self.id).count()
        self.quantity = q
        db.session.add(self)
        db.session.commit()
        return q


class CartItem(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'), nullable=False)
    menu = db.relationship("Menu", foreign_keys="CartItem.menu_id")
    quantity = db.Column(db.Integer)

    cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)

    price = db.Column(db.Float)
    total = db.Column(db.Float)
    discount = db.Column(db.Float, default=0.0)

    is_special = db.Column(db.Boolean, default=False, index=True)
    is_cooked = db.Column(db.Boolean, default=False, index=True, nullable=True)
    is_uncooked = db.Column(db.Boolean, default=False, index=True, nullable=True)

    menu_size_code = db.Column(db.String, db.ForeignKey('menu_size.code'), nullable=False)
    menu_size = db.relationship("MenuSize", foreign_keys="CartItem.menu_size_code")

    menu_condition_code = db.Column(db.String, db.ForeignKey('menu_condition.code'), nullable=False)
    menu_condition = db.relationship("MenuCondition", foreign_keys="CartItem.menu_condition_code")

    def calculate_assumed_total(self, menu_size_code):
        """ Calculates the assumed total charge of this item """
        menu_size = MenuSize.query.get(menu_size_code)

        size_multiplier = protein_cost_multiplier = side_dish_cost_multiplier = 0

        if not menu_size:
            return 0

        menu_cost = self.menu.uncooked_cost if self.menu_condition_code == "uncooked" else self.menu.default_cost
        menu_cost = menu_cost if menu_cost else 0

        size_multiplier = menu_size.cost_multiplier if menu_size and menu_size.cost_multiplier else 0
        protein_cost_multiplier = menu_size.protein_cost_multiplier if menu_size and menu_size.protein_cost_multiplier else 0
        side_dish_cost_multiplier = menu_size.side_dish_cost_multiplier if menu_size and menu_size.side_dish_cost_multiplier else 0

        menu_total = size_multiplier * menu_cost

        extra_tot = 0
        for extra in CartItemExtra.query.filter(CartItemExtra.cart_item_id == self.id).all():
            protein_price = extra.protein.price if extra.protein else 0
            dish_price = extra.side_dish.price if extra.side_dish else 0

            protein_total = protein_cost_multiplier * protein_price

            side_dish_total = side_dish_cost_multiplier * dish_price

            _total = protein_total + side_dish_total

            extra_tot = extra_tot + _total

        total = menu_total + extra_tot

        return total

    def calculate_total(self):
        """ Calculates the total charge of this item """

        if not self.menu_size:
            return 0

        for extra in CartItemExtra.query.filter(CartItemExtra.cart_item_id == self.id).all():
            extra.calculate_total()

        size_multiplier = self.menu_size.cost_multiplier if self.menu_size.cost_multiplier else 0

        menu_cost = self.menu.uncooked_cost if self.menu_condition_code == "uncooked" else self.menu.default_cost
        menu_cost = menu_cost or 0

        extra_cost = db.session.query(func.sum(CartItemExtra.total)).filter(
            CartItemExtra.cart_item_id == self.id).scalar()
        extra_cost = extra_cost or 0

        menu_total = size_multiplier * menu_cost
        total = menu_total + extra_cost

        discount = self.discount if self.discount else 0
        if self.is_special:
            discount = 0.15 * float(total)
        else:
            discount = 0.10 * float(total)

        self.total = total
        self.discount = float(int(discount))

        db.session.add(self)
        db.session.commit()

        return self.total

    @property
    def extras(self):
        return CartItemExtra.query.filter(CartItemExtra.cart_item_id == self.id).all()

    def __repr__(self):
        return '<CartItem %r>' % self.id


class CartItemExtra(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)
    value = db.Column(db.String(200), unique=False)

    cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'), nullable=False)
    cart = db.relationship("Cart", foreign_keys="CartItemExtra.cart_id")

    cart_item_id = db.Column(db.Integer, db.ForeignKey('cart_item.id'), nullable=False)
    cart_item = db.relationship("CartItem", foreign_keys="CartItemExtra.cart_item_id")

    protein_id = db.Column(db.Integer, db.ForeignKey('protein.id'), nullable=True)
    protein = db.relationship("Protein", foreign_keys="CartItemExtra.protein_id")

    side_dish_id = db.Column(db.Integer, db.ForeignKey('side_dish.id'), nullable=True)
    side_dish = db.relationship("SideDish", foreign_keys="CartItemExtra.side_dish_id")

    menu_size_code = db.Column(db.String, db.ForeignKey('menu_size.code'), nullable=True)
    menu_size = db.relationship("MenuSize", foreign_keys="CartItemExtra.menu_size_code")

    total = db.Column(db.Float)

    def __repr__(self):
        return '<CartItemExtra %r>' % self.id

    def calculate_total(self):
        if not self.menu_size:
            self.total = 0
            db.session.add(self)
            db.session.commit()
            return 0

        menu_size = self.menu_size

        new_code = protein_cost_multiplier = side_dish_cost_multiplier = None

        if self.menu_size_code != self.cart_item.menu_size_code:
            new_code = self.cart_item.menu_size_code

        if new_code:
            menu_size = MenuSize.query.get(new_code)
            self.menu_size_code = new_code

        if new_code and menu_size in ["", None]:
            self.total = 0
            db.session.add(self)
            db.session.commit()
            return 0

        protein_cost_multiplier = menu_size.protein_cost_multiplier if menu_size and menu_size.protein_cost_multiplier else 0
        side_dish_cost_multiplier = menu_size.side_dish_cost_multiplier if menu_size and menu_size.side_dish_cost_multiplier else 0

        protein_price = self.protein.price if self.protein else 0
        dish_price = self.side_dish.price if self.side_dish else 0

        protein_total = protein_cost_multiplier * protein_price
        side_dish_total = side_dish_cost_multiplier * dish_price
        total = protein_total + side_dish_total

        self.total = total
        db.session.add(self)
        db.session.commit()
        return total


class Subscription(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=True)
    total = db.Column(db.Float, default=0.0)
    delivery_charge = db.Column(db.Float, default=0.0)
    phone = db.Column(db.String(200), nullable=True)
    email = db.Column(db.String(200), nullable=True)
    address = db.Column(db.String(500), nullable=True)
    city = db.Column(db.String(200), nullable=True)
    additional_info = db.Column(db.Text)

    state_code = db.Column(db.String(200), db.ForeignKey('state.code'), nullable=True)
    state = db.relationship("State", foreign_keys="Subscription.state_code")

    country_code = db.Column(db.String(200), db.ForeignKey('country.code'), nullable=True)
    country = db.relationship("Country", foreign_keys="Subscription.country_code")

    payment_option_code = db.Column(db.String(200), db.ForeignKey('payment_option.code'), nullable=True)
    payment_option = db.relationship("PaymentOption", foreign_keys="Subscription.payment_option_code")

    delivery_option_code = db.Column(db.String(200), db.ForeignKey('delivery_option.code'), nullable=True)
    delivery_option = db.relationship("DeliveryOption", foreign_keys="Subscription.delivery_option_code")

    purchase_session_token = db.Column(db.String(100), nullable=True)  # used to id a purchase session on checkout
    transactions = db.relationship('Transaction', backref='subscription', lazy='dynamic')
    subscription_items = db.relationship('SubscriptionItem', cascade="all,delete-orphan", backref='subscription',
                                         lazy='dynamic')
    quantity = db.Column(db.Integer, default=0)
    checkout_type = db.Column(db.String(200), nullable=True)

    menu_size_code = db.Column(db.String, db.ForeignKey('menu_size.code'), nullable=False)
    menu_size = db.relationship("MenuSize", foreign_keys="Subscription.menu_size_code")

    menu_frequency_code = db.Column(db.String, db.ForeignKey('menu_frequency.code'), nullable=False)
    menu_frequency = db.relationship("MenuFrequency", foreign_keys="Subscription.menu_frequency_code")

    def __repr__(self):
        return '<Subscription %r>' % self.name

    @property
    def token(self):
        s = "StcRgeBnw" + "_" + token_generator(size=4) + "_" + id_generator(size=6) + "_" + str(self.id)
        return s.lower()

    def item(self, menu_id):
        return SubscriptionItem.query.filter(SubscriptionItem.subscription_id == self.id,
                                             SubscriptionItem.variant_id == menu_id).first()

    def items(self):
        return SubscriptionItem.query.filter(SubscriptionItem.subscription_id == self.id).order_by(
            asc(SubscriptionItem.menu_id)).all()

    def items_count(self):
        return SubscriptionItem.query.filter(SubscriptionItem.subscription_id == self.id).order_by(
            asc(SubscriptionItem.menu_id)).count()

    @property
    def cart_total(self):
        return 0

    @property
    def total(self):
        dc = self.delivery_charge if self.delivery_charge else 0.0
        return dc + self.cart_total

    @property
    def total_quantity(self):
        q = db.session.query(SubscriptionItem).filter(SubscriptionItem.cart_id == self.id).count()
        self.quantity = q
        db.session.add(self)
        db.session.commit()
        return q


class SubscriptionItem(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'), nullable=False)
    menu = db.relationship("Menu", foreign_keys="SubscriptionItem.menu_id")

    quantity = db.Column(db.Integer)
    subscription_id = db.Column(db.Integer, db.ForeignKey('subscription.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    price = db.Column(db.Float)
    total = db.Column(db.Float)

    def calculate_total(self):
        """Calculates the total charge of this item """
        size_multiplier = self.menu_size.cost_multiplier
        default_cost = self.menu.default_cost
        self.total = size_multiplier * default_cost
        db.session.add(self)
        db.session.commit()
        return self.total

    def __repr__(self):
        return '<SubscriptionItem %r>' % self.id


class SubscriptionItemExtra(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)
    value = db.Column(db.String(200), unique=False)

    subscription_item_id = db.Column(db.Integer, db.ForeignKey('subscription_item.id'), nullable=False)
    subscription_item = db.relationship("SubscriptionItem", foreign_keys="SubscriptionItemExtra.subscription_item_id")

    protein_id = db.Column(db.Integer, db.ForeignKey('protein.id'), nullable=True)
    protein = db.relationship("Protein", foreign_keys="SubscriptionItemExtra.protein_id")

    side_dish_id = db.Column(db.Integer, db.ForeignKey('side_dish.id'), nullable=True)
    side_dish = db.relationship("SideDish", foreign_keys="SubscriptionItemExtra.side_dish_id")

    def __repr__(self):
        return '<SubscriptionItemExtra %r>' % self.id


class Order(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["user", "delivery_status", "delivery_option", "state", "order_status", "entries", "total",
                            "checkout_type", "verified_status", "amount", "discount", "first_name", "last_name",
                            "shipment_state", "shipment_city", "city", "payment_option", "email", "bob_commission"]

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(200), nullable=True)
    last_name = db.Column(db.String(200), nullable=True)
    name = db.Column(db.String(300), nullable=True)
    discount_applied = db.Column(db.Float, default=0.0)
    amount = db.Column(db.Float, default=0.0)
    shipping_charge = db.Column(db.Float, default=0.0)
    code = db.Column(db.String(200), nullable=False)
    message = db.Column(db.Text, nullable=True)
    verified_status = db.Column(db.Boolean, default=False)  # considering either verified or not

    payment_option_code = db.Column(db.String(200), db.ForeignKey('payment_option.code'), nullable=False)
    payment_option = db.relationship("PaymentOption", foreign_keys="Order.payment_option_code")

    payment_status_code = db.Column(db.String(200), db.ForeignKey('payment_status.code'), nullable=True)
    payment_status = db.relationship("PaymentStatus", foreign_keys="Order.payment_status_code")

    delivery_option_code = db.Column(db.String(200), db.ForeignKey('delivery_option.code'), nullable=True)
    delivery_option = db.relationship("DeliveryOption", foreign_keys="Order.delivery_option_code")

    delivery_status_code = db.Column(db.String(200), db.ForeignKey('delivery_status.code'), nullable=True)
    delivery_status = db.relationship("DeliveryStatus", foreign_keys="Order.delivery_status_code")

    order_status_code = db.Column(db.String(200), db.ForeignKey('order_status.code'), nullable=False)
    order_status = db.relationship("OrderStatus", foreign_keys="Order.order_status_code")

    entries = db.relationship("OrderEntry", cascade="all,delete-orphan", backref="order", lazy="dynamic")
    transactions = db.relationship('Transaction', backref="order_", lazy='dynamic')
    # Adding address information to the order
    email = db.Column(db.String(200), nullable=False)
    phone = db.Column(db.String(200), nullable=False)
    address = db.Column(db.String(400), nullable=False)
    address_line2 = db.Column(db.String(200), nullable=True)
    city = db.Column(db.String(200), nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    user = db.relationship("User", foreign_keys="Order.user_id")

    state_code = db.Column(db.String(200), db.ForeignKey('state.code'), nullable=False)
    state = db.relationship("State", foreign_keys="Order.state_code")

    country_code = db.Column(db.String(200), db.ForeignKey('country.code'), nullable=False)
    country = db.relationship("Country", foreign_keys="Order.country_code")

    is_vegetarian = db.Column(db.Boolean, default=False)

    auto_set_shipment = db.Column(db.Boolean, default=False)
    shipment_name = db.Column(db.String(200), nullable=True)
    shipment_phone = db.Column(db.String(200), nullable=True)
    shipment_address = db.Column(db.String(300), nullable=True)
    shipment_city = db.Column(db.String(200))
    shipment_description = db.Column(db.String(200))
    shipment_state_code = db.Column(db.String(200), nullable=True)
    shipment_country_code = db.Column(db.String(200), nullable=True)
    checkout_type = db.Column(db.String(200), nullable=True)

    rave_transaction_id = db.Column(db.Integer, db.ForeignKey(
        'rave_transaction.id'), nullable=True)
    rave_transaction = db.relationship("RaveTransaction", foreign_keys="Order.rave_transaction_id")

    __table_args__ = (
        UniqueConstraint('user_id', 'code', name='_user_order_uc'),
    )

    def __repr__(self):
        return '<Order %r>' % self.code

    @property
    def full_name(self):
        _first = self.first_name
        _last = " %s" % self.last_name if self.last_name else ""

        return "%s%s" % (_first, _last)

    @property
    def total(self):
        return self.amount if self.amount else 0.0

    @property
    def w_total(self):
        return whole_number_format(self.amount)

    @property
    def shipping_fee(self):
        return whole_number_format(self.shipping_charge)

    @property
    def total_quantity(self):
        return db.session.query(func.sum(OrderEntry.quantity)).filter(OrderEntry.order_id == self.id).scalar()

    @property
    def total_entries(self):
        return self.entries.count()

    def cal_bob_commission(self):
        commission = db.session.query(func.sum(OrderEntry.bob_commission)).filter(
            OrderEntry.order_id == self.id).scalar()
        if not commission:
            commission = 0
        self.bob_commission = commission
        db.session.add(self)
        db.session.commit()
        return commission


class OrderEntry(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'), nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship("User", foreign_keys="OrderEntry.user_id")

    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'), nullable=False)
    menu = db.relationship("Menu", foreign_keys="OrderEntry.menu_id")

    is_special = db.Column(db.Boolean, default=False, index=True)
    is_cooked = db.Column(db.Boolean, default=False, index=True, nullable=True)
    is_uncooked = db.Column(db.Boolean, default=False, index=True, nullable=True)

    menu_size_code = db.Column(db.String, db.ForeignKey('menu_size.code'), nullable=False)
    menu_size = db.relationship("MenuSize", foreign_keys="OrderEntry.menu_size_code")

    menu_condition_code = db.Column(db.String, db.ForeignKey('menu_condition.code'), nullable=False)
    menu_condition = db.relationship("MenuCondition", foreign_keys="OrderEntry.menu_condition_code")

    quantity = db.Column(db.Integer)
    price = db.Column(db.Float)
    total = db.Column(db.Float)
    discount = db.Column(db.Float, default=0.0)

    order_status_code = db.Column(db.String(200), db.ForeignKey('order_status.code'), nullable=True)
    order_status = db.relationship("OrderStatus", foreign_keys="OrderEntry.order_status_code")

    date_cancelled = db.Column(db.Date, nullable=True)
    date_cust_cancelled = db.Column(db.Date, nullable=True)
    date_delivered = db.Column(db.Date, nullable=True)
    date_processed = db.Column(db.Date, nullable=True)
    date_returned = db.Column(db.Date, nullable=True)
    is_in_house = db.Column(db.Boolean, default=False)
    affiliate_price = db.Column(db.Float, default=0.0)

    def __repr__(self):
        return '<OrderEntry %r>' % self.id

    # @property
    # def total(self):
    #     """Calculates the total charge of this item """
    #     qty = self.quantity
    #     tot = qty * self.price
    #     if self.is_in_house:
    #         self.bob_commission = 0.1 * tot
    #         db.session.add(self)
    #         db.session.commit()
    #     elif self.affiliate_price and self.affiliate_price > 0:
    #         aff_tot = qty * self.affiliate_price
    #         self.bob_commission = tot - aff_tot
    #         db.session.add(self)
    #         db.session.commit()
    #     return tot

    # @property
    # def w_total(self):
    #     """Calculates the total charge of this item """
    #     return whole_number_format(self.quantity * self.price)
    #
    # @property
    # def w_price(self):
    #     """Calculates the total charge of this item """
    #     return whole_number_format(self.price)

    # @property
    # def product(self):
    #     return self.variant.product

    @property
    def user(self):
        return User.query.get(self.user_id)

    @property
    def cover_image(self):
        return self.menu.cover_image

    @property
    def name(self):
        return self.menu.name


    @property
    def url(self):
        return url_for('.recipe',id=self.menu_id,slug=self.menu.slug)

    @property
    def image_url(self):
        return self.menu.cover_image_url

    @property
    def qty(self):
        return self.quantity

    @property
    def extras(self):
        return OrderEntryExtra.query.filter(OrderEntryExtra.order_entry_id == self.id).all()


class OrderEntryExtra(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)
    value = db.Column(db.String(200), unique=False)

    order_entry_id = db.Column(db.Integer, db.ForeignKey('order_entry.id'), nullable=False)
    order_entry = db.relationship("OrderEntry", foreign_keys="OrderEntryExtra.order_entry_id")

    protein_id = db.Column(db.Integer, db.ForeignKey('protein.id'), nullable=True)
    protein = db.relationship("Protein", foreign_keys="OrderEntryExtra.protein_id")

    side_dish_id = db.Column(db.Integer, db.ForeignKey('side_dish.id'), nullable=True)
    side_dish = db.relationship("SideDish", foreign_keys="OrderEntryExtra.side_dish_id")


class TransactionStatus(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["name", "code", "message"]

    code = db.Column(db.String(200), nullable=False, unique=True, primary_key=True)  # Principal code for a tag
    name = db.Column(db.String(200), nullable=False)
    message = db.Column(db.String(200))

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<TransactionStatus %r>' % self.name

        # # table arguments to extend the behaviour of our classes
        # __table_args__ = (
        #     UniqueConstraint('gateway', 'code', name='_transaction_status_code'),
        # )


class Transaction(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cart_id = db.Column(db.Integer, db.ForeignKey('cart.id', ondelete='SET NULL'), nullable=True)
    subscription_id = db.Column(db.Integer, db.ForeignKey('subscription.id'), nullable=True)
    code = db.Column(db.String(200), nullable=False)
    name = db.Column(db.String(300), nullable=True)
    description = db.Column(db.Text)

    payment_option_code = db.Column(db.String(200), db.ForeignKey('payment_option.code'), nullable=False)
    payment_option = db.relationship("PaymentOption", foreign_keys="Transaction.payment_option_code")

    transaction_status_code = db.Column(db.String(200), db.ForeignKey('transaction_status.code'), nullable=False)
    transaction_status = db.relationship("TransactionStatus", foreign_keys="Transaction.transaction_status_code")

    order_id = db.Column(db.Integer, db.ForeignKey('order.id'), nullable=True)
    order = db.relationship('Order', foreign_keys='Transaction.order_id')

    amount = db.Column(db.Float)
    discount_applied = db.Column(db.Float, default=0.0)
    shipping_charge = db.Column(db.Float, default=0.0)

    phone = db.Column(db.String(200), nullable=True)
    email = db.Column(db.String(200), nullable=True)
    address = db.Column(db.String(400), nullable=True)
    city = db.Column(db.String(200), nullable=True)

    state_code = db.Column(db.String(200), db.ForeignKey('state.code'), nullable=True)
    state = db.relationship("State", foreign_keys="Transaction.state_code")

    country_code = db.Column(db.String(200), db.ForeignKey('country.code'), nullable=True)
    country = db.relationship("Country", foreign_keys="Transaction.country_code")

    auto_set_shipment = db.Column(db.Boolean, default=False)
    shipment_name = db.Column(db.String(200), nullable=True)
    shipment_phone = db.Column(db.String(200), nullable=True)
    shipment_address = db.Column(db.String(300), nullable=True)
    shipment_city = db.Column(db.String(200))
    shipment_description = db.Column(db.String(200))
    shipment_state_code = db.Column(db.String(200), nullable=True)
    shipment_country_code = db.Column(db.String(200), nullable=True)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    user = db.relationship("User", foreign_keys="Transaction.user_id")

    checkout_type = db.Column(db.String(200), nullable=True)

    rave_transaction_id = db.Column(db.Integer, db.ForeignKey(
        'rave_transaction.id'), nullable=True)
    rave_transaction = db.relationship("RaveTransaction", foreign_keys="Transaction.rave_transaction_id")

    def __repr__(self):
        return '<Transaction %r>' % self.id

    @property
    def amount_in_string(self):
        return str(int(self.amount))

    @property
    def response_url(self):
        url = "%s%s" % (app.config.get("CANONICAL_URL"), url_for('frontend_bp.vbv_confirmation', code=self.code))
        return url

    __table_args__ = (
        UniqueConstraint('user_id', 'code', name='_user_code_uc'),
    )


class TransactionEntry(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)

    transaction_id = db.Column(db.Integer, db.ForeignKey('transaction.id'), nullable=False)
    transaction = db.relationship("Transaction", foreign_keys="TransactionEntry.transaction_id")

    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'), nullable=False)
    menu = db.relationship("Menu", foreign_keys="TransactionEntry.menu_id")

    is_special = db.Column(db.Boolean, default=False, index=True)
    is_cooked = db.Column(db.Boolean, default=False, index=True, nullable=True)
    is_uncooked = db.Column(db.Boolean, default=False, index=True, nullable=True)

    menu_size_code = db.Column(db.String, db.ForeignKey('menu_size.code'), nullable=False)
    menu_size = db.relationship("MenuSize", foreign_keys="TransactionEntry.menu_size_code")

    menu_condition_code = db.Column(db.String, db.ForeignKey('menu_condition.code'), nullable=False)
    menu_condition = db.relationship("MenuCondition", foreign_keys="TransactionEntry.menu_condition_code")

    quantity = db.Column(db.Integer)
    price = db.Column(db.Float)
    total = db.Column(db.Float)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    user = db.relationship("User", foreign_keys="TransactionEntry.user_id")

    def __repr__(self):
        return '<TransactionEntry %r>' % self.id

    @property
    def product(self):
        return self.variant.product

    @property
    def cover_image(self):
        return self.variant.product.cover_image

    # @hybrid_property
    # def total(self):
    #     """Calculates the total charge of this item """
    #     return self.quantity * self.price

    @property
    def extras(self):
        return TransactionEntryExtra.query.filter(TransactionEntryExtra.transaction_entry_id == self.id).all()


class TransactionEntryExtra(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)
    value = db.Column(db.String(200), unique=False)

    transaction_entry_id = db.Column(db.Integer, db.ForeignKey('transaction_entry.id'), nullable=False)
    transaction_entry = db.relationship("TransactionEntry", foreign_keys="TransactionEntryExtra.transaction_entry_id")

    protein_id = db.Column(db.Integer, db.ForeignKey('protein.id'), nullable=True)
    protein = db.relationship("Protein", foreign_keys="TransactionEntryExtra.protein_id")

    side_dish_id = db.Column(db.Integer, db.ForeignKey('side_dish.id'), nullable=True)
    side_dish = db.relationship("SideDish", foreign_keys="TransactionEntryExtra.side_dish_id")

    def __repr__(self):
        return '<TransactionEntryExtra %r>' % self.id


class Currency(AppMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["code", "name", "enabled", "symbol", "payment_code"]
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(200), nullable=False, index=True, unique=True)
    name = db.Column(db.String(200))
    enabled = db.Column(db.Boolean, default=False)
    symbol = db.Column(db.String(200))
    payment_code = db.Column(db.String(200))

    def __unicode__(self):
        return "%s (%s)" % (self.name.title(), self.code)

    def __repr__(self):
        return '<Currency %r>' % self.name


class RecurrentCard(UserMixin, db.Model):
    __searchable__ = True

    __include_in_index__ = ["token", "mask", "brand", "exp_month", "exp_year"]

    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(200), nullable=False)
    mask = db.Column(db.String(300), nullable=True)
    brand = db.Column(db.String(300), nullable=True)
    exp_month = db.Column(db.Integer, nullable=True)
    exp_year = db.Column(db.Integer, nullable=True)

    def __repr__(self):
        return '<RecurrentCard %r>' % self.name


class RaveTransaction(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    embed_token = db.Column(db.String(200), nullable=False)
    user_token = db.Column(db.String(200), nullable=False)
    amount = db.Column(db.Float)
    appfee = db.Column(db.Float)
    charged_amount = db.Column(db.Float)
    narration = db.Column(db.String(200))
    orderRef = db.Column(db.String(200), nullable=False)
    txRef = db.Column(db.String(200), nullable=False)
    chargeResponseCode = db.Column(db.String(200), nullable=False)
    chargeResponseMessage = db.Column(db.String(200), nullable=False)
    merchantbearsfee = db.Column(db.String(200), nullable=False)
    paymentId = db.Column(db.String(200), nullable=False)
    paymentType = db.Column(db.String(200), nullable=False)
    status = db.Column(db.String(200), nullable=False)
    vbvrespcode = db.Column(db.String(200), nullable=False)
    vbvrespmessage = db.Column(db.String(200), nullable=False)
    auth_model_used = db.Column(db.String(200), nullable=False)
    charge_type = db.Column(db.String(200), nullable=False)
    channel = db.Column(db.String(200), nullable=False)
    cycle = db.Column(db.String(200), nullable=False)
    device_fingerprint = db.Column(db.String(200), nullable=False)
    flwRef = db.Column(db.String(200), nullable=False)
    paymentId = db.Column(db.String(200), nullable=False)
    paymentType = db.Column(db.String(200), nullable=False)
    vbvrespcode = db.Column(db.String(200), nullable=False)
    vbvrespmessage = db.Column(db.String(200), nullable=False)

    def __repr__(self):
        return '<RaveTransaction %r>' % self.id


class MenuReview(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)

    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'), nullable=True)
    menu = db.relationship("Menu", foreign_keys="MenuReview.menu_id")

    subject = db.Column(db.String(200), nullable=False)
    rating = db.Column(db.Integer, nullable=False)
    message = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return '<MenuReview %r>' % self.id
