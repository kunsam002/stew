"""
utils.py

@Author:    Olukunle Ogunmokun
@Date:      10th Dec, 2018
@Time:      3:42 PM

This module contains a number of utility functions useful throughout our application.
No references are made to specific models or resources. As a result, they are useful with or
without the application context.
"""

from user_agents import parse
import requests
import os
import base64
import hashlib
from urllib import parse
import math
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives import padding
from cryptography.hazmat.backends import default_backend
import bcrypt
import time
from reportlab.lib.units import mm
from reportlab.graphics.barcode import *
from reportlab.graphics.shapes import Drawing, String
import pyaes
import xmltodict
import dicttoxml
from math import floor, ceil
from crud_factory.utils import *


def detect_user_agent(ua_string):
    """
    Detects what kind of device is being used to access the server
    based on the user agent

    :param ua_string: The user agent string to parse

    :returns: user agent object
    """

    ua = parse(ua_string)

    return ua


def detect_user_device(ua_string):
    """ returns which device is used in reaching the application. 'm' for mobile, 'd' for desktop and 't' for tablet """

    ua = detect_user_agent(ua_string)

    device = "d"

    if ua.is_tablet:
        device = "t"

    if ua.is_mobile:
        device = "m"

    if ua.is_pc:
        device = "d"

    return device


def download_file(url, dest, filename):
    """
    Downloads a file from a url into a given destination
    and returns the location when it's done

    :param url: url to downlaod from
    :param dest: destination folder
    :param filename: filename to save the downloaded file as

    """

    r = requests.get(url)
    path = os.path.join(dest, filename)
    with open(path) as doc:
        doc.write(r.content)
    doc.close()
    return path


def next_working_day(start_date, days):
    """ calculate the next working day from the start date over a period in days by removing all weekends between and appending them to the date """
    end = int(math.ceil(days + 1))
    days_involved = [(start_date + timedelta(days=x)).isoweekday() for x in range(1, end)]

    weekends = [x for x in days_involved if x in [6, 7]]
    off_days = len(weekends)
    days_without_weekends = start_date + timedelta(days=math.ceil(days))
    days_with_weekends = start_date + timedelta(days=math.ceil(days + off_days))

    if off_days == 0:
        return days_with_weekends
    else:
        return next_working_day(days_without_weekends, (days_with_weekends - days_without_weekends).days)


class BarcodeDrawing(Drawing):
    def __init__(self, code, text_value, width, height, humanReadable, *args, **kwargs):
        _barcode = createBarcodeDrawing(code, value=text_value, barWidth=width, barHeight=height * mm,
                                        humanReadable=humanReadable)
        Drawing.__init__(self, _barcode.width, _barcode.height, *args, **kwargs)
        self.add(_barcode, name='barcode')


def generate_barcode(code, code_format='Code128', barWidth=1, barHeight=12, humanReadable=False):
    """ New barcode generator"""
    drawing = BarcodeDrawing(code_format, code, barWidth, barHeight, humanReadable)
    drawing.resized(lpad=-5, rpad=-5)  # resize the drawing to remove all padding
    bc = drawing.asString('png')
    return base64.b64encode(bc)


def generate_key(bytes=16, rounds=50, prefix=None, safe=False):
    """

    :param bytes: how long the generated key should be in bytes
    :param rounds: number of time s the algorithm should run
    :param prefix: add a prefix if you wish to identify generated key
    :return: key
    :rtype: String
    """
    s = token_generator()
    key = base64.b64encode(bcrypt.kdf(password=s, salt=str(datetime.now()), desired_key_bytes=bytes, rounds=rounds))
    if safe:
        key = base64.urlsafe_b64encode(
            bcrypt.kdf(password=s, salt=str(datetime.now()), desired_key_bytes=bytes, rounds=rounds))
    key = key.strip('==')
    if prefix:
        key = "%s_%s" % (str(prefix), key)

    return key


def to_boolean(value):
    """
    Determine Falsy values for boolean.
    :param value: value representation of false
    :return:
    """

    #  lowercase the value if it comes as a string
    if isinstance(value, (str)):
        value = value.lower()

    if value in ['false', 'f', 'n', 'no', None, False]:
        return False

    return bool(value)


def encrypt_3des(des_key, text):
    """
        Encrypt the specified value using the 3DES symmetric encryption algorithm
        :param des_key: encryption key
        :param text: parameter to encrypt
        :returns cipher_text: 3DES encrypted values
    """

    padder = padding.PKCS7(algorithms.TripleDES.block_size).padder()
    padded_text = padder.update(text) + padder.finalize()

    cipher = Cipher(algorithms.TripleDES(des_key), mode=modes.ECB(), backend=default_backend())
    encryptor = cipher.encryptor()
    cipher_text = encryptor.update(padded_text) + encryptor.finalize()

    return cipher_text


def decrypt_3des(des_key, cipher_text):
    """
        Decrypt the specified value using the 3DES symmetric decryption algorithm
        :param cipher_text: parameter to decrypt
        :param des_key: decryption key
        :returns u: plain text value
    """

    cipher = Cipher(algorithms.TripleDES(des_key), modes.ECB(), backend=default_backend())
    decryptor = cipher.decryptor()
    padded_text = decryptor.update(cipher_text) + decryptor.finalize()

    unpadder = padding.PKCS7(algorithms.TripleDES.block_size).unpadder()
    text = unpadder.update(padded_text) + unpadder.finalize()

    return text


def encrypt_data(des_key, data):
    """ encrypt the data sent in. Will return the 3des version of the dictionary """

    print(json.dumps(data, indent=2))
    des_key = build_3des_key(des_key)
    encrypted_data = dict()

    for k, v in data.items():
        encrypted_data[k] = base64.b64encode(encrypt_3des(des_key, str(v)))

    return encrypted_data


def decrypt_data(des_key, data):
    """ decrypt the data sent in. Will return the plain version of the dictionary """

    print(json.dumps(data, indent=2))
    des_key = build_3des_key(des_key)
    decrypted_data = dict()

    for k, v in data.items():
        decrypted_data[k] = decrypt_3des(des_key, base64.b64decode(str(v)))

    return decrypted_data


def build_3des_key(key):
    """ build the key for 3des using md5 digest"""

    m = hashlib.md5()
    m.update(key)
    des_key = m.digest()

    return des_key


def build_aes_key(key):
    """build an aes key of 16/24/32bytes, defaults to 16byte"""

    m = hashlib.md5()
    m.update(key)
    aes_key = m.hexdigest()

    return aes_key


def encrypt_aes(aes_key, text):
    """
        Encrypt the specified value using the 3DES symmetric encryption algorithm
        :param aes_key: encryption key
        :param text: parameter to encrypt
        :returns cipher_text: AES encrypted values
    """

    cipher = Cipher(algorithms.AES(aes_key), mode=modes.CTR(), backend=default_backend())
    encryptor = cipher.encryptor()
    cipher_text = encryptor.update(text) + encryptor.finalize()

    return cipher_text


def encrypt_pyaes(aes_key, text):
    """encrypt ciphertext using ctr mode of the pyaes library"""

    aes = pyaes.AESModeOfOperationCTR(aes_key)
    ciphertext = aes.encrypt(text)

    return ciphertext


def decrypt_pyaes(aes_key, text):
    """decrypt ciphertext using ctr mode of the pyaes library"""

    aes = pyaes.AESModeOfOperationCTR(aes_key)
    plaintext = aes.decrypt(text)

    return plaintext


def encrypt_data_pyaes(key, data):
    """ encrypt the data sent in. Will return the aes version of the dictionary """

    print(json.dumps(data, indent=2))
    aes_key = build_aes_key(key)
    print(aes_key, 'enc key')
    encrypted_data = dict()

    for k, v in data.items():
        encrypted_data[k] = base64.b64encode(encrypt_pyaes(aes_key, str(v).encode('utf-8')))

    return encrypted_data


def decrypt_data_pyaes(key, data):
    """ decrypt the data sent in. Will return the plain version of the dictionary """

    print(json.dumps(data, indent=2))
    aes_key = build_aes_key(key)
    print(aes_key, 'dec key')
    decrypted_data = dict()

    for k, v in data.items():
        decrypted_data[k] = decrypt_pyaes(aes_key, base64.b64decode(str(v)))

    return decrypted_data


def convert_xml_to_dict(xml_string, custom_root=None):
    """ convert xml string into a dictionary.
        custom_root:
    """
    data = xmltodict.parse(str(xml_string))
    if custom_root:
        return data.get(custom_root, None)

    return data


def prepare_dict_item(item):
    """ Determine the tag name to use for an object item within an xml list """
    print(item)
    itername = item.pop("itername", None)
    print(itername)

    return itername if itername else "Item"


def convert_dict_to_xml(data, root=True, custom_root='Response', item_func=None):
    """
    Convert a dict into an xml dictionary. This is acheived by first converting into json.
    root: The root xml to insert the data into.
    case: Case sensitivity of data keys
    """
    if not item_func:
        item_func = lambda x: data.pop(x, [{}])[0].pop("itername", "Item")
    xml_data = dicttoxml.dicttoxml(data, root=root, custom_root=custom_root, attr_type=False, item_func=item_func)
    return xml_data


def convert_file_base64(source_file):
    """convert a file to its base64 equivalent"""

    filename = source_file.split('/')[-1]
    ext = filename.split('.')[-1]
    try:
        with open(source_file, "rb") as a_file:
            encoded_string = base64.b64encode(a_file.read()).decode("ascii")
    except Exception as e:
        print(e.message)
        return None
    return {'filename': filename, 'stream': encoded_string, 'ext': ext}


def round_down(n, d=2):
    d = int('1' + ('0' * d))
    return floor(n * d) / d


def round_up(n, d=2):
    d = int('1' + ('0' * d))
    return ceil(n * d) / d


def check_extension(filename, extensions=("jpg", "jpeg", "png", "gif",)):
    """ Checks if the filename contains any of the specified extensions """

    bits = filename.split(".")
    bits.reverse()

    ext = bits[0].lower()

    return ext in extensions


def whole_number_format(value):
    return "{:,.0f}".format(float(value))


def build_page_url(path, data, p):
    args = ""
    for k in data:
        try:
            if k == "page":
                args = args + "&page" + "=" + str(p)
            else:
                args = args + "&" + parse.quote_plus(str(k)) + "=" + parse.quote_plus(str(data[k]))
        except:
            print(" error building page url ------ for %s in data payload %s" % (k, data))

    return str(path) + "?" + str(args)
