"""
templating.py

@Author:    Olukunle Ogunmokun
@Date:      10th Dec, 2018
@Time:      1:05 PM

This module is responsible for converting templates into string (either html or text)
"""

from jinja2 import TemplateNotFound
from flask import render_template, current_app
import os
from stew import logger


def generate_content(template_dir, template_name, data={}):
    """
    Generates email content by using the template name and the given data
    email templates are stored as either .html or .txt in the templates email directory.
    When a template name is given, it locates the html version by concatenating the template name with '.html'
    and the text template with '.txt'.

    :param template_dir: The path to the template file
    :param template_name: The template name
    :param data: Template data to merge with

    :returns: rendered text
    """

    logger = current_app.logger

    _template = os.path.join(template_dir, template_name)

    try:
        _text = render_template(_template, **data)
        return _text

    except TemplateNotFound as e:
        logger.info(e)


def generate_email_content(template_name, data={}):
    """
    Generates email content by using the template name and the given data
    email templates are stored as either .html or .txt in the templates email directory.
    When a template name is given, it locates the html version by concatenating the template name with '.html'
    and the text template with '.txt'.

    :param template_name: The template name
    :param data: Template data to merge with

    :returns: a (html, text) tuple with either fields
    """

    html_template = "emails/%s.html" % template_name
    text_template = "emails/%s.txt" % template_name

    # Fetch html template

    try:
        html = render_template(html_template, **data)
    except TemplateNotFound as e:
        logger.info(e)
        logger.info("html template not found")
        html = None

    try:
        text = render_template(text_template, **data)
    except TemplateNotFound as e:
        logger.info("text template not found")
        text = None

    return html, text
