__author__ = 'kunsam002'

from stew import mail
import json
from integrations import sendgrid


def send_flask_email(body="", sender=[], recipients=[], cc=None, bcc=None, subject="", text="", html=None, files=None,
                     channel=None):
    msg = Message(subject, sender=("STEW", "stewnigeria1@gmail.com"), recipients=recipients)

    msg.body = body
    msg.html = html

    try:
        mail.send(msg)

        res = {"success": True}
        res = json.dumps(res)
        response = make_response(res)
        return response
    except:
        res = {"success": False}
        res = json.dumps(res)
        response = make_response(res)
        raise


def send_email(body="", sender="info@stew.ng", recipients=[], cc=None, bcc=None, subject="", text="", html=None,
               files=None, channel=None):
    resp_code, response = 403, None
    for recipient in recipients:
        resp_code, response = sendgrid.send(subject, sender="S.T.E.W <%s>" % sender, recipients=[recipient], html=html)
    return resp_code, response
