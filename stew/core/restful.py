"""
restful.py

@Author: Olukunle Ogunmokun
@Date: 11th Dec, 2018
@Time: 12:01 PM

Extension Integration for Flask-Restful.

The classes in this module will be used to provide support for sqlalchemy and non-sqlalchemy integrations. Each base class should be
extended to implement specific functionality.

Requires flask-restful and sqlalchemny

"""
from datetime import datetime
import time
import urllib
import json
import pprint
from collections import namedtuple
import pymongo

import dateutil.parser
from flask_restful import fields
from flask import request, g, make_response, url_for, got_request_exception, current_app as app
from flask_restful import Resource, marshal, reqparse, abort
from sqlalchemy import asc, desc, and_, or_
from stew.core import utils
from stew.core.exceptions import ValidationFailed, IntegrityException, \
    VALIDATION_FAILED, ObjectNotFoundException, FurtherActionException
from stew.core.utils import copy_dict, DateJSONEncoder
from stew.core.utils import build_pagination, convert_dict, copy_dict
import wtforms
import marshmallow
import psycopg2
import base64
try:
    import urlparse
except ImportError:
    import urllib.parse as urlparse

# flask restful api object. Should be instantiated in the flask app
api = app.api
logger = app.logger
Q = None


class Timestamp(fields.Raw):
    def format(self, value):
        # In the event you send the time as a string
        if isinstance(value, (str, unicode)):
            value = dateutil.parser.parse(value)
        return value.isoformat()


class ModelField(fields.Raw):
    """ Custom field class to support embedding a model object within a resource reponse """

    def __init__(self, fields=None, exclude=[], extras=[], endpoint=None, **kwargs):
        super(ModelField, self).__init__(**kwargs)
        self.fields = fields
        self.endpoint = endpoint
        self.exclude = exclude
        self.extras = extras

    def format(self, value):
        """ Extract data as a dict from value object. value must be contain `sendbox_extensions.models.AppMixin` """
        return self.prepare(value)

    def prepare(self, value):
        """ Function to convert value into a dictionary for nesting """

        if isinstance(value, dict):
            data = value
        else:
            data = value.as_dict(include_only=self.fields, exclude=self.exclude, extras=self.extras)

        if self.endpoint:
            data["obj_id"] = data.get("id")  # add obj_id as card_key field

            # remove all unnecessary sections of the url
            uri = urlparse.urlparse(url_for(self.endpoint, **data))
            data["uri"] = urlparse.urlunparse(("", "", uri.path, "", "", ""))
            data.pop("obj_id")  # remove obj_id field

        return data


class ModelListField(ModelField):
    """ Extension class to support embedding iteratable properties within models """

    def __init__(self, _fields=None, exclude=[], extras=[], endpoint=None, **kwargs):
        """
        Field class to support sqlalchemy model list values in json serialization
        :param _fields: Fields to serialize
        :param exclude: Fields to exclude
        :param extras: Extra _fields to include in serialization
        :param endpoint: Endpoint name. If provided will include the URL property inside each object in the list
        :param kwargs: Extra arguments
        :return:
        """
        super(ModelListField, self).__init__(_fields, exclude, extras, endpoint, **kwargs)

    def format(self, values):
        """ Values should be an iteratable property """
        results = [self.prepare(value) for value in values]
        return results


def to_boolean(value):
    """
    Determine Falsy values for boolean.
    :param value: value representation of false
    :return:
    """

    #  lowercase the value if it comes as a string
    if isinstance(value, (str, unicode)):
        value = value.lower()

    if value in ['false', 'f', 'n', 'no', '', None, False]:
        return False
    else:
        return bool(value)


def to_date(value):
    """
    Parse a string representation of date into a date
    :param value: date in string format
    :return:
    """
    try:
        return datetime.strptime(value, '%Y-%m-%d')
    except ValueError:
        raise ValueError("Incorrect data format, should be YYYY-MM-DD")


def to_datetime(value):
    try:
        return dateutil.parser.parse(value)
    except ValueError:
        raise ValueError("Unable to determine date or time from the values sent")


@api.representation('application/json')
def output_json(data, code, headers={}):
    """ Custom json output format to allow automatic date formatting with json """

    if request.content_type in ["application/xml", "text/xml"]:
        print(data)
        api_endpoint = data.pop("api_endpoint", '')
        custom_root = data.pop("custom_root", "Response")

        resp = make_response(utils.convert_dict_to_xml(data, custom_root=custom_root), code)
        resp.headers["Content-Type"] = "text/xml"
        resp.headers["Accept-Type"] = "text/xml"

        resp.headers.extend(headers)
        return resp

    resp = make_response(json.dumps(data, cls=DateJSONEncoder), code)
    resp.headers.extend(headers)

    return resp


@api.representation('text/xml')
@api.representation('application/xml')
def output_xml(data, code, headers={}):
    """ Custom xml response output to allow for automatic data conversion to xml """

    api_endpoint = data.pop("api_endpoint", '')
    custom_root = data.pop("custom_root", "Response")

    resp = make_response(utils.convert_dict_to_xml(data, custom_root=custom_root), code)
    resp.headers["Content-Type"] = "text/xml"
    resp.headers["Accept-Type"] = "text/xml"

    print(resp.headers)
    resp.headers.extend(headers)
    return resp


def empty_filter_parser():
    """ A utility function to return empty filter arguments when an implementation isn't available """
    return {}


@app.errorhandler(ValidationFailed)
def form_validation_error(e):
    """ Error handler for custom form validation errors """
    print(e.data, e.status, "This is where i'm from")
    return api.make_response(e.data, e.status)


@app.errorhandler(IntegrityException)
def integrity_exception_handler(e):
    """ Error handler for custom form validation errors """
    print(" ===============> This is me inside integrity exception handdler <=================")
    app.db.session.rollback()

    return api.make_response(e.data, e.status)


@app.errorhandler(ObjectNotFoundException)
def integrity_exception_handler(e):
    """ Error handler for custom form validation errors """
    return api.make_response(e.data, e.status)


@app.errorhandler(FurtherActionException)
def further_action_exception_handler(e):
    """ Error handler for custom form validation errors """
    return api.make_response(e.data, e.status)


class BaseResource(Resource):
    """
    Base resource class to control all REST API calls.
    """

    # Generic method decorators. Override where necessary
    method_decorators = []
    first_result_only = False  # return individual object rather than Array
    default_page_size = 20
    default_order_param = json.dumps({"order_by": "date_created", "asc_desc": "desc"})
    default_paging_param = json.dumps({"page": 1, "per_page": 20})
    default_order_direction = 'desc'
    default_from_cache = False
    sub_resource = None
    validation_form = None
    print_template = None
    resource_fields = None  # Implement this in subclass!
    resource_name = None  # A default name for the resource. This would be part of the meta information as well
    service_class = None
    mappings_module = None
    db_module = None
    pagers = [dict(name=str(v), value=v) for v in [5, 10, 20, 50, 100]]  # default number of items per page
    filters = []  # list of filters to show on each page. the values will be a list of dict of dicts
    # default sorters. can be overridden in any subclass
    searchable_fields = []  # implement in subclass
    sorters = [
        {
            "name": "ID",
            "value": "id"
        },
        {
            "name": "Name",
            "value": "name"
        },
        {
            "name": "Date Created",
            "value": "date_created"
        },
        {
            "name": "Last Updated",
            "value": "last_updated"
        }
    ]

    def modify_query_response(self, resp, obj_id=None, resource_name=None):
        """ a way to modify response for query """
        return resp

    def modify_get_response(self, resp, obj_id=None):
        """ a way to modify response for get """
        return resp

    def modify_subquery_response(self, resp, obj_id=None, resource_name=None):
        """ a way to modify response for sub query """
        return resp

    @property
    def output_fields(self):
        """ Property function to always generate a clean base value for output fields """
        return {
            'id': fields.Integer,
            'date_created': Timestamp,
            'last_updated': Timestamp,
        }

    # def paging_parser(self):
    #     """
    #     Builds the parser to extract pagination information from the request
    #
    #     :returns: a dict containing extracted values
    #     :rtype: dict
    #     """
    #
    #     r_args = {"error_out": False}
    #
    #     parser = reqparse.RequestParser()
    #     parser.add_argument('page', type=int, default=1, location='args')
    #     parser.add_argument('per_page', type=int, default=self.default_page_size, location='args')
    #
    #     r_args.update(parser.parse_args())
    #
    #     # return r_args

    # def sort_parser(self):
    #     """
    #     Builds the request parser for extracting sorting parameters from the request.
    #
    #     :returns: a dict containing extracted values
    #     :rtype: dict
    #
    #     """
    #     parser = reqparse.RequestParser()
    #     parser.add_argument("order_by", type=str, default=self.default_order_param, location='args')
    #     parser.add_argument("asc_desc", type=str, default=self.default_order_direction, location='args')
    #
    #     return parser.parse_args()

    @staticmethod
    def search_parser():
        """
        Builds the request parser for extracting search parameters from the request.

        :returns: a dict containing extracted values
        :rtype: dict

        """
        parser = reqparse.RequestParser()
        parser.add_argument("query", type=str, default=None, location='args')
        parser.add_argument("id", type=str, location='args')
        parser.add_argument("from_cache", type=bool, location='args')
        parser.add_argument("single_result", type=bool, location='args')

        return parser.parse_args()

    @staticmethod
    def operator_parser():
        """
        Determine the query operator tactic to use. The default value will be '==' or 'eq'. Possible values include:
        'eq', 'neq', 'gte', 'gt', 'lte', 'lt', 'btw'
        """

        parser = reqparse.RequestParser()
        parser.add_argument("op", type=str, location='args', default='eq')

        return parser.parse_args()

    def view_parser(self):
        """
        Determine if a specialized query view needs to be handled. This will carry a name to map to a method on the resource for handling a special query
        """

        parser = reqparse.RequestParser()
        parser.add_argument("view", type=str, location='args', default=None)

        return utils.remove_empty_keys(parser.parse_args())

    def print_parser(self):
        """
        Determine if a specialized query view needs to be handled. This will carry a name to map to a method on the resource for handling a special query
        """

        parser = reqparse.RequestParser()
        parser.add_argument("print", type=str, location='args', default=None)

        return utils.remove_empty_keys(parser.parse_args())

    @staticmethod
    def group_parser():
        """
        Determine if there are bulk ids to execute multiple actions on. If this parameter is present and is represented as
        a list, the values will be used for bulk query actions (bulk_delete, bulk_get)
        """

        parser = reqparse.RequestParser()
        parser.add_argument("group_ids", type=int, location='args', action='append')

        return parser.parse_args()

    def group_action_parser(self):
        """
        group action parser to extract values available for execution.
        These values will be extracted from the form location in a request argument
        the action parser requires a minimum of 1 parameter: action.

        The action specifies which method to execute and passes the group_ids and other parameter values to it

        Example:
        =======

        parser = reqparse.RequestParser()
        parser.add_argument("action", type=str, location='form', default=None)
        parser.add_argument("enabled", type=to_bool, location='form', default=False)

        return parser.parse_args()

        """

        parser = reqparse.RequestParser()
        parser.add_argument("action_name", type=str, location='args', default=None)

        return parser.parse_args()

    def search_filter_queries(self):
        """ Build default search queries."""
        return []

    def search_must_queries(self):
        """ Build default search filters"""
        return []

    # def search_must_fields(self):
    #     return []

    def search_query(self, search_q=None, filter_args={}):
        """ Default search query. Should be typically overwritten in subclass"""
        if search_q:
            must_query = [Q("query_string", **{"query": search_q})]
        else:
            must_query = [Q("match_all")]

        # must_queries = self.search_must_queries()

        # if search_q:
        #     for i in self.search_must_fields():
        #         match_q = {
        #             "match": {
        #                 i: {
        #                     "query": search_q,
        #                     "operator": "or"
        #                 }
        #             }
        #         }
        #
        #         must_queries.append(match_q)

        # must_query = must_queries
        filter_queries = self.search_filter_queries()
        print(" this is inside filter args now", filter_args)

        for name, value in filter_args.items():
            if type(value) is dict:
                for key, val in value.items():
                    data = dict()
                    data[name] = val
                    filter_queries = self.search_operator_func(key, filter_queries, data)
            else:
                key = "$eq"
                data = dict()
                data[name] = value
                filter_queries = self.search_operator_func(key, filter_queries, data)

        print(" match_query", must_query, " filter_queries", filter_queries)
        return must_query, filter_queries

    def sort_parser(self):
        """
        Builds the request parser for extracting sorting parameters from the request.

        :returns: a dict containing extracted values
        :rtype: dict

        """
        parser = reqparse.RequestParser()
        parser.add_argument("sort_by", type=str, default=self.default_order_param, location='args')

        args = parser.parse_args()
        sort_by = args.get("sort_by")

        res_sort_by = utils.remove_empty_keys(json.loads(sort_by))

        if not res_sort_by:
            return utils.remove_empty_keys(json.loads(self.default_order_param))

        return res_sort_by

    def paging_parser(self):
        """
        Builds the parser to extract pagination information from the request

        :returns: a dict containing extracted values
        :rtype: dict
        """

        parser = reqparse.RequestParser()
        parser.add_argument('page_by', type=str, default=self.default_paging_param, location='args')

        args = parser.parse_args()
        page_by = args.get("page_by")

        res_page_by = utils.remove_empty_keys(json.loads(page_by))

        if not res_page_by:
            return utils.remove_empty_keys(json.loads(self.default_paging_param))

        return res_page_by

    def filter_parser(self):
        """
        Builds the request parser for extracting filter parameters from the request.
        Implement this in the subclass. This is accessed during GET queries

        :returns: `flask.ext.restful.reqparse` or None
        """

        parser = reqparse.RequestParser()
        parser.add_argument("filter_by", type=str, default="{}", location='args')

        args = parser.parse_args()
        filter_by = args.get("filter_by")

        return utils.remove_empty_keys(json.loads(filter_by))

    def sort_func(self, asc_desc):
        """
        Returns the proper ordering function based on the card_key given

        :param asc_desc: Ascending or Descending, represented by `asc` or `desc`
        :returns: `sqlalchemy.asc` or `sqlalchemy.desc`
        :rtype: func
        """

        if asc_desc == "asc":
            return asc

        if asc_desc == "desc":
            return desc

    def format_value(self, prop_type, value):
        """ return a formatted prop_type for the value presented """
        prop_type_name = str(prop_type)

        if prop_type_name in ["DATETIME", "DATE", "TIME"]:
            val = dateutil.parser.parse(value)
            return val

        if prop_type_name in ["FLOAT"]:
            return float(value)

        if prop_type_name in ["INTEGER"]:
            return int(value)

        return value

    def or_operator_func(self, query, service_class, names, search_q):
        """ Generate an or filter query for all params inside names. Names is retrieved from the resource file as an array """

        if not names:
            return query

        prop_options = []
        klasses = []
        for name in names:
            # Join and gather props here
            print("this is service_class, query and others", service_class, name)
            klass, prop = self.process_attribute(query, service_class, name)
            prop_options.append(prop)
            klasses.append(klass)

        value = '%{0}%'.format(search_q)

        binary_expressions = [prop.ilike(value) for prop in prop_options]
        # print("binary expression is now", binary_expressions)

        join_klasses = set([kls for kls in klasses if kls is not None])
        print(join_klasses)
        if len(join_klasses) > 0:
            query = query.join(*join_klasses)

        return query.filter(or_(*binary_expressions))

    def operator_func(self, query, service_class, op, name, value):
        """ Returns the query filtered according to the operator and property used.
            Values in use include:
                $eq:  ==
                $ne: !=
                $gt:  >
                $gte: >=
                $lt: <
                $lte: <=
                $in: in_
                $like: ilike
                $btw: between
        """
        if not isinstance(value, (list, tuple)):
            value = [value]

        query2 = query

        klasses = []

        if op == '$eq':
            klass, prop = self.process_attribute(query, service_class, name)
            klasses.append(klass)
            val = self.format_value(prop.type, value[0])
            query = query.filter(prop == val)
        if op == '$ne':
            klass, prop = self.process_attribute(query, service_class, name)
            klasses.append(klass)
            val = self.format_value(prop.type, value[0])
            query = query.filter(prop != val)
        if op == '$gt':
            klass, prop = self.process_attribute(query, service_class, name)
            klasses.append(klass)
            val = self.format_value(prop.type, value[0])
            query = query.filter(prop > val)
        if op == '$gte':
            klass, prop = self.process_attribute(query, service_class, name)
            klasses.append(klass)
            val = self.format_value(prop.type, value[0])
            query = query.filter(prop >= val)
        if op == '$lt':
            klass, prop = self.process_attribute(query, service_class, name)
            klasses.append(klass)
            val = self.format_value(prop.type, value[0])
            query = query.filter(prop < val)
        if op == '$lte':
            klass, prop = self.process_attribute(query, service_class, name)
            klasses.append(klass)
            val = self.format_value(prop.type, value[0])
            query = query.filter(prop <= val)
        if op == '$in':
            klass, prop = self.process_attribute(query, service_class, name)
            klasses.append(klass)
            vals = [self.format_value(prop.type, v) for v in value[0]]
            query = query.filter(prop.in_(vals))
        if op == '$like':
            klass, prop = self.process_attribute(query, service_class, name)
            klasses.append(klass)
            val = self.format_value(prop.type, value[0])
            query = query.filter(prop.ilike('%{0}%'.format(val)))
        if op == '$btw':
            btw = value[0]
            start = btw.get("from", None)
            finish = btw.get("to", None)

            # Parse format of value based on prop type
            klass, prop = self.process_attribute(query, service_class, name)
            klasses.append(klass)
            _from = self.format_value(prop.type, start)
            _to = self.format_value(prop.type, finish)

            # print "inside filtering", prop, prop.type, op, start, type(start), finish, type(finish)
            query = query.filter(prop.between(_from, _to))

        join_klasses = set([kls for kls in klasses if kls is not None])
        print(join_klasses)
        if len(join_klasses) > 0:
            query = query.join(*join_klasses)

        return query

    def process_attribute(self, query, service_class, name):
        """ This will be used to fetch the filter attribute based on either a dot notation on single property """

        bits = name.split(".")
        if len(bits) == 2:
            k, prop = bits
            klass = getattr(self.db_module, k)
            # klass = getattr(self.db_module, k).mapper.class_

            return klass, getattr(klass, prop)

        # name is normal, filter as regular field
        return None, getattr(service_class.model_class, name)

    def is_permitted(self, obj=None, **kwargs):
        """
        Determines whether or not access to this object is permitted. Implement a mechanism to check
        that the user has access to the object in `obj`. It should either raise an 'Access Denied'
        exception or return the object

        """

        return obj

    def ordering(self, service_class, query, order_by, asc_desc):
        """ ordering will handle single or dot notation based sorting. it figures out how to sort the database by determining if a join query is required or not, if it's not possible, then just return the query as is """

        _sort = self.sort_func(asc_desc)  # Extracts which sorting direction is required

        # Determine if a dot notation exists and use this to proxy filtering into another object
        if order_by.find(".") < 0:
            return query.order_by(_sort(getattr(service_class.model_class, order_by)))

        # Object contains dot notation. Determine parent object before moving forward
        bits = order_by.split(".")
        if len(bits) != 2:
            return query

        k, prop = bits
        klass = getattr(self.db_module, k)

        return query.join(klass).order_by(_sort(getattr(klass, prop)))

    def updated_form_data(self, attrs):
        """
        Updates the form data to be stored by including any global or default information necessary.
        This can be overridden in subclasses to provide merchant/user specific input

        """

        return attrs

    def adjust_form_fields(self, form):
        """
        Adjust the validation form fields by modifying choices or improving elements accordingly.
        Override this method to implement the core functionality.
        """

        return form

    def adjust_form_data(self, data):
        """
        Adjust the validated form data after completing form validation but before persisting the data.
        Override this method to implement the core functionality.
        """

        return data

    @staticmethod
    def prepare_errors(errors):
        """
        Helper class to prepare errors for response
        """
        _errors = {}
        for k, v in errors.items():
            _res = [str(z) for z in v]
            _errors[str(k)] = _res

        return _errors

    def include_form_data(self):
        """ Function to include backend specific data into every form, it will be integrated with the form before validation """
        return dict()

    def prevalidate(self, data):
        """ Prevalidation function. This can be used to modify or augment the data before it is sent into validation """

        return data

    def validate(self, form_class, obj=None, adjust_func=None):
        """
        Validates data from a POST/PUT request and returns the results.
        If any validation errors occur, the request ends and returns a
        400 http error code along with the error message

        :param form_class: The validation form class to be used
        :param obj: The initial object to update. (Useful when updating an existing object)

        :returns: Validated form data or raises a http error if validation fails

        """

        # if not adjust_func:
        # 	adjust_func = self.adjust_form_fields

        if form_class is None:
            abort(405, status="Not Allowed", message="The data transmitted cannot be validated.")
        # converted to a patched version of wtforms

        # Tweak this bit to support both marshmallow models and wtforms models

        if issubclass(form_class, wtforms.Form):

            data = dict()
            if request.content_type in ["text/xml", "application/xml"]:
                custom_root = "%sRequest" % self.resource_name
                data = utils.convert_xml_to_dict(str(request.data), custom_root=custom_root)

            form = form_class(obj=obj, csrf_enabled=False, **data)

            if adjust_func:
                form = adjust_func(form)

            if form.validate():
                return self.updated_form_data(form.data), request.files
            else:
                raise ValidationFailed(self.prepare_errors(form.errors))

        elif issubclass(form_class, marshmallow.Schema):

            # get Content-Type management
            content_type = request.content_type

            if "multipart/form-data" in content_type:
                print("File is multipart")

            data = request.form if "multipart/form-data" in content_type else request.get_json()
            if request.content_type in ["text/xml", "application/xml"]:
                custom_root = "%sRequest" % self.resource_name
                data = utils.convert_xml_to_dict(str(request.data), custom_root=custom_root)

            # pass the function into prevalidation so that we can augment it with values or do whatever we need before we begin
            pre_validated_data = self.prevalidate(data)

            data, errors = form_class().load(pre_validated_data)

            if errors:
                print("Errors", errors)
                raise ValidationFailed(errors)

            else:
                return self.updated_form_data(data), request.files

    # def permit_sub_query(self, obj_id, query, resource_name, param, filter_args, sort_args, search_args, paging_args, **kwargs):
    #     return query

    def execute_sub_query(self, obj_id, resource_name, param, **kwargs):
        """ Build a sub resource query by filtering it's base query with the principal parameters """

        query = getattr(self, "%s_query" % resource_name, None)
        query = query(self.query()) if query else None
        if not query:
            query = self.query()

        sort_args = self.sort_parser()
        filter_args = self.filter_parser()
        paging_args = self.paging_parser()
        search_args = self.search_parser()

        query = query.filter(getattr(self.service_class.model_class, param) == obj_id)
        # Add a property to prevent sub query from executing, or modifying the sub query before moving ahead

        resp, status = self.execute_query(self.resource_name, query, self.service_class, filter_args=filter_args,
                                          sort_args=sort_args,
                                          paging_args=paging_args, view_args=kwargs.get("view_args", {}),
                                          print_args=kwargs.get("print_args", {}))

        print("====+++++ this is inside the subquery part", obj_id)
        resp = self.modify_subquery_response(resp, obj_id=obj_id, resource_name=resource_name)

        return resp, status

    def execute_query(self, resource_name, query, service_class, filter_args={}, sort_args={}, search_args={},
                      paging_args={}, resource_fields={}, operator_args={}, view_args={}, print_args={}, obj_id=None,
                      **kwargs):
        """
        Builds and executes the actual query based on a Model/Object/ORM and filtering, sorting and paging
        information.

        :param query: Base query to start with
        :param service_class: Model class to base filtering on
        :param filter_args: parameters to filter results by
        :param sort_args: parameters to sort/order results by
        :param paging_args: parameters for paging results
        :param resource_fields: fields to use when generating the output
        :param kwargs: other optional parameters required

        :returns a dict containing meta information and results
        """

        resp = {"api_endpoint": resource_name}
        order_by, asc_desc = sort_args.get("order_by"), sort_args.get("asc_desc")
        page, per_page, error_out = paging_args.get("page", 1), paging_args.get("per_page", 20), paging_args.get(
            "error_out")
        search_q = search_args.get("query")
        search_id = search_args.get("search_id")
        from_cache = search_args.get("from_cache", self.default_from_cache)
        single_result = search_args.get("single_result", self.first_result_only)

        # if not from_cache:
        #     from_cache=self.default_from_cache

        view_name = view_args.get("view")
        with_print = print_args.get("print")

        # Filter Query by using view_function name on object
        view_func = getattr(self, "%s_query" % view_name.lower(), None) if view_name else None
        print_template = getattr(self, "%s_print_template" % view_name.lower(), None) if view_name else None

        # Reset of order by and sort by with filter args
        if not order_by or not asc_desc:
            default_order_param = json.loads(self.default_order_param)
            order_by, asc_desc = default_order_param.get("order_by"), default_order_param.get("asc_desc")

        if search_q:
            print("---------> the point of hijacking search, resetting filters and view <---------------", search_q,
                  filter_args)
            names = self.searchable_fields or []
            view_func = None
            filter_args = {}
            print("searchable fields", names)

            query = self.or_operator_func(query, service_class, names, search_q)

        if view_func:
            query = view_func(query)

        # TODO implement permission check here
        if obj_id:
            self.is_permitted(self.fetch(obj_id))

        # apply searching based on searchable fields
        if search_id:
            return self.execute_search_by_id(service_class.model_class, search_id)

        if from_cache:
            print(" i am calling the search", filter_args)
            return self.execute_search(sort_args=sort_args, paging_args=paging_args,
                                       filter_args=filter_args, from_cache=from_cache)

        # attempting to hijack search_q
        # if search_q:
        #     print " i am calling the search", filter_args
        #     return self.execute_search(search_q=search_q, sort_args=sort_args, paging_args=paging_args,
        #                                filter_args={}, from_cache=from_cache)


        query = self.limit_query(query)
        # apply the query filters
        print("filter_args", filter_args)
        for name, value in filter_args.items():
            if type(value) is dict:
                for key, val in value.items():
                    op = key
                    query = self.operator_func(query, service_class, op, name, val)
            else:
                op = '$eq'
                query = self.operator_func(query, service_class, op, name, value)

        # # apply sorting
        # _sort = self.sort_func(asc_desc)  # Extracts which sorting direction is required
        #
        # query = query.order_by(_sort(getattr(service_class.model_class, order_by)))
        # for name, value in sort_args.items():
        #     model_field = getattr(getattr(service_class.model_class, name),value)()
        #     query = query.order_by(model_field)


        query = self.ordering(service_class=service_class, query=query, order_by=order_by, asc_desc=asc_desc)

        if single_result:
            res = query.first()

            if not res:
                abort(404, status='Result not Found', message='')

            output_fields = self.output_fields

            output_fields.update(self.resource_fields)

            return marshal(res, output_fields), 200

        try:
            paging = query.paginate(int(page), int(per_page), True)
        except Exception as e:
            print(e, "this is over paging")
            paging = query.paginate(int(1), int(per_page), False)

        # file_url = None
        # if with_print and print_template:
        #
        #     print_file_path = service_class.do_print(print_template, number_format=utils.number_format,
        #                                       service_class=service_class, items=paging.items, today=datetime.today())
        #
        #     download_url_path = app.config.get("DOWNLOAD_URL_PATH", "/download")
        #     file_url = "%s/%s" % (download_url_path, print_file_path)


        # implement converting the query into a string
        print_string = base64.b64encode(str(query))  # this will be used to develop print functionality

        resp["query"] = search_q
        resp["from_cache"] = from_cache
        resp["sort_by"] = sort_args
        resp["filter_by"] = filter_args
        resp["view"] = view_name
        resp["print_string"] = print_string

        # extract the request args and modify them for paging
        request_args = copy_dict(request.args, {})

        paging_args["total"] = paging.total
        paging_args["pages"] = paging.pages
        paging_args["page"] = paging.page

        if paging.has_next:
            # build next page query parameters
            request_args["page"] = paging.next_num
            resp["next"] = paging.next_num
            paging_args["next"] = paging.next_num
            resp["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))
        else:
            resp["next"] = None
            paging_args["next"] = None

        if paging.has_prev:
            # build previous page query parameters
            request_args["page"] = paging.prev_num
            resp["prev"] = paging.prev_num
            paging_args["prev"] = paging.prev_num
            resp["prev_page"] = "%s%s" % ("?", urllib.urlencode(request_args))
        else:
            resp["prev"] = None
            paging_args["prev"] = None

        resp["page_by"] = paging_args

        output_fields = self.output_fields
        _resource_fields = resource_fields or self.resource_fields

        output_fields.update(_resource_fields)

        resp["results"] = marshal(paging.items, output_fields)

        # experimental way to modify the response after all has been done.
        resp = self.modify_query_response(resp, obj_id=obj_id, resource_name=resource_name)

        return resp, 200

    def execute_get(self, obj_id, **kwargs):
        """ Execute a get query to retrieve an exact object by id."""

        output_fields = self.output_fields
        output_fields.update(self.resource_fields or {})
        obj = self.fetch(obj_id)

        obj = self.is_permitted(obj)  # check if you're permitted

        resp = marshal(obj, output_fields)
        resp.update({"api_endpoint": self.resource_name})

        # experimental way to modify the response after all has been done.
        resp = self.modify_get_response(resp, obj_id=obj_id)

        return resp, 200

    def execute_group_action(self, obj_ids, attrs, files=None):
        """ Executes group actions by obj_ids """
        action_name = attrs.get("action_name", None)

        resp = None
        status = 201

        if action_name:
            action_func = getattr(self, "%s_group_action" % action_name, None)

            # if action_func exists, then pass the attrs
            if action_func:

                # inject validation here
                validation_form = getattr(self, "%s_validation_form" % action_name, None)
                adjust_func = getattr(self, "%s_adjust_form_fields" % action_name, None)

                if not validation_form:
                    abort(405, status="Not Authorized",
                          message="The requested resource is not yet authorized for access")

                data, files = self.validate(validation_form, adjust_func=adjust_func)
                data = self.adjust_form_data(data)

                resp = action_func(obj_ids, data, files)

        return resp, status

    def execute_search_by_id(self, model_class, search_id):

        raise NotImplementedError("This function is yet to be implemented")

    def search_operator_func(self, op, filter_queries, filter_params):
        for term, value in filter_params.items():

            if not isinstance(value, (list, tuple)):
                value = [value]

            data = dict()
            if op == "$eq":
                data[term] = value[0]
                filter_queries.append(Q("term", **data))
            if op == '$neq':
                data[term] = value[0]
                filter_queries.append(~Q("term", **data))
            if op == '$gt':
                data[term] = dict(gt=value[0])
                filter_queries.append(Q("range", **data))
            if op == '$gte':
                data[term] = dict(gte=value[0])
                filter_queries.append(Q("range", **data))
            if op == '$lt':
                data[term] = dict(lt=value[0])
                filter_queries.append(Q("range", **data))
            if op == '$lte':
                data[term] = dict(gt=value[0])
                filter_queries.append(Q("range", **data))
            if op == '$btw':
                data[term] = dict(gte=value[0], lte=value[1])
                filter_queries.append(Q("range", **data))

        return filter_queries

    def execute_search(self, search_q=None, sort_args={}, search_args={}, paging_args={}, filter_args={},
                       from_cache=False, aggregate_by=None, aggregate_type=None, **kwargs):
        """
        Builds and executes the actual search query based on a Model injection into the search engine
        information.

        :param search_q: Search query to start with
        :param sort_args: parameters to sort/order results by
        :param paging_args: parameters for paging results
        :param kwargs: other optional parameters required

        :returns a dict containing meta information and results
        """

        resp = {"api_endpoint": self.resource_name}
        order_by, asc_desc = sort_args.get("order_by"), sort_args.get("asc_desc")
        page, per_page, error_out = int(paging_args.get("page", 1)), int(
            paging_args.get("per_page", 20)), paging_args.get("error_out")

        sorting = dict([(order_by, {'order': asc_desc})])

        klass = getattr(self.mappings_module, self.service_class.model_class.__name__)

        must_query, filter_query = self.search_query(search_q=search_q, filter_args=filter_args)

        from_ = int(per_page) * (int(page) - 1)

        print("inside search =====?>>>>", must_query, filter_query)
        print(klass.search())

        _resp_ = klass.search().query(Q('bool', must=must_query, filter=filter_query)).extra(from_=from_, size=per_page)

        print(_resp_.to_dict())

        _resp_ = _resp_.sort(sorting)

        _resp_ = _resp_[from_: per_page]

        result = _resp_.execute()

        if from_cache:
            hits = [x.to_dict() for x in result.hits]
        else:
            response_ids = [o.id for o in result.hits]
            hits = self.query_by_ids(response_ids)

        output_fields = self.output_fields
        output_fields.update(self.resource_fields or {})

        response = {"total": result.hits.total, "results": marshal(hits, output_fields), "page": page,
                    "limit": per_page}

        paging = build_pagination(page, per_page, len(hits))

        response.update(paging)

        request_args = copy_dict(request.args, {})

        resp.update(response)

        to_be_popped = ['current_page']

        paging_args["total"] = paging.get("total")
        paging_args["pages"] = paging.get("pages")
        paging_args["page"] = paging.get("page")

        resp["page_by"] = paging_args
        resp["filter_by"] = filter_args
        resp["sort_by"] = sort_args

        resp["query"] = search_q
        resp["from_cache"] = from_cache

        if not resp['prev']:
            to_be_popped.append('prev')
            to_be_popped.append('prev_page')
        else:
            request_args["page"] = resp['prev']
            resp["prev_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

        if not resp['next']:
            to_be_popped.append('next')
            to_be_popped.append('next_page')
        else:
            request_args["page"] = resp['next']
            resp["next_page"] = "%s%s" % ("?", urllib.urlencode(request_args))

        for i in to_be_popped:
            resp.pop(i, None)

        return resp, 200

    def query_by_ids(self, object_ids):

        """ This method returns database model objects based on the ids of a search query """

        if hasattr(self.service_class.model_class, "extra_fields") and self.service_class.model_class.extra_fields:
            return [x.as_dict(extras=self.service_class.model_class.extra_fields) for x in
                    self.service_class.model_class.query.filter(
                        self.service_class.model_class.id.in_(object_ids)).all()]
        else:
            return [x.as_dict() for x in self.service_class.model_class.query.filter(
                self.service_class.model_class.id.in_(object_ids)).all()]

    def limit_query(self, query, **kwargs):
        """
        Optionally filter the query by a particular property.
        This is useful in limiting the queries to only elements from a particular subset of the data.
        modifications on the query should be implemented here and the query object
        should be returned.

        :param query: the query to be executed

        :returns: query

        """

        return query

    def query(self):
        """ Define the query that loads data for your GET request. """
        return self.service_class.query

    def save(self, attrs, files=None):
        """
        Saves information sent in by POST request.
        This will be used along with self.validate(form_class, obj=None)
        The functionality is usually implemented in one of the service functions

        :param attrs: the data to be saved.
        Override this to implement specialized functionality
        """

        obj = self.service_class.create(**attrs)

        return obj

    def update(self, obj_id, attrs, files=None):
        """
        Updates information sent in by PUT request.
        This will be used along with self.validate(form_class, obj=None)
        The functionality is usually implemented in one of the service functions

        :param attrs: the data to be saved.
        """
        return self.service_class.update(obj_id, **attrs)

    def fetch(self, obj_id):
        """
        Updates information sent in by PUT request.
        This will be used along with self.validate(form_class, obj=None)
        The functionality is usually implemented in one of the service functions

        :param attrs: the data to be saved.
        """
        return self.service_class.get(obj_id)

    def bulk_update(self, obj_ids, attrs, files=None):
        """
        Updates information sent in by PUT request.
        This will be used along with self.validate(form_class, obj=None)
        The functionality is usually implemented in one of the service functions

        :param attrs: the data to be saved.
        """

        ignored_args = ["id", "date_created", "last_updated"]
        return self.service_class.update_by_ids(obj_ids, ignored_args=ignored_args, **attrs)

    def destroy(self, obj_id):
        """
        Deletes and object sent in by DELETE request.
        The functionality is usually implemented in one of the service functions

        :param obj_id: the id of the object to be deleted
        """

        return self.service_class.delete(obj_id)

    def delete_group_action(self, obj_ids, attrs, files=None):
        """
        Deletes objects sent in by DELETE request.
        The functionality is usually implemented in one of the service functions

        :param obj_ids: the list of ids for objects to be deleted
        """

        return self.service_class.delete_by_ids(ids=obj_ids)

    @staticmethod
    def current_user():
        """ Retrieves the current user of the api """
        return getattr(g, "user")

    @staticmethod
    def get_user_id():
        _user_id = getattr(g, "user_id", None)

        if not _user_id:
            abort(401, status="Invalid credentials",
                  message="the user credentials provided is invalid for this resource")

        return _user_id

    def get(self, obj_id=None, resource_name=None):
        """
        Handle a get request based on the parameters
        """

        filter_args = self.filter_parser()
        paging_args = self.paging_parser()
        sort_args = self.sort_parser()
        search_args = self.search_parser()
        view_args = self.view_parser()
        print_args = self.print_parser()
        resource_fields = self.resource_fields

        # Determine which query to execute based on parameters passed.

        try:
            if obj_id is None:
                # This is the bulk query method. If no obj_id is passed, then execute the collection request [execute_query]
                query = self.query()
                # execute the query and build the response.
                resp, status = self.execute_query(self.resource_name, query, self.service_class, filter_args, sort_args,
                                                  search_args,
                                                  paging_args, resource_fields, view_args=view_args,
                                                  print_args=print_args)

            elif obj_id is not None and resource_name is None:
                # This is an object get request. Based on obj_id, execute the get request [execute_get]
                resp, status = self.execute_get(obj_id)

            elif obj_id is not None and resource_name is not None:
                # This is a get sub-request based on the parent object by obj_id. execute the collection sub request [execute_query]

                # sub_query_method = getattr(self, "%s_query" % resource_name.lower(), None)  # find the appropriate query
                # sub_filter_parser = getattr(self, "%s_filter_parser" % resource_name.lower(), empty_filter_parser)  # find the filter parser
                # sub_resource_fields = getattr(self, "%s_resource_fields" % resource_name.lower(), {})  # find the resource fields
                # sub_service_class = getattr(self, "%s_service_class" % resource_name.lower(), None)  # find the resource fields

                # if not sub_query_method or not sub_service_class:
                #     abort(404)
                #     # extract the sub_query, filter_args, parser_args, sort_args and execte the collection.
                #
                # sub_filter_args = sub_filter_parser()
                # query = sub_query_method(obj_id)

                obj = self.fetch(obj_id)

                sub_resource_class = getattr(self, "%s_resource_class" % resource_name.lower(), None)

                if not sub_resource_class:
                    abort(404)

                param = getattr(self, "%s_resource_param" % resource_name.lower(), None)

                resp, status = sub_resource_class().execute_sub_query(obj.pk, resource_name=resource_name, param=param)

            return resp, status
        except Exception as e:
            app.db.session.rollback()
            raise IntegrityException(e)

    def post(self, obj_id=None, resource_name=None):
        """ Execute a post request based on criteria given by the above parameters.
            If obj_id isn't passed. It implies a create function call. If there's an obj_id, it implies and update.
            if resource_name is passed, it implies a sub update method call
            It also contains logic for a bulk update
         """

        # extract bulk ids from the list to see how it works
        group_args = self.group_parser()
        obj_ids = group_args.get("group_ids", None)

        if obj_id is None and obj_ids is None:
            # when obj_id isn't passed, this executes the save function call
            self.is_permitted()  # check if you're permitted first
            attrs, files = self.validate(self.validation_form, adjust_func=self.adjust_form_fields)

            # update the form data using this interceptor. i.e inject a domain group id (card_merchant_id, courier_id etc.)
            attrs = self.adjust_form_data(attrs)

            try:
                res = self.save(attrs, files)  # execute save method [self.save()]
                output_fields = self.output_fields
                output_fields.update(self.resource_fields or {})

                return marshal(res, output_fields), 201
            except Exception as e:
                logger.error(e)
                raise

        elif obj_id is not None and resource_name is None:
            # when an obj_id is passed but resource_name doesn't exist, this implies an update method call
            obj = self.fetch(obj_id)
            obj = self.is_permitted(obj)  # check if you're permitted first
            attrs, files = self.validate(self.validation_form, obj=obj, adjust_func=self.adjust_form_fields)
            attrs = self.adjust_form_data(attrs)
            try:
                res = self.update(obj_id, attrs, files)  # execute update method [self.update()]
                output_fields = self.output_fields
                output_fields.update(self.resource_fields or {})

                return marshal(res, output_fields), 201
            except Exception as e:
                logger.error(e)
                raise IntegrityException(e)

        elif obj_id is not None and resource_name is not None:
            # when obj_id is passed along with a resource_name. this implies a do_method call.
            obj = self.fetch(obj_id)
            self.is_permitted(obj)  # check if you're permitted first

            adjust_func = getattr(self, "%s_adjust_form_fields" % resource_name, None)
            do_method = getattr(self, "do_%s" % resource_name, None)
            validation_form = getattr(self, "%s_validation_form" % resource_name, None)

            if do_method is None:
                abort(405, status="Not Authorized", message="The requested resource is not yet authorized for access")

            try:
                attrs = request.data or {}
                files = None

                # if there is a validation form, use it
                if validation_form:
                    attrs, files = self.validate(validation_form, obj, adjust_func=adjust_func)
                    attrs = self.adjust_form_data(attrs)

                res = do_method(obj_id, attrs, files)  # Functionality for saving data implemented here
                output_fields = self.output_fields
                output_fields.update(self.resource_fields or {})

                return marshal(res, output_fields), 201
            except Exception as e:
                logger.error(e)
                raise

        elif obj_id is None and resource_name is None and obj_ids is not None:
            # attempting a bulk update. only occurs when bulk_ids values are present and there's not obj_id
            self.is_permitted()  # check if you're permitted first

            # cannot use validation form here, values will be derived from an update parser
            attrs = self.group_action_parser()
            files = None

            try:
                resp, status = self.execute_group_action(obj_ids, attrs, files)

                return resp, status
            except Exception as e:
                logger.error(e)
                raise IntegrityException(e)

    def put(self, obj_id=None, resource_name=None):
        """ Put request will re-route to post request """
        return self.post(obj_id=obj_id, resource_name=resource_name)

    def delete(self, obj_id=None):
        """
        Handles deleting the resource entity. If the obj_id value isn't passed, then a bulk delete function is executed
        and requires values from bulk_ids retrieved via bulk_parser method
        """
        if obj_id is None:
            # the obj_id isn't sent, as such a bulk delete is requested
            group_args = self.group_parser()
            obj_ids = group_args.get("group_ids", [])
            attrs = self.group_action_parser()
            files = None
            try:

                resp = self.delete_group_action(obj_ids, attrs, files)
                return resp, 201
            except Exception as e:
                raise IntegrityException(e)
        else:
            # the obj_id sent, as such a single delete is requested
            obj = self.fetch(obj_id)
            self.is_permitted(obj)  # check if you're permitted
            try:
                resp = self.destroy(obj_id)  # Functionality for saving data implemented here

                return resp, 201
            except Exception as e:
                logger.error(e)
                raise IntegrityException(e)
