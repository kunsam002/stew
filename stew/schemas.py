from marshmallow import Schema, fields, validates, validate, ValidationError, validates_schema
from marshmallow.validate import Regexp, OneOf
from stew.models import *
from sqlalchemy import func, and_, or_
from pprint import pprint


class NumericStringField(fields.Field):
    """ Numeric string field to ensure validation and conversion of a numeric value into string """

    # default validator to use
    validator = Regexp(regex="\d+(\.\d+)?")

    def _serialize(self, value, attr, obj):
        return str(value)

    def _deserialize(self, value, attr, obj):
        return str(value)

    def _validate(self, value):
        self.validator(value)


def check_chars(input):
    """ Checks if there's a special character in the text """

    chars = """ '"!@#$%^&*()+=]}_[{|\':;?/>,<\r\n\t """
    return any((c in chars) for c in input)


class LoginSchema(Schema):
    username = fields.String(required=True, allow_none=False)
    password = fields.String(required=True, allow_none=False)


class UploadSchema(Schema):
    placeholder = fields.String(required=False, allow_none=True)


class StateSchema(Schema):
    country_code = fields.String(required=True, allow_none=False)
    name = fields.String(required=True, allow_none=False)


class UserSchema(Schema):
    name = fields.String(required=True, allow_none=False, validate=validate.Length(min=1, error='Cannot be blank'))
    username = fields.String(required=True, allow_none=False)
    email = fields.String(required=True, allow_none=False)
    password = fields.String(required=True, allow_none=False)
    phone = fields.String(required=True, allow_none=False)
    roles = fields.String(required=True, allow_none=False)

    @validates('username')
    def validate_username(self, value):
        if User.query.filter(func.lower(User.username) == value.lower()).count() != 0:
            raise ValidationError("This username is already in use")

        if check_chars(value):
            raise ValidationError("Only letters and numbers, no spaces or special characters allowed")

    @validates('email')
    def validate_email(self, value):
        if User.query.filter(func.lower(User.email) == value.lower()).count() != 0:
            raise ValidationError("This username is already in use")


class MenuSchema(Schema):
    name = fields.String(required=True, allow_none=False, validate=validate.Length(min=1, error='Cannot be blank'))
    caption = fields.String(required=True, allow_none=False)
    description = fields.String(required=True, allow_none=False)
    ingredients = fields.String(required=True, allow_none=False)
    menu_sizes = fields.String(required=True, allow_none=False)
    menu_frequencies = fields.String(required=True, allow_none=False)
    production_time = fields.Integer(required=True, allow_none=False)
    default_cost = fields.Float(required=True, allow_none=False)
    menu_category_code = fields.String(required=True, allow_none=False)

    @validates('menu_category_code')
    def validate_menu_category_code(self, value):
        if not MenuCategory.query.get(value):
            raise ValidationError("Invalid Menu Category")


class LikeSchema(Schema):
    user_id = fields.Integer(required=True, allow_none=False)
    menu_id = fields.Integer(required=True, allow_none=False)




class BlankSchema(Schema):
    pass
