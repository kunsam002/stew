__author__ = 'kunsam002'
"""
All subscriptions having to do with accounts services.
"""

from stew.signals import *
from stew import app, logger
from stew.models import *
from stew.core import templating, utils, messaging
from datetime import date, datetime, timedelta
import json

stew_contact = app.config.get("CONTACT_EMAIL").split(",")+["stewnigeria1@gmail.com"]
# stew_contact = ['kunsam002@gmail.com',"hello@stew.ng"]
_canonical_url = app.config.get("CANONICAL_URL")


@user_created.connect
def _user_created(obj_id, **kwargs):
    """ Sends out an email when the an user signs up """
    today = date.today()

    user = User.query.get(obj_id)
    if not user:
        raise Exception("User does not exist")

    canonical_url = _canonical_url

    html, text = templating.generate_email_content("user_created", data=locals())
    recipients = stew_contact + [user.email]
    try:
        messaging.send_email(subject="Welcome", recipients=recipients, html=html, text=text,
                             files=None)
    except:
        _data_ = {"status": "incomplete", "message": "Email Notification Sending Failed."}
        return _data_


@calculate_cart_total.connect
def _calculate_cart_total(cart_id, **kwargs):
    cart = Cart.query.get(cart_id)
    if not cart:
        raise Exception("Cart does not exist")

    return cart.total


@user_password_reset.connect
def _user_password_reset(user_id, **kwargs):
    email, name, link = kwargs.get("email", ""), kwargs.get("name", ""), kwargs.get("link", "")
    canonical_url = _canonical_url

    html, text = templating.generate_email_content("user_password_reset", data=locals())
    try:
        messaging.send_email(subject="Request Password Reset", recipients=[email], html=html,
                             text=text,
                             files=None)
    except:
        _data_ = {"status": "incomplete", "message": "Email Notification Sending Failed."}
        return _data_


@password_reset_successful.connect
def _password_reset_successful(user_id, **kwargs):
    email, name = kwargs.get("email", ""), kwargs.get("name", "")
    canonical_url = _canonical_url

    html, text = templating.generate_email_content("password_reset_successful", data=locals())

    try:
        messaging.send_email(subject="Password Reset Successful", recipients=[email], html=html,
                             text=text,
                             files=None)
    except:
        _data_ = {"status": "incomplete", "message": "Email Notification Sending Failed."}
        return _data_


@contact_message.connect
def _contact_message(obj_id, **kwargs):
    msg = AdminMessage.query.get(obj_id)
    if not msg:
        raise Exception("AdminMessage does not exist")
    canonical_url = _canonical_url

    sender, body = msg.name, msg.body

    html, text = templating.generate_email_content("contact_message", data=locals())
    recipients = stew_contact
    try:
        messaging.send_email(subject="Contact Message - %s" % msg.subject,
                             recipients=recipients, html=html, text=text,
                             files=None)
    except:
        _data_ = {"status": "incomplete", "message": "Email Notification Sending Failed."}
        return _data_


@order_placed.connect
def _order_placed(obj_id, **kwargs):
    order = Order.query.get(obj_id)
    if not order:
        raise Exception("Order does not exist")

    canonical_url = _canonical_url

    number_format = utils.number_format

    code, full_name = order.code, order.name
    order_entries = OrderEntry.query.filter(OrderEntry.order_id == order.id).all()

    html, text = templating.generate_email_content("order", data=locals())

    recipients = [order.email]
    try:
        messaging.send_email(subject="New Order - %s" % order.code,
                             recipients=recipients, html=html, text=text,
                             files=None)
    except:
        _data_ = {"status": "incomplete", "message": "Email Notification Sending Failed."}
        return _data_
