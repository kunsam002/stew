'use strict';
var app = angular.module('app', []);
//
app.config(['$interpolateProvider', '$locationProvider',
    function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[')
        $interpolateProvider.endSymbol(']]')
    }
]);

app.filter('striptags', function () {
    return function (text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});

app.controller('HeaderController', ['$scope', '$rootScope', '$http', '$timeout', '$window',
		function($scope, $rootScope, $http, $timeout, $window) {
				$scope.init = function() {

						$http({
								url: $window.HEADER_URL,
								method: 'GET',
																headers: {
																				"Content-Type": "application/json"
																},
								params: '',
								data: '',
						}).then(function successCallback(resp) {
							$scope.total_quantity = resp.data.total_quantity || 0
							$scope.cart_items = resp.data.cart_items
						}, function failureCallback(resp) {
						})
				}

				$rootScope.$on('item.added', function (event, data) {
						$scope.init();
					});

				$rootScope.$on('item.added_wishlist', function (event, data) {
						$scope.init();
					});

				$rootScope.$on('item.updated', function (event, data) {
						$scope.init();
					});

				$rootScope.$on('item.all_clear', function (event, data) {
					$scope.init();
					});

		}
]);


//
// app.controller('AddedItemController', ['$scope', '$rootScope', '$http', '$timeout', '$window', '$element',
//     function ($scope, $rootScope, $http, $timeout, $window, $element) {
//
//         $scope.added = false
//
//         $rootScope.$on('item.added', function (event, data) {
//             $scope.data = data
//             $('.item-added-flash').show()
//             setTimeout(function () {
//                 $('.item-added-flash').fadeOut('fast');
//             }, 5000);
//         });
//
//         $rootScope.$on('item.not_added', function (event, data) {
//             $scope.data = data
//             $('.item-not-added-flash').show()
//             setTimeout(function () {
//                 $('.item-not-added-flash').fadeOut('fast');
//             }, 5000);
//         });
//
//         $rootScope.$on('item.updated', function (event, data) {
//             $scope.data = data
//             $('.item-updated-flash').show()
//             setTimeout(function () {
//                 $('.item-updated-flash').fadeOut('fast');
//             }, 5000);
//         });
//
//         $rootScope.$on('item.added_wishlist', function (event, data) {
//             $scope.data = data;
//             $('.item-wishlist-added-flash').show()
//             setTimeout(function () {
//                 $('.item-wishlist-added-flash').fadeOut('fast');
//             }, 5000);
//         });
//
//         $rootScope.$on('item.not_added_wishlist', function (event, data) {
//             $scope.data = data;
//             $('.item-wishlist-not-added-flash').show()
//             setTimeout(function () {
//                 $('.item-wishlist-not-added-flash').fadeOut('fast');
//             }, 5000);
//         });
//
//     }
//
// ]);
//
//
// app.controller('CartController', ['$scope', '$rootScope', '$http', '$timeout', '$window',
//     function ($scope, $rootScope, $http, $timeout, $window) {
//
//         $scope.init = function () {
//
//             $http({
//                 url: $window.CART_NOTI_URL,
//                 method: 'GET',
//                 headers: {
//                     "Content-Type": "application/json"
//                 },
//                 params: '',
//                 data: '',
//             }).then(function successCallback(resp) {
//                 $scope.cart_id = resp.data.cart_id
//                 $scope.sub_total = resp.data.sub_total
//                 $scope.delivery_charge = resp.data.delivery_charge
//                 $scope.total_quantity = resp.data.total_quantity
//                 $scope.cart_items = resp.data.cart_items
//
//             }, function failureCallback(resp) {
//             })
//         }
//
//         $scope.clearCart = function () {
//
//             $http({
//                 url: "/clear_cart_items/",
//                 method: 'POST',
//                 'Content-Type': 'application/json',
//                 data: $scope.cart_id
//             }).then(function successCallback(resp) {
//                 $scope.sub_total = resp.data.sub_total
//                 $scope.delivery_charge = resp.data.delivery_charge
//                 $scope.total_quantity = resp.data.total_quantity
//                 $scope.cart_items = []
//                 $rootScope.$broadcast('item.all_clear', 'not added');
//             }, function failureCallback(resp) {
//             })
//
//         };
//
//
//         $rootScope.$on('item.added', function (event, data) {
//             $scope.init();
//         });
//
//
//         $rootScope.$on('item.updated', function (event, data) {
//             $scope.init();
//         });
//
//         $rootScope.$on('item.all_clear', function (event, data) {
//             $scope.init();
//         });
//
//
//     }
// ]);
//
//
// app.controller('CartItemController', ['$scope', '$rootScope', '$http', '$timeout', '$window', '$element',
//     function ($scope, $rootScope, $http, $timeout, $window, $element) {
//
//         $scope.product = {}
//         $scope.added = false;
//
//         $scope.init = function (obj) {
//             $scope.obj = obj
//             $scope.newObj = {}
//             // $scope.newObj.variant = $scope.obj.cart_choices[0]
//             $scope.newObj.quantity = 1
//
//         }
//
//         // $scope.initItem = function(obj) {
//         // 	$scope.obj = obj
//         // 	$scope.obj._total = $scope.obj.variant_price * $scope.obj.quantity
//         // 	$scope.$watch('obj.quantity', function(curr) {
//         // 		$scope.obj._total = $scope.obj.price * curr
//         // 	})
//         // }
//         //
//         //
//         $scope.addProductToWishList = function () {
//             $http({
//                 url: $window.ADD_TO_WISHLIST_URL + $scope.obj.id + "/",
//                 method: 'POST',
//                 'Content-Type': 'application/www-url-form-encoded',
//                 data: $scope.obj
//             }).then(function successCallback(resp) {
//                 $scope.obj.transmitting = false
//                 if (resp.data.status == "success") {
//                     $scope.obj.added_to_wishlist = true;
//                     $rootScope.$broadcast('item.added_wishlist', 'i have been added')
//                 }
//                 else {
//                     $rootScope.$broadcast('item.not_added_wishlist', 'not added')
//                 }
//             }, function failureCallback(resp) {
//             });
//
//         }
//
//         $scope.updateUniCartItem = function () {
//             $scope.obj.transmitting = true
//
//             $http({
//                 url: $window.CART_ITEM_UPDATE,
//                 method: 'POST',
//                 'Content-Type': 'application/json',
//                 data: $scope.obj
//             }).then(function successCallback(resp) {
//                 $scope.obj.transmitting = false
//                 $rootScope.$broadcast('item.updated', 'i have been updated')
//             }, function failureCallback(resp) {
//             })
//         }
//
//
//         $scope.deleteUniCartItem = function () {
//
//             $scope.obj.transmitting = true
//             $scope.obj.quantity = 0
//
//             $http({
//                 url: $window.UNICART_ITEM_URL,
//                 method: 'POST',
//                 'Content-Type': 'application/json',
//                 data: $scope.obj
//             }).then(function successCallback(resp) {
//                 $scope.obj.transmitting = false
//                 $rootScope.$broadcast('item.updated', 'i have been updated')
//             }, function failureCallback(resp) {
//             })
//         }
//
//     }
//
// ]);
//
