__author__ = 'kunsam002'

"""
signals.py

All signals required within the application.
These signals will be used to interconnect different parts of the application.
It will also be used by 3rd party applications as a connection mechanism
"""

from blinker import Namespace

app_signals = Namespace()

contact_message = app_signals.signal('contact_message')
contact_message_replied = app_signals.signal('contact_message_replied')
user_created = app_signals.signal('user_created')
calculate_cart_total = app_signals.signal('calculate_cart_total')
user_password_reset = app_signals.signal('user_password_reset')
password_reset_successful = app_signals.signal('password_reset_successful')
order_placed = app_signals.signal('order_placed')