"""
public.py

@Author: Olukunle Ogunmokun
@Date: 10th Dec, 2018

The public views required to sign up and get started
"""

from flask import Blueprint, render_template, abort, redirect, \
    flash, url_for, request, session, g, make_response, current_app, jsonify, g
from flask_login import logout_user, login_required, login_user, current_user
from datetime import date, datetime, timedelta
from stew.models import *
from sqlalchemy import asc, desc, or_, and_, func
from stew.forms import *
from stew.core import utils, templating
from stew.signals import *
import time
import json
import urllib
from flask_principal import Principal, Identity, AnonymousIdentity, identity_changed, PermissionDenied
from stew.services.authentication import authenticate, authenticate_forgot_password
from stew.services import assets
from stew.services.management import UserService, AdminMessageService, MenuReviewService
from stew.services import checkout as checkout_service
from stew.core import printing
import base64
import requests
import xmltodict
import os
import sys
from random import randrange
from pprint import pprint
import cgi
import hashlib
from htmlmin import minify

login_manager = app.login_manager

www = Blueprint('public', __name__)

ignored_args = ['id', 'date_created', 'last_updated']

captcha_verf_secret = "6Lfv0VkUAAAAAHe-8nm5H0XFJ3OOatgqI0jApcL4"
captcha_verf_url = "https://www.google.com/recaptcha/api/siteverify"

captcha_verf_secret = "6Lfv0VkUAAAAAHe-8nm5H0XFJ3OOatgqI0jApcL4"
captcha_verf_url = "https://www.google.com/recaptcha/api/siteverify"

contact_email = app.config.get("CONTACT_EMAIL")
contact_phone = app.config.get("CONTACT_PHONE")


@app.errorhandler(404)
def page_not_found(e):
    title = "404- Page Not Found"
    error_number = "404"
    error_title = "Page not found!"
    error_info = "The requested page cannot be found or does not exist. Please contact the Administrator."

    return minify(render_template('main/error.html', **locals()), 404)


@app.errorhandler(500)
def internal_server_error(e):
    title = "500- Internal Server Error"
    error_number = "500"
    error_title = "Server Error!"
    error_info = "There has been an Internal server Error. Please try again later or Contact the Administrator."

    return minify(render_template('main/error.html', **locals()), 500)


def object_length(data):
    return len(data)


def _list_sum(data):
    return sum(data)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def _calculate_cart_total(cart_id, **kwargs):
    cart = Cart.query.get(cart_id)
    if not cart:
        raise Exception("Cart does not exist")

    cart.total
    return cart


def delete_cart_item(id):
    item = CartItem.query.get(id)
    cart_id = item.cart_id
    if item:
        db.session.query(CartItemExtra).filter(CartItemExtra.cart_item_id == id).delete()
        db.session.commit()

        db.session.delete(item)
        db.session.commit()

        _calculate_cart_total(cart_id)

        flash("success", "Recipe successfully removed from cart.")

    return True


def update_cart_item_value(id, menu_size_code):
    item = CartItem.query.get(id)
    menu_size = MenuSize.query.get(menu_size_code)

    item = checkout_service.CartItemService.update(item.id, menu_size_code=menu_size.code)

    item.calculate_total()

    _calculate_cart_total(item.cart_id)

    flash("success", "Recipe successfully updated.")

    return item


def get_cart():
    cart = Cart.query.filter(Cart.user_id == current_user.id).first()
    if not cart:
        return 0, []

    cart_items = CartItem.query.filter(CartItem.user_id == current_user.id, CartItem.cart_id == cart.id).all()

    return cart.total_quantity, cart_items


def fetch_cart():
    cart = None
    if current_user.is_authenticated:
        cart = Cart.query.filter(Cart.user_id == current_user.id).first()

    return cart


def group_cart_data():
    cart = fetch_cart()
    if not cart:
        return {"delivery_charge": 0, "sub_total": 0,
                "total_quantity": 0, "cart_items": []}
    items = CartItem.query.filter(CartItem.cart_id == cart.id).order_by(desc(CartItem.id)).limit(3).all()

    data = {"delivery_charge": cart.delivery_charge, "sub_total": cart.cart_total,
            "total_quantity": cart.total_quantity}
    items_list = []
    for i in items:
        extra = CartItemExtra.query.filter(CartItemExtra.cart_item_id == i.id).first()
        d = {"cover_image_url": i.menu.cover_image.url if i.menu.cover_image else None,
             "menu_name": i.menu.name,
             "menu_url": url_for('.recipe', id=i.menu_id, slug=i.menu.slug),
             "menu_size": i.menu_size.name,
             "category": i.menu.menu_category.name,
             "protein": extra.protein.name if extra and extra.protein else None,
             "side_dish": extra.side_dish.name if extra and extra.side_dish else None,
             "total": i.total}
        items_list.append(d)

    data["cart_items"] = items_list

    return data


@www.context_processor
def main_context():
    """ Include some basic assets in the startup page """

    today = date.today()
    current_year = today.strftime('%Y')
    minimum = min
    string = str
    number_format = utils.number_format
    length = object_length
    join_list = utils.join_list
    slugify = utils.slugify
    clean_ascii = utils.clean_ascii
    subscription_complete = app.config.get("SUBSCRIPTION_COMPLETE")
    paging_url_build = utils.build_page_url

    list_sum = _list_sum
    stew_contact_email = contact_email
    stew_contact_phone = contact_phone

    canonical_url = app.config.get("CANONICAL_URL")

    return locals()


@www.route('/cart_notification/')
def cart_notification():
    cart_data = group_cart_data()
    content_type = request.headers.get("Content-Type")
    if content_type == "application/json":
        data = dict(cart_data)

        # All the math needed will be computed here. Total by shop, shop_ids, etc

        return jsonify(data)

    return jsonify({})


@www.route('/header_data/')
def header_data():
    data = {}
    content_type = request.headers.get("Content-Type")
    if content_type == "application/json":
        if current_user.is_authenticated:
            data = group_cart_data()

        return jsonify(data)

    return jsonify({})


@www.route('/login/', methods=["GET", "POST"])
def login():
    next_url = request.args.get("next") or url_for(".index")

    form = LoginForm()

    if form.validate_on_submit():
        data = form.data
        username = data["username"]
        password = data["password"]
        user = authenticate(username, password)

        if user is not None:
            login_user(user, remember=True, force=True)  # This is necessary to remember the user

            identity_changed.send(app, identity=Identity(user.id))

            # include the username and api_token in the session

            # logger.info(next_url)
            resp = redirect(next_url)

            # Transfer auth token to the frontend for use with api requests
            key = "%s:%s" % (user.id, user.get_auth_token())
            __xcred = base64.b64encode(key.encode())

            resp.set_cookie("__xcred", __xcred)
            resp.set_cookie("current_user_id", value=str(user.id))
            session.permanent = True

            user.update_last_login()
            flash("success", "Welcome back to STEW!, we are here to help you Save Time, Eat Well.")
            return resp

        else:
            login_error = "The username or password is invalid"

    return minify(render_template("main/login.html", **locals()))


@www.route('/forgot_password/', methods=["GET", "POST"])
def forgot_password():
    next_url_ = request.args.get("next_url") or url_for(".index")
    form = ForgotPasswordForm()
    if form.validate_on_submit():
        data = form.data
        username = data["username"]
        user = authenticate_forgot_password(username)

        if user is not None:
            token = UserService.request_new_user_password(user.id)
            resp = redirect(url_for(".reset_request_successful"))
            return resp
        else:
            login_error = "This email address or username does not exist"

    return minify(render_template("main/forgot_password.html", **locals()))


# recover password page
@www.route('/reset_request_successful/')
def reset_request_successful():
    return minify(render_template("main/reset_request_successful.html", **locals()))


# recover password page
@www.route('/recover_password/<string:code>/', methods=["GET", "POST"])
def recover_password(code):
    reset_token = PasswordResetToken.query.filter(PasswordResetToken.code == code).first()
    user = User.query.get(reset_token.user_id)

    if not reset_token:
        flash("error", "Invalid Password Reset Token... Please request for another token.")
        return redirect(url_for('.index'))

    if reset_token.is_expired:
        flash("warning", "Password Reset Token Expired... Please request for another token.")
        return redirect(url_for('.index'))

    if not user:
        abort(404)

    form = ResetPasswordForm()

    if form.validate_on_submit():
        password = form.data["password"]

        user = UserService.reset_password(user.id, password)
        reset_token.is_expired = True
        db.session.add(reset_token)
        db.session.commit()

        if user is not None:
            login_user(user, remember=True, force=True)  # This is necessary to remember the user

            # include user roles into scope
            # identity_changed.send(app, identity=Identity(user.id))
            # next_url = request.args.get("next", url_for(".index"))
            next_url = url_for(".reset_success")
            resp = redirect(next_url)
            # Transfer auth token to the frontend for use with api requests

            key = "%s:%s" % (user.id, user.get_auth_token())
            __xcred = base64.b64encode(key.encode())

            resp.set_cookie("__xcred", __xcred)

            user.update_last_login()

            session.permanent = True
            email_data = dict(email=user.email, name=user.name)
            password_reset_successful.send(user.id, **email_data)
            return resp
        return redirect(url_for(".reset_success"))

    return minify(render_template("main/reset_password.html", **locals()))


# recover password page
@www.route('/reset_success/')
def reset_success():
    return minify(render_template("main/reset_successful.html", **locals()))


# logout page
@www.route('/logout/')
def logout():
    logout_user()

    # Remove session keys set by Flask-Principal
    for key in ('identity.name', 'identity.auth_type'):
        session.pop(key, None)

    # Tell Flask-Principal the user is anonymous
    identity_changed.send(app, identity=AnonymousIdentity())

    resp = redirect(url_for('.index'))
    resp.set_cookie('current_user_id', "", expires=0)
    session.permanent = False
    return resp


@www.route('/signup/', methods=["GET", "POST"])
def signup():
    next_url = request.args.get("next") or url_for(".index")

    if current_user.is_authenticated:
        return redirect(next_url)

    form = SignUpForm()

    if form.validate_on_submit():
        data = form.data
        data["password"] = data.get("new_password")
        if User.query.filter(User.email != None, User.email == data.get("email")).count() > 0:
            login_error = "The email already exist."
            return minify(render_template("main/signup.html", **locals()))

        if User.query.filter(User.phone != None, User.phone == data.get("phone")).count() > 0:
            login_error = "The phone number already exist."
            return minify(render_template("main/signup.html", **locals()))

        user = UserService.create(**data)

        if user is not None:
            login_user(user, remember=True, force=True)  # This is necessary to remember the user

            identity_changed.send(app, identity=Identity(user.id))

            # include the username and api_token in the session

            # logger.info(next_url)
            resp = redirect(next_url)

            # Transfer auth token to the frontend for use with api requests
            # __xcred = base64.b64encode("%s:%s" % (user.id, user.get_auth_token()))
            key = "%s:%s" % (user.id, user.get_auth_token())
            __xcred = base64.b64encode(key.encode())

            resp.set_cookie("__xcred", __xcred)
            resp.set_cookie("current_user_id", value=str(user.id))
            session.permanent = True

            flash("success", "Welcome to STEW!, we are here to help you Save Time, Eat Well.")
            user_created.send(user.id)
            return resp

        else:
            login_error = "The username or password is invalid"

    return minify(render_template("main/signup.html", **locals()))


@www.route('/about-us/')
def about():
    page_title = "About Us"
    return minify(render_template("main/about.html", **locals()))


@www.route('/contact-us/', methods=['GET', 'POST'])
def contact():
    page_title = "Contact Us"
    form = ContactForm()

    if current_user:
        form = ContactForm(obj=current_user)

    if form.validate_on_submit():
        data = form.data
        if current_user.is_authenticated:
            data["user_id"] = current_user.id
        else:
            ref_user = User.query.filter(User.email == data.get("email", "")).first()
            if ref_user:
                data["user_id"] = ref_user.id

        obj = AdminMessageService.create(**data)

        flash("success", "Your message has been sent! Thank you")
        return redirect(url_for(".contact"))
    return minify(render_template('main/contact.html', **locals()))


@www.route('/contact/sending/', methods=['GET', 'POST'])
def contact_sending():
    if request.method == "POST":
        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)
        contact_form = ContactForm(obj=_data)
        if contact_form.validate_on_submit():
            data = contact_form.data
            u = User.query.filter(User.email == data.get("email")).first()
            if u:
                data["user_id"] = u.id
            AdminMessageService.create(**data)

            _data = {"status": "success", "message": "Message Sent successfully"}
            data = jsonify(_data)
            response = make_response(data)
            response.headers['Content-Type'] = "application/json"
            return response
        else:
            _data = {"status": "failure", "message": "Form Validation failed"}
            data = jsonify(_data)
            response = make_response(data)
            response.headers['Content-Type'] = "application/json"
            return response

    else:
        data = {"Status": "failure", "message": "Request Method not Allowed"}
        data = jsonify(data)
        response = make_response(data)
        response.headers['Content-Type'] = "application/json"
        return response


@www.route('/add_to_cart/', methods=['GET', 'POST'])
def add_to_cart():
    if request.method == "GET":
        return redirect(url_for('.index'))
    user = current_user

    next_url_ = request.args.get("next") or url_for(".index")

    cart_form = CartForm()
    cart_form.protein_id.choices = [(i.id, i.name) for i in Protein.query.all()]
    cart_form.side_dish_id.choices = [(0, "--- Select One ---")] + [(i.id, i.name) for i in SideDish.query.all()]
    cart_form.menu_size_code.choices = [(i.code, i.name) for i in MenuSize.query.filter(MenuSize.enabled == True).all()]
    cart_form.menu_condition_code.choices = [(i.code, i.name) for i in
                                             MenuCondition.query.order_by(MenuCondition.name).all()]

    if cart_form.validate_on_submit():
        data = cart_form.data
        data["quantity"] = 1
        checkout_service.CartService.add_item(user.id, **data)

        flash("success", "Recipe has been added to your Cart successfully")

        return redirect(next_url_ + "?&added=y")
    else:
        data = cart_form.data
        menu_size_code = data.get("menu_size_code", None)

        if menu_size_code in ["None", "", None, 0]:
            flash("warning", "Kindly Select valid Number of People (Menu Size) to proceed with adding to Cart")

    return redirect(next_url_)


@www.route('/add_review/', methods=['GET', 'POST'])
def add_review():
    if request.method == "GET":
        return redirect(url_for('.index'))
    user = current_user

    next_url_ = request.args.get("next") or url_for(".index")
    menu_id = request.args.get("menu_id", None)
    if not menu_id:
        return redirect(next_url_)

    review_form = MenuReviewForm()
    if review_form.validate_on_submit():
        data = review_form.data
        data["user_id"] = user.id
        data["menu_id"] = menu_id
        obj = MenuReviewService.create(**data)
        if obj:
            flash("success", "Thank you for your review")
        return redirect(next_url_)

    return redirect(next_url_)


@www.route('/update_cart_item/', methods=['GET', 'POST'])
def update_cart_item():
    if request.method == "GET":
        return redirect(url_for('.index'))

    update_item_form = UpdateCartItemForm()
    update_item_form.menu_size.choices = [("zero", "0")] + [(i.code, i.value) for i in MenuSize.query.all()]

    if update_item_form.validate_on_submit():
        data = update_item_form.data
        menu_size = data.get("menu_size")
        item_id = data.get("item_id")

        if menu_size == "zero":
            delete_cart_item(int(data.get('item_id')))
        else:
            update_cart_item_value(item_id, menu_size)

    else:
        flash("error", "Failed to remove recipe from cart")

    return redirect(url_for('.cart'))


@www.route('/calculate_recipe_cost/', methods=['GET', 'POST'])
def calculate_recipe_cost():
    if request.method != "POST":
        data = {"Status": "failure", "message": "Request Method not Allowed"}
        data = jsonify(data)
        response = make_response(data)
        response.headers['Content-Type'] = "application/json"
        return response

    _data = {}
    if request.data:
        _data = request.data
        _data = json.loads(_data)
    elif request.form:
        _data = json.dumps(request.form)
        _data = json.loads(_data)

    recipe = _data.get("recipe")
    condition = _data.get("condition", "uncooked")
    protein_id = _data.get("protein", 0)
    side_dish_id = _data.get("side_dish", 0)
    size = _data.get("size", "")

    menu_size = None

    menu = Menu.query.get(int(recipe))
    menu_cost = menu.uncooked_cost if condition == "uncooked" else menu.default_cost

    is_cooked = True if condition == "cooked" else False

    protein = side_dish = menu_size = None

    if protein_id not in ["", "0", " ", 0]:
        protein = Protein.query.get(protein_id)

    if side_dish_id not in ["", "0", " ", 0]:
        side_dish = SideDish.query.get(side_dish_id)

    if size not in ["", "0", " ", 0]:
        menu_size = MenuSize.query.filter(MenuSize.code == size, MenuSize.is_cooked == is_cooked).first()

    if not menu_size:
        payload = [dict(total=0.00)]

        data = jsonify(payload)
        response = make_response(data)
        response.headers['Content-Type'] = "application/json"
        return response

    size_multiplier = menu_size.cost_multiplier if menu_size else 0
    protein_multiplier = menu_size.protein_cost_multiplier if menu_size and menu_size.protein_cost_multiplier else 0
    side_dish_multiplier = menu_size.side_dish_cost_multiplier if menu_size and menu_size.side_dish_cost_multiplier else 0

    menu_cost = menu_cost if menu_cost else 0

    protein_price = protein.price if protein else 0
    dish_price = side_dish.price if side_dish else 0

    protein_total = protein_price * protein_multiplier
    side_dish_total = dish_price * side_dish_multiplier

    extra_total = protein_total + side_dish_total

    menu_total = size_multiplier * menu_cost

    total = menu_total + extra_total

    payload = [dict(total=utils.number_format(total))]

    data = jsonify(payload)
    response = make_response(data)
    response.headers['Content-Type'] = "application/json"
    return response


@www.route('/search/')
def search():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        category_code = request.args.get("category", None)
        search_q = request.args.get("q", None)
    except:
        abort(404)
    page_title = "Search Results"
    page_description = "Search Results Page"

    request_args = utils.copy_dict(request.args, {})

    search_page = True

    categories = MenuCategory.query.order_by(MenuCategory.name).all()
    # cat_codes = [i.menu_category_code for i in query.all()]
    # categories = MenuCategory.query.filter(MenuCategory.code.in_(list(set(cat_codes)))).all()

    if category_code:
        query = Menu.query.filter(Menu.is_visible == True,
                                  Menu.menu_category_code == category_code)
    else:
        query = Menu.query.filter(Menu.is_visible == True)

    if search_q:
        search_q = search_q.strip()
        queries = [Menu.name.ilike("%%%s%%" % search_q), Menu.menu_category_code.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))

    results = query.paginate(page, 24, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return minify(render_template("main/recipes.html", **locals()))


@www.route('/recipes/')
def recipes():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        category_code = request.args.get("category", None)
    except:
        abort(404)

    page_title = "Recipes and Cooked Meals"
    page_description = "Collection of Recipes and Cooked Meals"

    request_args = utils.copy_dict(request.args, {})

    all_recipes = True

    query = Menu.query.filter(Menu.is_visible == True)

    results = query.paginate(page, 24, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return minify(render_template("main/recipes.html", **locals()))


@www.route('/recipes/classic/')
def classic_recipes():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        category_code = request.args.get("category", None)
    except:
        abort(404)

    recipe_section = "classic"

    page_title = "Classic Recipes"
    page_description = "Collection of Classic Recipes"

    request_args = utils.copy_dict(request.args, {})

    categories = MenuCategory.query.filter(MenuCategory.is_special != True).order_by(MenuCategory.name).all()
    if category_code:
        query = Menu.query.filter(Menu.is_visible == True, Menu.is_special != True, Menu.support_uncooked == True,
                                  Menu.menu_category_code == category_code)
    else:
        query = Menu.query.filter(Menu.is_visible == True, Menu.is_special != True, Menu.support_uncooked == True)

    results = query.paginate(page, 24, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return minify(render_template("main/recipes.html", **locals()))


@www.route('/recipes/special/')
def specials():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        category_code = request.args.get("category", None)
    except:
        abort(404)

    page_title = "Special Recipes"
    page_description = "Collection of Special Recipes"

    recipe_section = "special"

    request_args = utils.copy_dict(request.args, {})

    categories = MenuCategory.query.filter(MenuCategory.is_special == True).order_by(MenuCategory.name).all()

    if category_code:
        query = Menu.query.filter(Menu.is_visible == True, Menu.is_special == True, Menu.support_uncooked == True,
                                  Menu.menu_category_code == category_code)
    else:
        query = Menu.query.filter(Menu.is_visible == True, Menu.is_special == True, Menu.support_uncooked == True)

    results = query.paginate(page, 24, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return minify(render_template("main/recipes.html", **locals()))


@www.route('/recipes/cooked/')
def cooked_meals():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    page_title = "Cooked Meals"
    page_description = "Collection of Cooked Meals"

    recipe_section = "cooked"
    category_code = None

    request_args = utils.copy_dict(request.args, {})

    query = Menu.query.filter(Menu.is_visible == True, Menu.support_cooked == True)

    results = query.paginate(page, 24, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return minify(render_template("main/recipes.html", **locals()))


@www.route('/recipes/<int:id>/<string:slug>/')
def recipe(id, slug):
    menu = assets.MenuService.query.get(id)
    if not menu:
        abort(404)

    page_title = menu.name
    page_description = "{} - ({}) {}".format(menu.name, menu.menu_category_code.upper(), menu.caption)

    og_image = menu.cover_image_url

    support_review = True
    allow_review_creation = False

    if current_user.is_authenticated and id in current_user.ordered_recipes:
        allow_review_creation = True

    images = assets.ImageService.query.filter(Image.menu_id == menu.id, Image.id != menu.cover_image_id).all()
    images = [menu.cover_image] + images

    side_dishes = menu.side_dishes
    protein_query = Protein.query.order_by(Protein.name)
    proteins = protein_query.all()
    protein_count = protein_query.count()

    loop_rounds = int(protein_count / 3) + (1 if protein_count % 3 > 0 else 0)

    review_query = MenuReview.query.filter(MenuReview.menu_id == id)
    reviews = review_query.limit(5).all()
    review_count = review_query.count()

    default_condition = 0
    if menu.support_cooked and menu.support_uncooked:
        menu_conditions = MenuCondition.query.order_by(MenuCondition.name).all()
        sizes = assets.MenuSizeService.query.filter(MenuSize.enabled == True).order_by(MenuSize.name).all()
    elif menu.support_cooked and not menu.support_uncooked:
        menu_conditions = MenuCondition.query.filter(MenuCondition.code == "cooked").order_by(MenuCondition.name).all()
        one_menu_condition = True
        default_condition = "cooked"
        sizes = assets.MenuSizeService.query.filter(MenuSize.enabled == True, MenuSize.is_cooked == True).order_by(
            MenuSize.name).all()
    elif menu.support_uncooked and not menu.support_cooked:
        menu_conditions = MenuCondition.query.filter(MenuCondition.code == "uncooked").order_by(
            MenuCondition.name).all()
        one_menu_condition = True
        default_condition = "uncooked"
        sizes = assets.MenuSizeService.query.filter(MenuSize.enabled == True, MenuSize.is_cooked != True).order_by(
            MenuSize.name).all()

    cart_form = CartForm()
    cart_form.protein_id.choices = [(i.id, i.name) for i in proteins]
    cart_form.side_dish_id.choices = [(0, "--- Select One ---")] + [(i.id, i.name) for i in menu.side_dishes]
    cart_form.menu_size_code.choices = [(i.code, i.name) for i in sizes]
    cart_form.menu_condition_code.choices = [(i.code, i.name) for i in menu_conditions]

    review_form = MenuReviewForm()

    menu_ingredient_ids = menu.ingredient_ids.split(",")
    if len(menu_ingredient_ids) == 1 and menu_ingredient_ids[0] == "":
        ingredients = []
    else:
        ingredients = assets.IngredientService.query.filter(Ingredient.id.in_(menu_ingredient_ids)).all()

    other_recipes = assets.MenuService.query.filter(Menu.id != menu.id, Menu.is_visible == True,
                                                    Menu.menu_category_code == menu.menu_category_code).order_by(
        func.random()).limit(5).all()
    return minify(render_template("main/recipe.html", **locals()))


@www.route('/fetch/item_size_price/', methods=['GET', 'POST'])
def fetch_item_size_price():
    if request.method == "POST":
        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)
        logger.info(_data)

        item_id = _data.get("item_id")
        size_code = _data.get("size_code")

        item = CartItem.query.get(int(item_id))
        total = item.calculate_assumed_total(size_code)

        payload = [dict(total=total)]

        data = jsonify(payload)
        response = make_response(data)
        response.headers['Content-Type'] = "application/json"
        return response

    else:
        data = {"Status": "failure", "message": "Request Method not Allowed"}
        data = jsonify(data)
        response = make_response(data)
        response.headers['Content-Type'] = "application/json"
        return response


@www.route('/cart/')
def cart():
    if not current_user.is_authenticated:
        return redirect(url_for('.index'))

    page_title = "Cart"
    page_description = "Collection of your favourite Cart Items"

    menu_sizes = MenuSize.query.filter(MenuSize.enabled == True).order_by(MenuSize.value).all()
    update_item_form = UpdateCartItemForm()
    update_item_form.menu_size.choices = [("zero", 0)] + [(i.code, i.value) for i in menu_sizes]

    cart_total_quantity, cart_items = get_cart()

    cart = checkout_service.CartService.query.filter(Cart.user_id == current_user.id).first()
    if not cart:
        return redirect(url_for('.index'))
    return minify(render_template("main/cart.html", **locals()))


@www.route('/checkout/', methods=['GET', 'POST'])
def checkout():
    if not current_user.is_authenticated:
        return redirect(url_for('.index'))

    cart = fetch_cart()
    if not cart:
        return redirect(url_for('.index'))

    user = current_user
    if cart and cart.total_quantity <= 0:
        return redirect(url_for('.recipes'))

    page_title = "Checkout"

    cart_items = CartItem.query.filter(CartItem.cart_id == cart.id).order_by(CartItem.id).all()

    countries = assets.CountryService.query.filter(Country.enabled == True).all()
    states = assets.StateService.query.filter(State.enabled == True).all()

    form = CheckoutForm(obj=cart, address=cart.address, additional_info=cart.additional_info)

    form.country_code.choices = [(0, "---Select One---")] + [(i.code, i.name) for i in
                                                             countries]
    form.state_code.choices = [(0, "---Select One---")] + [(i.code, i.name) for i in
                                                           states]

    if form.validate_on_submit():
        data = form.data
        data["email"] = user.email
        data["phone"] = user.phone
        cart = checkout_service.CartService.update(cart.id, **data)
        checkout_service.CartService.calculate_delivery(cart.id)

        return redirect(url_for(".checkout_review"))

    return minify(render_template("main/checkout.html", **locals()))


@www.route('/checkout/review/')
def checkout_review():
    if not current_user.is_authenticated:
        return redirect(url_for('.index'))

    rave_pub_key = current_app.config.get("RAVE_PUB_KEY")

    cart = checkout_service.CartService.query.filter(Cart.user_id == current_user.id).first()
    if not cart:
        return redirect(url_for('.index'))

    page_title = "Review Checkout"

    cart = _calculate_cart_total(cart.id)

    cart_total_quantity, cart_items = get_cart()

    return minify(render_template("main/checkout-review.html", **locals()))


@www.route('/cart_rave_success/', methods=['GET', 'POST'])
def cart_rave_success():
    _data = {}
    if request.data:
        _data = request.data
        _data = json.loads(_data)
    elif request.form:
        _data = json.dumps(request.form)
        _data = json.loads(_data)

    cart = fetch_cart()

    _data["charge_response_code"] = _data.get("chargeResponseCode", "")
    _data["charge_response_message"] = _data.get("chargeResponseMessage", "")
    _data["auth_model_used"] = _data.get("authModelUsed", "")
    user_id = _data.get("actor", "")

    form = RavePayCheckoutForm(csrf_enabled=False, **_data)
    if form.validate_on_submit():
        data = form.data
        data["cart_id"] = cart.id
        data["user_id"] = user_id
        data["payment_option_code"] = "online"
        obj = checkout_service.RaveTransactionService.create_success(**data)
    else:
        logger.info("Errot: Not validated data to RavePayCheckout Form -------- %s" % _data)
        print("Errot: Not validated data to RavePayCheckout Form -------- %s" % _data)
        logger.info(form.errors)
        flash("warning", "Payment Unsuccessful... Please try again later")
        return redirect(url_for('.cart'))
    order = Order.query.filter(Order.rave_transaction_id == obj.id).first()
    if order:
        flash("success", "Payment Successful! Thank you for placing an Order on STEW.")
    return redirect(url_for('.checkout_complete', code=order.code))


@www.route('/checkout/orders/<string:code>#<int:id>/invoice/', methods=['GET', 'POST'])
def print_invoice(id, code):
    page_title = "Order Invoice"

    order = Order.query.get(id)

    if not order:
        abort(404)

    page_title = "Order Invoice #{}".format(code)

    items = OrderEntry.query.filter(OrderEntry.order_id == id).all()

    transaction = Transaction.query.filter(Transaction.order_id == order.id).order_by(desc(Transaction.id)).first()
    return printing.return_pdf("main/print_invoice.html", **locals())


@www.route('/cart_rave_failed/', methods=['GET', 'POST'])
def cart_rave_failed():
    _data = {}
    if request.data:
        _data = request.data
        _data = json.loads(_data)
    elif request.form:
        _data = json.dumps(request.form)
        _data = json.loads(_data)

    _data["charge_response_code"] = _data.get("chargeResponseCode", "")
    _data["charge_response_message"] = _data.get("chargeResponseMessage", "")
    _data["auth_model_used"] = _data.get("authModelUsed", "")
    user_id = _data.get("actor", "")

    form = RavePayCheckoutForm(csrf_enabled=False, **_data)
    if form.validate_on_submit():
        data = form.data
        data["cart_id"] = cart.id
        data["user_id"] = user_id
        data["payment_option_code"] = "online"

        checkout_service.RaveTransactionService.create_failure(**data)
    else:
        logger.info("Errot: Not validated data to RavePayCheckout Form -------- %s" % _data)
        print("Errot: Not validated data to RavePayCheckout Form -------- %s" % _data)
        logger.info(form.errors)
    flash("warning", "Payment Unsuccessful... Please try again later")
    return redirect(url_for('.cart'))


@www.route('/checkout/complete/', methods=['GET', 'POST'])
def checkout_complete():
    order = Order.query.filter(Order.code == request.args.get("code")).first()
    if not order:
        abort(404)

    if order.user_id != current_user.id:
        abort(404)

    return minify(render_template("main/checkout-completed.html", **locals()))


@www.route('/checkout/order/<string:code>/invoice/')
def invoice(code):
    order = Order.query.filter(Order.code == code).first()
    if order.user_id != current_user.id:
        abort(404)
    items = OrderEntry.query.filter(OrderEntry.order_id == order.id).all()

    return minify(render_template("main/invoice.html", **locals()))


@www.route('/invoice/')
def demoinvoice():
    return minify(render_template("main/invoice.html", **locals()))


# @www.route('/demoemail/')
# def demoemail():
#     order = Order.query.first()
#     if not order:
#         raise Exception("Order does not exist")
#     canonical_url = app.config.get("CANONICAL_URL")
#     code, full_name = order.code, order.name
#     order_entries = OrderEntry.query.filter(OrderEntry.order_id == order.id).all()
#     return render_template("emails/order.html", **locals())
#

@www.route('/subscription/', methods=['GET', 'POST'])
def subscription():
    form = SubscriptionForm()

    rave_pub_key = current_app.config.get("RAVE_PUB_KEY")

    menu = Menu.query.first()
    images = assets.ImageService.query.filter(Image.menu_id == menu.id, Image.id != menu.cover_image_id).all()
    images = [menu.cover_image] + images

    frequencies = assets.MenuFrequencyService.query.order_by(MenuFrequency.name).all()
    sizes = assets.MenuSizeService.query.filter(MenuSize.enabled == True).order_by(MenuSize.name).all()
    menus = assets.MenuService.query.filter(Menu.allow_subscription == True).all()
    categories = MenuCategory.query.order_by(MenuCategory.name).all()
    side_dishes = SideDish.query.order_by(SideDish.name).all()
    proteins = Protein.query.order_by(Protein.name).all()

    form.country_code.choices = [(0, "---Select One---")] + [(i.code, i.name) for i in
                                                             assets.CountryService.query.filter(
                                                                 Country.enabled == True).all()]
    form.state_code.choices = [(0, "---Select One---")] + [(i.code, i.name) for i in
                                                           assets.StateService.query.filter(
                                                               State.enabled == True).all()]

    form.menu_frequency_code.choices = [(i.code, i.name) for i in frequencies]
    form.menu_size_code.choices = [(i.code, i.name) for i in sizes]
    form.menu_category_code.choices = [(i.code, i.name) for i in categories]

    form.menu_ids.choices = [(i.id, i.name) for i in menus]
    form.menu_option_ids.choices = [(i.id, i.name) for i in menus]

    if form.validate_on_submit():
        data = form.data

        return data

    return minify(render_template("main/subscription.html", **locals()))


@www.route('/faq/')
def faq():
    page_title = "FAQ"
    page_description = "Frequently Asked Questions"

    return minify(render_template("main/faq.html", **locals()))


@www.route('/privacy-policy/')
def privacy_policy():
    page_title = "Privacy Policy"

    return minify(render_template("main/privacy-policy.html", **locals()))


@www.route('/terms-and-condition/')
def terms():
    page_title = "Terms and Condition"

    return minify(render_template("main/terms-condition.html", **locals()))


@www.route('/how-it-works/')
def how_it_works():
    page_title = "How It Works"

    return minify(render_template("main/how-it-works.html", **locals()))


@www.route('/')
# @login_required
def index():
    menu = Menu.query.first()
    featured_menus = Menu.query.filter(Menu.is_featured == True).order_by(Menu.featured_position).all()

    review_query = MenuReview.query
    reviews = review_query.limit(3).all()

    return minify(render_template("main/index.html", **locals()))


@www.route('/email_demo/<string:temp>')
def email_demo(temp=None):
    rand_number = randrange(1, 6)
    if not temp:
        temp = "demo"
    return render_template("emails/%s.html" % temp, **locals())
