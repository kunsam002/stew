"""
public.py

@Author: Olukunle Ogunmokun
@Date: 10th Dec, 2018

The public views required to sign up and get started
"""

from flask import Blueprint, render_template, abort, redirect, \
    flash, url_for, request, session, g, make_response, current_app, jsonify, g
from flask_login import logout_user, login_required, login_user, current_user
from datetime import date, datetime, timedelta
from stew.models import *
from sqlalchemy import asc, desc, or_, and_, func
from stew.forms import *
from stew import handle_uploaded_photos
from stew.schemas import *
from stew.core import utils, templating
from stew.signals import *
import time
import json
import urllib
from flask_principal import Principal, Identity, AnonymousIdentity, identity_changed, PermissionDenied
from stew.services.authentication import authenticate_admin
from stew.services import assets, management, checkout
import base64
import requests
import xmltodict
import os
import sys
import random
from pprint import pprint
import cgi
import hashlib

control = Blueprint('control', __name__)

ignored_args = ['id', 'date_created', 'last_updated']


@app.errorhandler(404)
def page_not_found(e):
    title = "404- Page Not Found"
    error_number = "404"
    error_title = "Page not found!"
    error_info = "The requested page cannot be found or does not exist. Please contact the Administrator."

    return render_template('main/error.html', **locals()), 404


@app.errorhandler(500)
def internal_server_error(e):
    title = "500- Internal Server Error"
    error_number = "500"
    error_title = "Server Error!"
    error_info = "There has been an Internal server Error. Please try again later or Contact the Administrator."

    return render_template('main/error.html', **locals()), 500


def object_length(data):
    return len(data)


def _list_sum(data):
    return sum(data)

def get_unread_message_count():
    return AdminMessage.query.filter(AdminMessage.is_read!=True).count()

@control.context_processor
def main_context():
    """ Include some basic assets in the startup page """

    today = date.today()
    current_year = today.strftime('%Y')
    minimum = min
    string = str
    number_format = utils.number_format
    length = object_length
    join_list = utils.join_list
    slugify = utils.slugify
    clean_ascii = utils.clean_ascii
    paging_url_build = utils.build_page_url

    recent_messages = []

    unread_message_count = get_unread_message_count()

    list_sum = _list_sum

    return locals()


@control.route('/login/', methods=["GET", "POST"])
def login():
    next_url = request.args.get("next") or url_for(".index")

    form = LoginForm()
    if form.validate_on_submit():
        data = form.data
        username = data["username"]
        password = data["password"]
        user = authenticate_admin(username, password)

        if user is not None:
            login_user(user, remember=True, force=True)  # This is necessary to remember the user

            identity_changed.send(app, identity=Identity(user.id))

            # include the username and api_token in the session

            # logger.info(next_url)
            resp = redirect(next_url)

            # Transfer auth token to the frontend for use with api requests
            # __xcred = base64.b64encode("%s:%s" % (user.username, user.get_auth_token()))
            key = "%s:%s" % (user.id, user.get_auth_token())
            __xcred = base64.b64encode(key.encode())

            resp.set_cookie("__xcred", __xcred)
            resp.set_cookie("current_user_id", value=str(user.id))
            session.permanent = True
            user.update_last_login()
            return resp

        else:
            login_error = "The username or password is invalid"

    return render_template("admin/login.html", **locals())


# logout page
@control.route('/fetch_menu_category/', methods=['GET', 'POST'])
def fetch_menu_category():
    if request.method == "POST":
        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)
        logger.info(_data)

        is_special = _data.get("is_special", False)

        payload = [dict(code=i.code, name=i.name) for i in
                   MenuCategory.query.filter(MenuCategory.is_special == is_special).all()]

        data = jsonify(payload)
        response = make_response(data)
        response.headers['Content-Type'] = "application/json"
        return response

    else:
        data = {"Status": "failure", "message": "Request Method not Allowed"}
        data = jsonify(data)
        response = make_response(data)
        response.headers['Content-Type'] = "application/json"
        return response


@control.route('/logout/')
def logout():
    logout_user()

    # Remove session keys set by Flask-Principal
    for key in ('identity.name', 'identity.auth_type'):
        session.pop(key, None)

    # Tell Flask-Principal the user is anonymous
    identity_changed.send(app, identity=AnonymousIdentity())

    resp = redirect(url_for('.index'))
    resp.set_cookie('current_user_id', "", expires=0)
    session.permanent = False
    return resp


@control.route('/')
@login_required
def index():
    user_count = User.query.count()
    order_count = Order.query.count()
    menu_count = Menu.query.count()
    message_count = AdminMessage.query.count()
    return render_template("admin/index.html", **locals())


@control.route('/users/')
@login_required
def users():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = User.query.order_by(desc(User.id))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/accounts/users.html', **locals())


@control.route('/users/create/', methods=["GET", "POST"])
@login_required
def create_user():
    form = SignUpForm()

    if form.validate_on_submit():
        data = form.data

        obj = management.UserService.create(**data)
        if obj:
            flash("success", "User Updated successfully!")
            return redirect(url_for('.users'))
    return render_template('admin/accounts/create_user.html', **locals())


@control.route('/users/<int:obj_id>/update/', methods=["GET", "POST"])
@login_required
def update_user(obj_id):
    obj = User.query.get(obj_id)
    if not obj:
        abort(404)
    form = SignUpForm(obj=obj)

    if form.validate_on_submit():
        data = form.data

        obj = management.UserService.update(obj.id, **data)
        if obj:
            flash("success", "User Updated successfully!")
            return redirect(url_for('.users'))
    return render_template('admin/accounts/create_user.html', **locals())


@control.route('/messages/inbox/')
@control.route('/messages/')
@login_required
def messages():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = AdminMessage.query.order_by(desc(AdminMessage.date_created))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/communications/messages.html', **locals())


@control.route('/messages/<int:obj_id>/')
@login_required
def view_message(obj_id):
    obj = AdminMessage.query.get(obj_id)
    if not obj:
        abort(404)
    responses = AdminMessageResponse.query.filter(AdminMessageResponse.admin_message_id == obj.id).order_by(
        AdminMessageResponse.id).all()
    management.AdminMessageService.update(obj.id, is_read=True)
    return render_template('admin/communications/view_message.html', **locals())


@control.route('/menu_sizes/')
@login_required
def menu_sizes():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = MenuSize.query.order_by(desc(MenuSize.date_created))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/assets/menu_sizes.html', **locals())


@control.route('/menu_sizes/create/', methods=["GET", "POST"])
@login_required
def create_menu_size():
    form = MenuSizeForm()
    if form.validate_on_submit():

        data = form.data
        obj = assets.MenuSizeService.create(**data)

        if obj:
            flash("success", "Menu Size created successfully!")
            return redirect(url_for('.menu_sizes'))

    return render_template('admin/assets/create_menu_size.html', **locals())


@control.route('/menu_sizes/<string:code>/update/', methods=["GET", "POST"])
@login_required
def update_menu_size(code):
    obj = assets.MenuSizeService.get(code)

    form = MenuSizeForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = assets.MenuSizeService.update(obj.code, **data)

        if obj:
            flash("success", "Menu Size updated successfully!")
            return redirect(url_for('.menu_sizes'))

    return render_template('admin/assets/create_menu_size.html', **locals())


@control.route('/menu_sizes/<string:code>/disable/', methods=["GET", "POST"])
@login_required
def disable_menu_size(code):
    title = "Disable Menu Size"
    obj = assets.MenuSizeService.query.get(code)

    prev_url = url_for('.menu_sizes')
    if request.method == "POST":
        resp = assets.MenuSizeService.update(code, enabled=False)
        if resp:
            flash("success", "Menu Size Successfully Disable")
            return redirect(prev_url)
        flash("error", "Failed to Disable Menu Size")

    return render_template('admin/delete.html', **locals())


@control.route('/menu_sizes/<string:code>/enable/', methods=["GET", "POST"])
@login_required
def enable_menu_size(code):
    title = "Activate Menu Size"
    obj = assets.MenuSizeService.query.get(code)

    prev_url = url_for('.menu_sizes')
    if request.method == "POST":
        resp = assets.MenuSizeService.update(code, enabled=True)
        if resp:
            flash("success", "Menu Size Successfully Enabled")
            return redirect(prev_url)
        flash("error", "Failed to Activate Menu Size")

    return render_template('admin/delete.html', **locals())


@control.route('/menu_conditions/')
@login_required
def menu_conditions():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = MenuCondition.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/assets/menu_conditions.html', **locals())


@control.route('/menu_frequencies/')
@login_required
def menu_frequencies():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = MenuFrequency.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/assets/menu_frequencies.html', **locals())


@control.route('/menu_frequencies/create/', methods=["GET", "POST"])
@login_required
def create_menu_frequency():
    form = MenuFrequencyForm()

    if form.validate_on_submit():

        data = form.data
        obj = assets.MenuFrequencyService.create(**data)

        if obj:
            flash("success", "Menu Frequency created successfully!")
            return redirect(url_for('.menu_frequencies'))

    return render_template('admin/assets/create_menu_frequency.html', **locals())


@control.route('/menu_frequencies/<string:code>/update/', methods=["GET", "POST"])
@login_required
def update_menu_frequency(code):
    obj = assets.MenuFrequencyService.get(code)

    form = MenuFrequencyForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = assets.MenuFrequencyService.update(obj.code, **data)

        if obj:
            flash("success", "Menu Frequency created successfully!")
            return redirect(url_for('.menu_frequencies'))

    return render_template('admin/assets/create_menu_frequency.html', **locals())


@control.route('/menu_frequencies/<string:code>/delete/', methods=["GET", "POST"])
@login_required
def delete_menu_frequency(code):
    title = "Delete Menu Frequency"
    obj = assets.MenuFrequencyService.query.get(code)

    prev_url = url_for('.menu_frequencies')
    if request.method == "POST":
        resp = assets.MenuFrequencyService.delete(code)
        if resp:
            flash("success", "Menu Frequency Successfully Deleted")
            return redirect(prev_url)
        flash("error", "Failed to Delete Menu Frequency")

    return render_template('admin/delete.html', **locals())


@control.route('/menu_categories/')
@login_required
def menu_categories():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        search_q = request.args.get("q",None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = MenuCategory.query

    if search_q:
        queries = [MenuCategory.name.ilike("%%%s%%" % search_q), MenuCategory.code.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))


    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/assets/menu_categories.html', **locals())


@control.route('/menu_categories/create/', methods=["GET", "POST"])
@login_required
def create_menu_category():
    form = MenuCategoryForm()

    if form.validate_on_submit():

        data = form.data
        obj = assets.MenuCategoryService.create(**data)

        if obj:
            flash("success", "Menu Category created successfully!")
            return redirect(url_for('.menu_categories'))

    return render_template('admin/assets/create_menu_category.html', **locals())


@control.route('/menu_categories/<string:code>/update/', methods=["GET", "POST"])
@login_required
def update_menu_category(code):
    obj = assets.MenuCategoryService.get(code)

    form = MenuCategoryForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = assets.MenuCategoryService.update(obj.code, **data)

        if obj:
            flash("success", "Menu Category created successfully!")
            return redirect(url_for('.menu_categories'))

    return render_template('admin/assets/create_menu_category.html', **locals())


@control.route('/menu_categories/<string:code>/delete/', methods=["GET", "POST"])
@login_required
def delete_menu_category(code):
    title = "Delete Menu Category"
    obj = assets.MenuCategoryService.query.get(code)

    prev_url = url_for('.menu_categories')
    if request.method == "POST":
        resp = assets.MenuCategoryService.delete(code)
        if resp:
            flash("success", "Menu Category Successfully Deleted")
            return redirect(prev_url)
        flash("error", "Failed to Delete Menu Category")

    return render_template('admin/delete.html', **locals())


@control.route('/delivery_options/')
@login_required
def delivery_options():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = DeliveryOption.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/assets/delivery_options.html', **locals())


@control.route('/delivery_options/create/', methods=["GET", "POST"])
@login_required
def create_delivery_option():
    form = DeliveryOptionForm()

    if form.validate_on_submit():

        data = form.data
        obj = assets.DeliveryOptionService.create(**data)

        if obj:
            flash("success", "Delivery Option created successfully!")
            return redirect(url_for('.delivery_options'))

    return render_template('admin/assets/create_delivery_option.html', **locals())


@control.route('/delivery_options/<string:code>/update/', methods=["GET", "POST"])
@login_required
def update_delivery_option(code):
    obj = assets.DeliveryOptionService.get(code)

    form = DeliveryOptionForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = assets.DeliveryOptionService.update(obj.code, **data)

        if obj:
            flash("success", "Delivery Option created successfully!")
            return redirect(url_for('.delivery_options'))

    return render_template('admin/assets/create_delivery_option.html', **locals())


@control.route('/payment_options/')
@login_required
def payment_options():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = PaymentOption.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/assets/payment_options.html', **locals())


@control.route('/payment_options/create/', methods=["GET", "POST"])
@login_required
def create_payment_option():
    form = PaymentOptionForm()

    if form.validate_on_submit():

        data = form.data
        obj = assets.PaymentOptionService.create(**data)

        if obj:
            flash("success", "Payment Option created successfully!")
            return redirect(url_for('.payment_options'))

    return render_template('admin/assets/create_payment_option.html', **locals())


@control.route('/payment_options/<string:code>/update/', methods=["GET", "POST"])
@login_required
def update_payment_option(code):
    obj = assets.PaymentOptionService.get(code)

    form = PaymentOptionForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = assets.PaymentOptionService.update(obj.code, **data)

        if obj:
            flash("success", "Payment Option created successfully!")
            return redirect(url_for('.payment_options'))

    return render_template('admin/assets/create_payment_option.html', **locals())


@control.route('/payment_statuses/')
@login_required
def payment_statuses():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = PaymentStatus.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/assets/payment_statuses.html', **locals())


@control.route('/payment_statuses/create/', methods=["GET", "POST"])
@login_required
def create_payment_status():
    form = PaymentStatusForm()

    if form.validate_on_submit():

        data = form.data
        obj = assets.PaymentStatusService.create(**data)

        if obj:
            flash("success", "Payment Status created successfully!")
            return redirect(url_for('.payment_statuses'))

    return render_template('admin/assets/create_payment_status.html', **locals())


@control.route('/payment_statuses/<string:code>/update/', methods=["GET", "POST"])
@login_required
def update_payment_status(code):
    obj = assets.PaymentStatusService.get(code)

    form = PaymentStatusForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = assets.PaymentStatusService.update(obj.code, **data)

        if obj:
            flash("success", "Payment Status created successfully!")
            return redirect(url_for('.payment_statuses'))

    return render_template('admin/assets/create_payment_status.html', **locals())


@control.route('/delivery_statuses/')
@login_required
def delivery_statuses():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = DeliveryStatus.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/assets/delivery_statuses.html', **locals())


@control.route('/delivery_statuses/create/', methods=["GET", "POST"])
@login_required
def create_delivery_status():
    form = DeliveryStatusForm()

    if form.validate_on_submit():

        data = form.data
        obj = assets.DeliveryStatusService.create(**data)

        if obj:
            flash("success", "Delivery Status created successfully!")
            return redirect(url_for('.delivery_statuses'))

    return render_template('admin/assets/create_delivery_status.html', **locals())


@control.route('/delivery_statuses/<string:code>/update/', methods=["GET", "POST"])
@login_required
def update_delivery_status(code):
    obj = assets.DeliveryStatusService.get(code)

    form = DeliveryStatusForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = assets.DeliveryStatusService.update(obj.code, **data)

        if obj:
            flash("success", "Delivery Status created successfully!")
            return redirect(url_for('.delivery_statuses'))

    return render_template('admin/assets/create_delivery_status.html', **locals())


@control.route('/ingredients/')
@login_required
def ingredients():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        search_q = request.args.get("q", None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Ingredient.query

    if search_q:
        search_q = search_q.strip()
        queries = [Ingredient.name.ilike("%%%s%%" % search_q), Ingredient.code.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/inventory/ingredients.html', **locals())


@control.route('/ingredients/create/', methods=["GET", "POST"])
@login_required
def create_ingredient():
    form = IngredientForm()

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit() and empty_files is False:

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            obj = assets.IngredientService.create(**data)

            images = []
            for upload_ in uploaded_files:
                images = assets.ImageService.create(ingredient_id=obj.id, pub_id="Inventory", **upload_)

            if obj:
                flash("success", "Ingredient created successfully!")
                return redirect(url_for('.ingredients'))

        else:
            flash("error", "Error: Image not found!")
            form.errors["images"] = errors

    return render_template('admin/inventory/create_ingredient.html', **locals())


@control.route('/ingredients/<int:obj_id>/update/', methods=["GET", "POST"])
@login_required
def update_ingredient(obj_id):
    obj = Ingredient.query.get(obj_id)
    if not obj:
        abort(404)
    form = UpdateIngredientForm(obj=obj, data={"cover_image_id": obj.cover_image_id},
                                expiration_date=obj.expiration_date)

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    existing_images = obj.images.all()

    if form.validate_on_submit():

        data = form.data

        removables = data.pop("removables", [])
        cover_image_id = data.pop("cover_image_id")
        obj = assets.IngredientService.update(obj.id, **data)

        if obj:
            # If files are present, attach them to the product images
            if empty_files is False:
                uploaded_files, errors = handle_uploaded_photos(files, (160, 160))
                if uploaded_files:
                    for _d in uploaded_files:
                        _d["alt_text"] = obj.name
                        img = assets.ImageService.create(ingredient_id=obj.id, pub_id="Inventory", **_d)

            for _img_id in removables:
                if obj._cover_image_id != _img_id:
                    assets.ImageService.delete(_img_id)

            if obj._cover_image_id != cover_image_id:
                assets.IngredientService.set_cover_image(obj.id, cover_image_id)

            flash("success", "Ingredient updated successfully!")
            return redirect(url_for('.ingredients'))

    return render_template('admin/inventory/create_ingredient.html', **locals())


@control.route('/recipes/')
@login_required
def recipes():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        page_size = request.args.get("page_size", 20)
        search_q = request.args.get("q", None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Menu.query.order_by(desc(Menu.id))

    if search_q:
        search_q = search_q.strip()
        queries = [Menu.name.ilike("%%%s%%" % search_q), Menu.menu_category_code.ilike("%%%s%%" % search_q),
                   Menu.code.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))

    results = query.paginate(page, page_size, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/inventory/recipes.html', **locals())


@control.route('/recipes/featured/')
@login_required
def featured_recipes():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Menu.query.filter(Menu.is_featured == True).order_by(Menu.featured_position)

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/inventory/featured_recipes.html', **locals())


@control.route('/recipes/featured/<int:obj_id>/remove/', methods=['GET', 'POST'])
@login_required
def remove_featured_recipe(obj_id):
    obj = Menu.query.get(obj_id)

    if not obj:
        abort(404)

    if request.method == "POST":
        obj = assets.MenuService.remove_featured(obj.id)

        if obj and obj.is_featured == False:
            flash("success", "Recipe successfully removed from featured dishes")

    else:
        flash("warning", "Request Method not allowed")

    return redirect(url_for('.featured_recipes'))


@control.route('/recipes/create/', methods=["GET", "POST"])
@login_required
def create_recipe():
    form = MenuForm()

    form.menu_category_code.choices = [(0, "--- Select One ---")] + [(i.code, i.name) for i in
                                                                     MenuCategory.query.all()]
    form.ingredient_ids.choices = [(i.id, i.name) for i in Ingredient.query.all()]
    form.side_dish_ids.choices = [(i.id, i.name) for i in SideDish.query.all()]
    form.menu_size_codes.choices = [(i.code, i.name) for i in MenuSize.query.filter(MenuSize.is_cooked != True).all()]
    form.cooked_menu_size_codes.choices = [(i.code, i.name) for i in
                                           MenuSize.query.filter(MenuSize.is_cooked == True).all()]
    form.menu_frequency_codes.choices = [(i.code, i.name) for i in MenuFrequency.query.all()]

    used_positions = [i.featured_position for i in Menu.query.filter(Menu.is_featured == True).all()]
    available_positions = [i for i in range(1, 13) if i not in used_positions]

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit() and empty_files is False:

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            data["allow_one_off"] = True
            data["support_uncooked"] = True
            obj = assets.MenuService.create(**data)

            images = []
            for upload_ in uploaded_files:
                images = assets.ImageService.create(menu_id=obj.id, pub_id="Inventory", **upload_)

            if obj:
                flash("success", "Recipe created successfully!")
                return redirect(url_for('.recipes'))

        else:
            flash("error", "Error: Image not found!")
            form.errors["images"] = errors

    return render_template('admin/inventory/create_recipe.html', **locals())


@control.route('/recipes/<int:obj_id>/update/', methods=["GET", "POST"])
@login_required
def update_recipe(obj_id):
    obj = Menu.query.get(obj_id)

    if not obj:
        abort(404)

    print("-----ingr side", obj.ingredient_ids, obj.side_dish_ids)

    existing_images = obj.images.all()

    disable_delete_action = False

    tot_in_cart = CartItem.query.filter(CartItem.menu_id==obj_id).count()
    tot_in_order = OrderEntry.query.filter(OrderEntry.menu_id==obj_id).count()

    if tot_in_cart+tot_in_order >0:
        disable_delete_action =True

    try:
        form = UpdateMenuForm(obj=obj, data={"cover_image_id": obj.cover_image_id}, is_visible=obj.is_visible,
                              side_dish_ids=[(i.id, i.name) for i in obj.side_dishes],
                              ingredient_ids=[(i.id, i.name) for i in obj.ingredients])
    except:
        form = UpdateMenuForm(obj=obj, data={"cover_image_id": obj.cover_image_id})

    form.menu_category_code.choices = [(0, "--- Select One ---")] + [(i.code, i.name) for i in
                                                                     MenuCategory.query.all()]
    form.ingredient_ids.choices = [(i.id, i.name) for i in Ingredient.query.all()]
    form.side_dish_ids.choices = [(i.id, i.name) for i in SideDish.query.all()]
    form.menu_size_codes.choices = [(i.code, i.name) for i in MenuSize.query.all()]
    form.menu_frequency_codes.choices = [(i.code, i.name) for i in MenuFrequency.query.all()]
    form.removables.choices = \
        [(0, "---- Select One ----")] + [(l.id, l.name) for l in existing_images]

    used_positions = [i.featured_position for i in Menu.query.filter(Menu.is_featured == True).all()]
    available_positions = [i for i in range(1, 13) if i not in used_positions]

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit():

        data = form.data

        if len(data.get("ingredient_ids", [])) == 0:
            data["ingredient_ids"] = [int(i) for i in obj.ingredient_ids.split(",") if i and int(i)]

        if len(data.get("side_dish_ids", [])) == 0:
            data["side_dish_ids"] = [int(i) for i in obj.side_dish_ids.split(",") if i and int(i)]

        removables = data.pop("removables", [])
        cover_image_id = data.pop("cover_image_id")
        obj = assets.MenuService.update(obj.id, **data)

        if obj:
            # If files are present, attach them to the product images
            if empty_files is False:
                uploaded_files, errors = handle_uploaded_photos(files, (160, 160))
                if uploaded_files:
                    for _d in uploaded_files:
                        _d["alt_text"] = obj.name
                        img = assets.ImageService.create(menu_id=obj.id, pub_id="Inventory", **_d)

            for _img_id in removables:
                if obj._cover_image_id != _img_id:
                    assets.ImageService.delete(_img_id)

            if obj._cover_image_id and obj._cover_image_id != cover_image_id:
                assets.MenuService.set_cover_image(obj.id, cover_image_id=cover_image_id)

            flash("success", "Recipe updated successfully!")
            return redirect(url_for('.recipes'))

    return render_template('admin/inventory/create_recipe.html', **locals())


@control.route('/recipes/<int:obj_id>/delete/', methods=["GET", "POST"])
@login_required
def delete_recipe(obj_id):
    title = "Delete Recipe"
    obj = assets.MenuService.query.get(obj_id)

    prev_url = url_for('.recipes')

    if request.method == "POST":

        if OrderEntry.query.filter(OrderEntry.menu_id == obj.id).count() > 0:
            flash("warning", "Recipe cannot be deleted because it is associated to an existing Order")
            return redirect(prev_url)

        passed, status, message = assets.MenuService.delete(obj_id)
        if passed:
            flash(status, message)
            return redirect(prev_url)

        flash("error", "Failed to Delete Recipe")

    return render_template('admin/delete.html', **locals())


@control.route('/proteins/')
@login_required
def proteins():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        search_q = request.args.get("q", None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Protein.query

    if search_q:
        search_q = search_q.strip()
        queries = [Protein.name.ilike("%%%s%%" % search_q), Protein.code.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/inventory/proteins.html', **locals())


@control.route('/proteins/create/', methods=["GET", "POST"])
@login_required
def create_protein():
    form = ProteinForm()

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit() and empty_files is False:

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            obj = assets.ProteinService.create(**data)

            images = []
            for upload_ in uploaded_files:
                images = assets.ImageService.create(protein_id=obj.id, pub_id="Inventory", **upload_)

            if obj:
                flash("success", "Protein created successfully!")
                return redirect(url_for('.proteins'))

        else:
            flash("error", "Error: Image not found!")
            form.errors["images"] = errors

    return render_template('admin/inventory/create_protein.html', **locals())


@control.route('/proteins/<int:obj_id>/update/', methods=["GET", "POST"])
@login_required
def update_protein(obj_id):
    obj = Protein.query.get(obj_id)
    if not obj:
        abort(404)
    form = UpdateProteinForm(obj=obj, data={"cover_image_id": obj._cover_image_id})

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    existing_images = obj.images.all()

    if form.validate_on_submit():

        data = form.data

        removables = data.pop("removables", [])
        cover_image_id = data.pop("cover_image_id")
        obj = assets.ProteinService.update(obj.id, **data)

        if obj:
            # If files are present, attach them to the product images
            if empty_files is False:
                uploaded_files, errors = handle_uploaded_photos(files, (160, 160))
                if uploaded_files:
                    for _d in uploaded_files:
                        _d["alt_text"] = obj.name
                        img = assets.ImageService.create(protein_id=obj.id, pub_id="Inventory", **_d)

            for _img_id in removables:
                if obj._cover_image_id != _img_id:
                    assets.ImageService.delete(_img_id)

            if obj._cover_image_id != cover_image_id:
                assets.ProteinService.set_cover_image(obj.id, cover_image_id)

            flash("success", "Protein updated successfully!")
            return redirect(url_for('.proteins'))

    return render_template('admin/inventory/create_protein.html', **locals())


@control.route('/side_dishes/')
@login_required
def side_dishes():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        search_q = request.args.get("q", None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = SideDish.query

    if search_q:
        search_q = search_q.strip()
        queries = [SideDish.name.ilike("%%%s%%" % search_q), SideDish.code.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/inventory/side_dishes.html', **locals())


@control.route('/side_dishes/create/', methods=["GET", "POST"])
@login_required
def create_side_dish():
    form = SideDishForm()

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit() and empty_files is False:

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            obj = assets.SideDishService.create(**data)

            images = []
            for upload_ in uploaded_files:
                images = assets.ImageService.create(side_dish_id=obj.id, pub_id="Inventory", **upload_)

            if obj:
                flash("success", "Side Dish created successfully!")
                return redirect(url_for('.side_dishes'))

        else:
            flash("error", "Error: Image not found!")
            form.errors["images"] = errors

    return render_template('admin/inventory/create_side_dish.html', **locals())


@control.route('/side_dishes/<int:obj_id>/update/', methods=["GET", "POST"])
@login_required
def update_side_dish(obj_id):
    obj = SideDish.query.get(obj_id)
    if not obj:
        abort(404)
    form = UpdateSideDishForm(obj=obj, data={"cover_image_id": obj.cover_image_id})

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    existing_images = obj.images.all()

    if form.validate_on_submit():

        data = form.data

        removables = data.pop("removables", [])
        cover_image_id = data.pop("cover_image_id")
        obj = assets.SideDishService.update(obj.id, **data)

        if obj:
            # If files are present, attach them to the product images
            if empty_files is False:
                uploaded_files, errors = handle_uploaded_photos(files, (160, 160))
                if uploaded_files:
                    for _d in uploaded_files:
                        _d["alt_text"] = obj.name
                        img = assets.ImageService.create(side_dish_id=obj.id, pub_id="Inventory", **_d)

            for _img_id in removables:
                if obj._cover_image_id != _img_id:
                    assets.ImageService.delete(_img_id)

            if obj._cover_image_id != cover_image_id:
                assets.SideDishService.set_cover_image(obj.id, cover_image_id)

            flash("success", "Side Dish updated successfully!")
            return redirect(url_for('.side_dishes'))

    return render_template('admin/inventory/create_side_dish.html', **locals())


@control.route('/carts/')
@login_required
def carts():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Cart.query.order_by(desc(Cart.id))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/checkouts/carts.html', **locals())


@control.route('/transactions/')
@login_required
def transactions():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        search_q = request.args.get("q", None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Transaction.query.order_by(desc(Transaction.id))

    if search_q:
        search_q = search_q.strip()
        queries = [Transaction.name.ilike("%%%s%%" % search_q), Transaction.code.ilike("%%%s%%" % search_q),
                   Transaction.email.ilike("%%%s%%" % search_q), Transaction.city.ilike("%%%s%%" % search_q),
                   Transaction.phone.ilike("%%%s%%" % search_q),Transaction.transaction_status_code.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/checkouts/transactions.html', **locals())


@control.route('/orders/')
@login_required
def orders():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
        search_q = request.args.get("q",None)
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Order.query.order_by(desc(Order.id))

    if search_q:
        search_q = search_q.strip()
        queries = [Order.name.ilike("%%%s%%" % search_q), Order.code.ilike("%%%s%%" % search_q),
                   Order.email.ilike("%%%s%%" % search_q), Order.city.ilike("%%%s%%" % search_q),
                   Order.phone.ilike("%%%s%%" % search_q),Order.order_status_code.ilike("%%%s%%" % search_q),Order.delivery_status_code.ilike("%%%s%%" % search_q)]
        query = query.filter(or_(*queries))



    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.parse.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.parse.urlencode(request.args))

    return render_template('admin/checkouts/orders.html', **locals())


@control.route('/orders/<int:id>/', methods=['GET', 'POST'])
@login_required
def order_detail(id):
    title = "Order Details"
    order = checkout.OrderService.get(id)
    if not order:
        abort(404)

    entries = OrderEntry.query.filter(OrderEntry.order_id==order.id).all()

    return render_template('admin/checkouts/order_details.html', **locals())

@control.route('/orders/<int:id>/update_status/', methods=['GET', 'POST'])
@login_required
def update_order_status(id):
    title = "Update Order Status"
    order = checkout.OrderService.get(id)
    if not order:
        abort(404)

    form = UpdateOrderStatusForm(status_code=order.order_status_code)
    form.status_code.choices = [(0, "--- Select One ---")] + [(i.code, i.name) for i in OrderStatus.query.all()]

    obj = None
    if form.validate_on_submit():
        data = form.data
        obj = checkout.OrderService.update_status(order.id, **data)

    if obj:
        flash("success", "Order Status Updated Successfully")
        return redirect(url_for('.orders'))

    return render_template('admin/checkouts/update_order_status.html', **locals())


@control.route('/orders/<int:id>/update_delivery/', methods=['GET', 'POST'])
@login_required
def update_order_delivery(id):
    title = "Update Delivery Status"
    order = checkout.OrderService.get(id)
    if not order:
        abort(404)

    form = UpdateOrderStatusForm(status_code=order.delivery_status_code)
    form.status_code.choices = [(0, "--- Select One ---")] + [(i.code, i.name) for i in DeliveryStatus.query.all()]

    obj = None
    if form.validate_on_submit():
        data = form.data
        obj = checkout.OrderService.update_delivery(order.id, **data)

    if obj:
        flash("success", "Order Delivery Status Updated Successfully")
        return redirect(url_for('.orders'))

    return render_template('admin/checkouts/update_order_status.html', **locals())
