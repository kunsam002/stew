import urllib
from PIL import ImageFile
from imagekitio.client import Imagekit
import base64
from stew import imagekit
from stew.models import Image
from stew.services.assets import ImageService


def getsizes(uri, img_id):
    # get file size *and* image size (None if not known)
    file = urllib.request.urlopen(uri)
    size = file.headers.get("content-length")
    if size: size = int(size)
    p = ImageFile.Parser()
    while 1:
        data = file.read(1024)
        if not data:
            break
        p.feed(data)
        if p.image:
            width, height = p.image.size
            size = round(size / 1000, 2)
            if width > 700:
                print("----width too large----", uri, width, height, "%s KB" % size)
                ImageService.update(img_id, imagekit_migrated=False)
                return uri
            if size > 100:
                print("----size too large----", uri, width, height, "%s KB" % size)
                ImageService.update(img_id, imagekit_migrated=False)
                return uri
            print("%s of img_id is good to go......" % img_id)
            migrate_image(img_id)
            return "{} KB".format(size), width, height
            break
    file.close()
    return size, None


def migrate_image(obj_id):
    obj = Image.query.get(obj_id)
    if obj:
        folder_path = "/Recipes" if obj.menu_id and obj.menu else "/Inventory"
        tags = ""
        if obj.menu:
            condition = "Cooked" if obj.menu.support_cooked else "Uncooked"
            menu_type = "Special" if obj.menu.is_special else "Classic"
            if obj.menu.support_cooked:
                menu_type = "Cooked"
            tags = "Stew,Recipes,Meal,%s,%s,%s,%s" % (obj.menu.name, condition, obj.menu.menu_category_code, menu_type)
        if obj.protein:
            tags = "Stew,Protein,%s" % obj.protein.name
        if obj.ingredient:
            tags = "Stew,Ingredient,%s" % obj.ingredient.name
        resp = imagekit.upload_via_url(obj.url, {"filename": obj.name, "folder": folder_path, "tags": tags})
        print(resp)
        ImageService.update(obj_id, imagekit_imagePath=resp.get("imagePath"), imagekit_id=resp.get("id"),
                            thumbnail=resp.get("thumbnail"), imagekit_url=resp.get("url"), size_scan=True,
                            width_scan=True, imagekit_migrated=True)
        return resp
