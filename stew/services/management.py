"""
management.py

@Author: Olukunle Ogunmokun
@Date: 11th Oct, 2018

Service classes for managing users and roles
"""
import json

from stew.core.exceptions import ObjectNotFoundException, ActionDeniedException, ValidationFailed
from stew.services import ServiceFactory, bugsnag
from stew.models import *
from stew.signals import *

BaseUserService = ServiceFactory.create_service(User, db)

BaseAdminMessageService = ServiceFactory.create_service(AdminMessage, db)
BaseAdminMessageResponseService = ServiceFactory.create_service(AdminMessageResponse, db)
MenuReviewService = ServiceFactory.create_service(MenuReview, db)
PasswordResetTokenService = ServiceFactory.create_service(PasswordResetToken, db)


class UserService(BaseUserService):
    """ Register a new user """

    @classmethod
    def create(cls, **kwargs):
        """ Register a non-staff account """

        password = kwargs.pop("password", "")
        name = "%s %s" % (kwargs.get("first_name"), kwargs.get("last_name"))

        kwargs["name"] = name

        user = BaseUserService.create(**kwargs)
        user.set_password(password)

        return user

    @classmethod
    def register_user(cls, **kwargs):
        """ Register a non-staff account """

        password = kwargs.pop('password', None)

        kwargs["is_staff"] = False

        user = cls.create(**kwargs)
        user.set_password(password)

        return user

    @classmethod
    def register_staff(cls, **kwargs):
        """ Register a staff account """
        password = kwargs.pop('password', None)

        kwargs["is_staff"] = True

        user = cls.create(**kwargs)
        user.set_password(password)

        return user

    @classmethod
    def register_administrator(cls, **kwargs):
        """ Register a non-staff account """

        password = kwargs.pop('password', None)

        kwargs["is_staff"] = True
        kwargs['roles'] = 'admin'

        user = cls.create(**kwargs)
        user.set_password(password)

        return user

    @classmethod
    def reset_password(cls, obj_id, password, **kwargs):
        """ Method to reset a user's password to a new password """

        obj = BaseUserService.get(obj_id)

        obj = db.session.merge(obj)

        obj.set_password(password)

        return obj

    @classmethod
    def request_new_user_password(cls, obj_id, **kwargs):
        """
        Generates a password change token for the requested user
        """
        obj = BaseUserService.get(obj_id)
        token = PasswordResetTokenService.create(email=obj.email, user_id=obj_id)
        url = app.config.get("CANONICAL_URL") + url_for("public.recover_password", code=token.code)
        email_data = dict(email=obj.email, name=obj.name, link=url)
        user_password_reset.send(obj.id, **email_data)

        return token

    @classmethod
    def change_password(cls, obj_id, old_password, new_password, **kwargs):
        """ Change the password of an existing user """

        obj = BaseUserService.get(obj_id)

        if obj.check_password(old_password):
            obj.set_password(new_password)

            data = {"password": bcrypt.generate_password_hash(new_password)}

            obj = cls.update(obj.id, **data)
            return obj

        else:
            bugsnag.notify(Exception("Invalid password given. Password cannot be reset."),
                           context="UserService.change_password",
                           meta_data=dict(obj_id=obj_id, old_password=old_password, new_password=new_password,
                                          kwargs=kwargs))

            raise Exception("Invalid password given. Password cannot be reset.")


class AdminMessageService(BaseAdminMessageService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        obj = BaseAdminMessageService.create(**kwargs)
        contact_message.send(obj.id)
        # redis.zadd('recent_message', obj.id, json.dumps(obj.as_dict()))
        return obj


class AdminMessageResponse(BaseAdminMessageResponseService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        obj = BaseAdminMessageResponseService.create(**kwargs)
        # contact_message_replied.send(obj.id)
        return obj
