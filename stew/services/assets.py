"""
assets.py

@Author: Olukunle Ogunmokun
@Date: 4th Oct, 2018

Core services are utilized for managing basic activities
"""
import json
import os
import base64
import time

from stew.services import *
from stew.models import *

CountryService = ServiceFactory.create_service(Country, db)
StateService = ServiceFactory.create_service(State, db)

PaymentOptionService = ServiceFactory.create_service(PaymentOption, db)
PaymentChannelService = ServiceFactory.create_service(PaymentChannel, db)
PaymentStatusService = ServiceFactory.create_service(PaymentStatus, db)
DeliveryStatusService = ServiceFactory.create_service(DeliveryStatus, db)
DeliveryOptionService = ServiceFactory.create_service(DeliveryOption, db)
OrderStatusService = ServiceFactory.create_service(OrderStatus, db)
TransactionStatusService = ServiceFactory.create_service(TransactionStatus, db)

BaseImageService = ServiceFactory.create_service(Image, db)

BaseMenuService = ServiceFactory.create_service(Menu, db)
BaseIngredientService = ServiceFactory.create_service(Ingredient, db)
BaseProteinService = ServiceFactory.create_service(Protein, db)
BaseSideDishService = ServiceFactory.create_service(SideDish, db)

MenuConditionService = ServiceFactory.create_service(MenuCondition, db)
MenuCategoryService = ServiceFactory.create_service(MenuCategory, db)
MenuSizeService = ServiceFactory.create_service(MenuSize, db)
MenuFrequencyService = ServiceFactory.create_service(MenuFrequency, db)

BaseCurrencyService = ServiceFactory.create_service(Currency, db)


class CurrencyService(BaseCurrencyService):
    """service used to create a new customer"""

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        BaseCurrencyService.get(obj_id)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        BaseCurrencyService.get(obj_id)


class ImageService(BaseImageService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        if kwargs.get("filepath") is None:
            bugsnag.notify(Exception('You have to upload at least one image'), context="ImageService.create",
                           meta_data=kwargs)
            raise Exception('You have to upload at least one image')

        pub_id = kwargs.get("pub_id", "Assets")

        today = datetime.today()
        timestamp = today.timestamp()
        image = kwargs.get("filepath")

        if app.config.get("DEBUG", True):
            pub_id = "TestEnv/%s" % (pub_id + str(int(timestamp)))
        else:
            pub_id = "%s/%s" % (pub_id, pub_id + str(int(timestamp)))

        upload_result = upload(image, public_id=pub_id)
        os.remove(image)

        kwargs["secure_url"] = upload_result["url"]
        kwargs["public_id"] = pub_id

        obj = BaseImageService.create(**kwargs)

        return obj

    @classmethod
    def delete(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        # uploader_destroy(obj.public_id)
        BaseImageService.update(obj.id, menu_id=None, ingredient_id=None, protein_id=None, side_dish_id=None)

        return True


class IngredientService(BaseIngredientService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        """ Management service for handling all menu items """
        obj = BaseIngredientService.create(**kwargs)

        obj = cls.update(obj.id, code=utils.generate_code(obj.id))

        return obj

    @classmethod
    def set_cover_image(cls, obj_id, cover_image_id):
        obj = cls.get(obj_id)

        img = ImageService.get(cover_image_id)
        obj = BaseIngredientService.update(obj_id, cover_image_id=img.id)

        return obj


class ProteinService(BaseProteinService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        """ Management service for handling all menu items """
        obj = BaseProteinService.create(**kwargs)

        obj = cls.update(obj.id, code=utils.character_generator())

        return obj

    @classmethod
    def set_cover_image(cls, obj_id, cover_image_id):
        obj = cls.get(obj_id)

        img = ImageService.get(cover_image_id)
        obj = BaseProteinService.update(obj_id, cover_image_id=img.id)

        return obj


class SideDishService(BaseSideDishService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        """ Management service for handling all menu items """
        obj = BaseSideDishService.create(**kwargs)

        obj = cls.update(obj.id, code=utils.character_generator())

        return obj

    @classmethod
    def set_cover_image(cls, obj_id, cover_image_id):
        obj = cls.get(obj_id)

        img = ImageService.get(cover_image_id)
        obj = BaseSideDishService.update(obj_id, cover_image_id=img.id)

        return obj


def populate_menu_ingredients(obj_id, ingredient_ids):
    menu = Menu.query.get(obj_id)

    if menu:
        ingredients = Ingredient.query.filter(Ingredient.id.in_(ingredient_ids)).all()
        menu.ingredients = ingredients
        db.session.add(menu)
        db.session.commit()
        return True

    return False


def populate_menu_side_dishes(obj_id, side_dish_ids):
    menu = Menu.query.get(obj_id)

    if menu:
        sides = SideDish.query.filter(SideDish.id.in_(side_dish_ids)).all()
        menu.side_dishes = sides
        db.session.add(menu)
        db.session.commit()
        return True

    return False


def populate_menu_available_sizes(obj_id, support_cooked, support_uncooked):
    menu = Menu.query.get(obj_id)

    available_menu_sizes = []
    if support_uncooked:
        available_menu_sizes = MenuSizeService.query.filter(MenuSize.is_cooked != True).all()

    if support_cooked:
        cooked_sizes = MenuSizeService.query.filter(MenuSize.is_cooked == True).all()
        available_menu_sizes = available_menu_sizes + cooked_sizes

    menu.available_menu_sizes = available_menu_sizes
    menu.menu_sizes = ",".join(i.code for i in available_menu_sizes)
    db.session.add(menu)
    db.session.commit()
    return True


class MenuService(BaseMenuService):
    """ Service class to handle product creation """

    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        """ Custom create method support """
        kwargs.pop('image_ids', [])
        raw_ingredient_ids = kwargs.pop('ingredient_ids', [])
        raw_side_dish_ids = kwargs.pop('side_dish_ids', [])
        menu_size_codes = kwargs.pop('menu_size_codes', [])
        menu_frequency_codes = kwargs.pop('menu_frequency_codes', [])

        support_cooked = kwargs.get("support_cooked", False)
        support_uncooked = kwargs.get("support_uncooked", False)

        kwargs["menu_sizes"] = ",".join(menu_size_codes)
        kwargs["menu_frequencies"] = ",".join(menu_frequency_codes)
        kwargs["ingredient_ids"] = ",".join(str(i) for i in raw_ingredient_ids)
        kwargs["side_dish_ids"] = ",".join(str(i) for i in raw_side_dish_ids)
        kwargs["is_visible"] = True

        obj = BaseMenuService.create(ignored_args=ignored_args, **kwargs)

        populate_menu_ingredients(obj.id, raw_ingredient_ids)
        populate_menu_side_dishes(obj.id, raw_side_dish_ids)
        populate_menu_available_sizes(obj.id, support_cooked, support_uncooked)

        obj = BaseMenuService.update(obj.id, code=utils.generate_code(obj.id))

        return obj

    @classmethod
    def update(cls, obj_id, **kwargs):
        """ Custom update method support """

        obj = cls.get(obj_id)

        kwargs.pop('image_ids', [])
        raw_ingredient_ids = kwargs.pop('ingredient_ids', [])
        raw_side_dish_ids = kwargs.pop('side_dish_ids', [])
        menu_size_codes = kwargs.pop('menu_size_codes', [])
        menu_frequency_codes = kwargs.pop('menu_frequency_codes', [])

        support_cooked = kwargs.get("support_cooked", obj.support_cooked)
        support_uncooked = kwargs.get("support_uncooked", obj.support_uncooked)

        kwargs["menu_sizes"] = ",".join(menu_size_codes)
        kwargs["menu_frequencies"] = ",".join(menu_frequency_codes)
        kwargs["ingredient_ids"] = ",".join(str(i) for i in raw_ingredient_ids)
        kwargs["side_dish_ids"] = ",".join(str(i) for i in raw_side_dish_ids)

        populate_menu_ingredients(obj.id, raw_ingredient_ids)
        populate_menu_side_dishes(obj.id, raw_side_dish_ids)
        populate_menu_available_sizes(obj.id, support_cooked, support_uncooked)

        obj = BaseMenuService.update(obj_id, **kwargs)

        return obj

    @classmethod
    def set_cover_image(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        cover_image_id = kwargs.get("cover_image_id", 0)
        img = ImageService.get(cover_image_id)
        obj = BaseMenuService.update(obj_id, cover_image_id=img.id)

        return obj

    @classmethod
    def string_to_dict(cls, string):
        """ Get product attributes from input as string and parse
        it into a dictionary"""

        result = dict()
        d = string.split('|')

        e = [i.split(':') for i in d]
        for i in e:
            if len(i) > 1:
                key = i[0].strip()
                v = i[1].strip()
                result[key] = v
        return result

    @classmethod
    def get_attributes(cls, variant_attributes):
        """ Get product attributes from input as string and parse
        it into a dictionary"""

        string = variant_attributes
        d = string.split('|')

        e = [i.split(':') for i in d]
        attributes = []
        values = []
        for i in e:
            key = i[0].strip()
            v = i[1]
            value = v.split(',')
            value = [v.strip() for v in value]

            attributes.append(key)
            values.append(value)

        return [attributes, values]

    @classmethod
    def set_cover_image(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)
        cover = obj.cover_image

        if cover:
            cover.is_cover = False
            ImageService.update(cover.id)

        image_id = kwargs.get('image_id')
        ImageService.update(image_id, is_cover=True)

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        BaseMenuService.get(obj_id)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        BaseMenuService.get(obj_id)

    @classmethod
    def check_recipe_for_delete(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)

        if CartItem.query.filter(CartItem.menu_id == obj.id).count() > 0:
            return False

        if OrderEntry.query.filter(OrderEntry.menu_id == obj.id).count() > 0:
            return False

        return True

    @classmethod
    def remove_featured(cls, obj_id):
        obj = BaseMenuService.update(obj_id, is_featured=False, featured_position=0)

        return obj

    @classmethod
    def delete(cls, obj_id):
        obj = cls.get(obj_id)

        if not cls.check_recipe_for_delete(obj_id):
            BaseMenuService.update(obj_id, is_visible=False)
            return False, "warning", "Recipe currently being referenced and cannot be deleted."

        for img in obj.images:
            ImageService.delete(img.id)

        BaseMenuService.delete(obj_id)

        return True, "success", "Recipe Successfully Deleted"
