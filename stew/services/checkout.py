"""
checkout.py

@Author: Olukunle Ogunmokun
@Date: 4th Oct, 2018

Core services are utilized for managing basic activities
"""

import json
import os
import base64
import time

from stew.services import *
from stew.models import *
from stew.core.utils import generate_code
from stew.services.assets import MenuService, CountryService, DeliveryAddress, StateService

DeliveryAddressService = ServiceFactory.create_service(DeliveryAddress, db)
BaseCartService = ServiceFactory.create_service(Cart, db)
BaseCartItemService = ServiceFactory.create_service(CartItem, db)
BaseCartItemExtraService = ServiceFactory.create_service(CartItemExtra, db)

BaseSubscriptionService = ServiceFactory.create_service(Subscription, db)

BaseOrderService = ServiceFactory.create_service(Order, db)
BaseOrderEntryService = ServiceFactory.create_service(OrderEntry, db)
OrderEntryExtraService = ServiceFactory.create_service(OrderEntryExtra, db)

BaseTransactionService = ServiceFactory.create_service(Transaction, db)
BaseTransactionEntryService = ServiceFactory.create_service(TransactionEntry, db)
TransactionEntryExtraService = ServiceFactory.create_service(TransactionEntryExtra, db)

BaseRaveTransactionService = ServiceFactory.create_service(RaveTransaction, db)


def update_user_order_count(user_id):
    user = User.query.get(user_id)
    if user:
        user.order_count = Order.query.filter(Order.user_id == user_id).count()
        db.session.add(user)
        db.session.commit()
        return True

    return False


class CartService(BaseCartService):
    @classmethod
    def attach_user(cls, cart_id, user_id):
        user = User.query.get(user_id)
        if not user:
            bugsnag.notify(Exception("User not found"), context="CartService.attache_user",
                           meta_data=dict(user_id=user_id, cart_id=cart_id))
            raise Exception("User not found")
        CartItem.query.filter(CartItem.cart_id == cart_id).update({CartItem.user_id: user_id},
                                                                  synchronize_session=False)
        db.session.commit()
        obj = cls.update(cart_id, user_id=user_id)
        return obj

    @classmethod
    def clear_cart(cls, cart_id):
        """ Clears the cart by deleting all items inside it """
        CartItem.query.filter(CartItem.cart_id == cart_id).delete()
        cls.update(cart_id, delivery_charge=0)

    @classmethod
    def calculate_delivery(cls, cart_id):
        """ Updates Cart """
        cart = BaseCartService.get(cart_id)
        cart = BaseCartService.update(cart_id, delivery_charge=cart.state.delivery_charge)
        return cart

    @classmethod
    def delete(cls, cart_id):
        """ Deletes Cart """
        CartItemExtra.query.filter(CartItemExtra.cart_id == cart_id).delete()
        CartItem.query.filter(CartItem.cart_id == cart_id).delete()
        BaseCartService.delete(cart_id)
        return True

    @classmethod
    def add_item(cls, user_id, **kwargs):
        """ Append a session cart item to the cart """

        protein_id, side_dish_id = kwargs.get("protein_id", None), kwargs.get("side_dish_id", None)

        user = User.query.get(user_id)
        if not user:
            bugsnag.notify(Exception("Invalid User"), context="CartService.add_item",
                           meta_data=dict(user_id=user_id, kwargs=kwargs))
            raise Exception("Invalid User")

        cart = cls.query.filter(Cart.user_id == user_id).first()

        if not cart:
            cart = cls.create(user_id=user_id, name="%s's Cart" % user.name, payment_option_code="online",
                              delivery_option_code="self-delivery")
        cart_id = cart.id
        cart_item = CartItem.query.filter(CartItem.cart_id == cart_id,
                                          CartItem.menu_id == kwargs.get('menu_id')).first()
        kwargs["quantity"] = kwargs.get("quantity", 1)
        kwargs["user_id"] = user_id

        menu_condition_code = kwargs.get("menu_condition_code", "uncooked")
        if menu_condition_code == "cooked":
            kwargs["is_cooked"] = True
            kwargs["is_uncooked"] = False
        if menu_condition_code == "uncooked":
            kwargs["is_cooked"] = False
            kwargs["is_uncooked"] = True

        if cart_item:
            cart_item = CartItemService.update(cart_item.id, **kwargs)
        else:
            cart_item = CartItemService.create(cart_id=cart_id, **kwargs)
            CartItemService.update(cart_item.id)

        if protein_id or side_dish_id:
            extra = CartItemExtraService.query.filter(CartItemExtra.cart_item_id == cart_item.id).first()

            if side_dish_id and SideDish.query.get(side_dish_id):
                pass
            else:
                kwargs["side_dish_id"] = None

            try:
                if extra:
                    extra = CartItemExtraService.update(extra.id, **kwargs)
                else:
                    kwargs["cart_item_id"] = cart_item.id
                    kwargs["cart_id"] = cart_id
                    extra = CartItemExtraService.create(**kwargs)

                extra.calculate_total()
            except Exception as e:
                db.session.rollback()
                raise Exception("Failed Creating Cart Item: %s" % e)

        cart_item.calculate_total()

        calculate_cart_total.send(cart.id)

        return cart_item

    @classmethod
    def merge_cart(cls, old_cart_id, new_cart_id):
        """"""

        old_cart = Cart.query.get(old_cart_id)
        new_cart = Cart.query.get(new_cart_id)

        for item in old_cart.cart_items.all():
            new_cart.cart_items.append(item)
        # CartItemService.create(cart_id=new_cart_id, **d)
        db.session.add(new_cart)
        db.session.commit()
        db.session.delete(old_cart)
        db.session.commit()
        return new_cart


class CartItemService(BaseCartItemService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        menu = Menu.query.get(kwargs.get("menu_id"))
        kwargs["menu_id"] = menu.id
        menu_condition_code = kwargs.get("menu_condition_code", "uncooked")
        kwargs["price"] = menu.uncooked_cost if menu_condition_code == "uncooked" else menu.default_cost
        kwargs["is_special"] = menu.is_special
        kwargs["is_cooked"] = True if menu_condition_code == "cooked" else False
        kwargs["is_uncooked"] = True if menu_condition_code == "uncooked" else False
        obj = BaseCartItemService.create(**kwargs)

        return obj


class CartItemExtraService(BaseCartItemExtraService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        CartService.query.get(kwargs.get("cart_item_id"))
        obj = BaseCartItemExtraService.create(**kwargs)

        return obj


class OrderService(BaseOrderService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        transaction_id = kwargs.get("transaction_id")
        kwargs.pop('country')
        kwargs.pop('state')
        kwargs.pop('rave_transaction')
        kwargs["order_status_code"] = "pending"
        kwargs["delivery_status_code"] = "pending"
        obj = BaseOrderService.create(ignored_args=ignored_args, **kwargs)

        for item in TransactionEntry.query.filter(TransactionEntry.transaction_id == transaction_id).all():
            item_data = item.as_dict(
                exclude=["date_created", "last_updated", "transaction", "menu", "menu_size", "user", "menu_condition"])
            item_data["order_id"] = obj.id
            OrderEntryService.create(item.id, **item_data)

        # build notification here
        order = cls.get(obj.id)

        if order.user:
            update_user_order_count(order.user_id)

        order_placed.send(order.id)
        return order

    @classmethod
    def update(cls, obj_id, ignored_args=None, **kwargs):
        obj = cls.get(obj_id)
        entries = obj.entries

        if obj is None:
            bugsnag.notify(Exception('Order does not exist, Ooops!'), context="OrderService.update",
                           meta_data=dict(obj_id=obj_id, kwargs=kwargs))
            raise Exception('Order does not exist, Ooops!')

        obj = BaseOrderService.update(obj_id, ignored_args, **kwargs)
        if entries:
            for entry in entries:
                OrderEntryService.update(entry.id, **kwargs)

        return obj

    @classmethod
    def update_status(cls, obj_id, **kwargs):
        obj = BaseOrderService.update(obj_id, order_status_code=kwargs.get("status_code"))
        return obj

    @classmethod
    def update_delivery(cls, obj_id, **kwargs):
        obj = BaseOrderService.update(obj_id, delivery_status_code=kwargs.get("status_code"))
        return obj


class OrderEntryService(BaseOrderEntryService):

    @classmethod
    def create(cls, entry_id, **kwargs):
        entry = TransactionEntryService.query.get(entry_id)
        kwargs["transaction_entry_id"] = entry.id
        obj = BaseOrderEntryService.create(**kwargs)

        for extra in entry.extras:
            OrderEntryExtraService.create(transaction_entry_extra_id=extra.id,
                                          order_entry_id=obj.id,
                                          **extra.as_dict(
                                              exclude=["id", "last_updated", "date_created", "cart_item",
                                                       "protein", "side_dish"]))

        return obj

    @classmethod
    def update(cls, obj_id, ignored_args=None, **kwargs):
        obj = cls.get(obj_id)
        status = kwargs.get('order_status_code')
        if status == 'processing':
            kwargs.update(date_processed=datetime.today())
        if status == 'cancelled':
            kwargs.update(date_cancelled=datetime.today())
        if status == 'fulfilled':
            kwargs.update(date_fulfilled=datetime.today())
        if status == 'customer_cancelled':
            kwargs.update(date_customer_cancelled=datetime.today())
        if status == 'returned':
            kwargs.update(date_returned=datetime.today())

        BaseOrderEntryService.update(obj_id, ignored_args, **kwargs)

        return obj


class TransactionService(BaseTransactionService):
    @classmethod
    def create(cls, payment_success, **kwargs):

        cart = CartService.get(kwargs.get("cart_id", None))

        code = id_generator(size=8)
        if TransactionService.query.filter(Transaction.code == code).count() > 0:
            code = id_generator(size=8)

        transaction = BaseTransactionService.create(code=code, **kwargs)

        entries = []
        entry_ids = []
        for item in cart.cart_items.all():
            entry = item.as_dict(
                exclude=["menu", "menu_condition", "user", "menu_size", "cart", "menu_condition"])
            entry["transaction_id"] = transaction.id
            entry["total"] = item.total
            TransactionEntryService.create(item.id, **entry)
            entries.append(entry)
            entry_ids.append(item.id)

        if payment_success:
            data = transaction.as_dict()
            data["order_status_code"] = "pending"
            data["payment_status_code"] = "pending"
            data["code"] = id_generator()
            data["transaction_id"] = transaction.id
            data["first_name"] = transaction.user.first_name if transaction.user else None
            data["last_name"] = transaction.user.last_name if transaction.user else None

            order = OrderService.create(**data)
            transaction = cls.update(transaction.id, order_id=order.id, transaction_status_code="successful")
            # CartItemService.delete_by_ids(entry_ids)

            CartService.delete(cart.id)

        return transaction

    @classmethod
    def review_transaction(cls, **kwargs):
        status_code = kwargs.get('status_code')
        transaction_id = kwargs.get('transaction_id')
        transaction = cls.get(transaction_id)
        order = None
        payment_status_code = None
        cart = CartService.get(transaction.cart_id)

        if status_code in ['successful']:
            if cart.payment_option.type == "postpaid":
                payment_status_code = "pending"
            elif cart.payment_option.type == "prepaid":
                payment_status_code = "paid"

            order = OrderService.update(transaction.order_id, order_status_code="processing",
                                        payment_status_code=payment_status_code)

            if cart.checkout_type == "BuyNow" or cart.user_id == None:
                CartService.delete(cart.id)

        kwargs['transaction_status_code'] = status_code
        cls.update(transaction_id, **kwargs)

        return order

    @classmethod
    def confirm_vbv(cls, trans_code, **kwargs):
        """ Confirm transaction that is authorized via VBVSecure Code """

        # Failsafe mechanism for when the response structure changes
        resp = kwargs.get("resp")

        data = json.loads(str(resp[0]))

        trans = Transaction.query.filter(Transaction.code == str(trans_code.upper())).first()

        if not trans:
            raise ObjectNotFoundException(Transaction, trans_code)

        res_code = data.get("responsecode", None)
        response_message = data.get("responsemessage", None)
        data.get("responsetoken", None)

        status_code = trans.status_code

        if res_code and res_code in ["0", "00"]:
            data['response_status'] = 'success'
            data['response_code'] = res_code
            data['response_message'] = response_message
            status_code = 'successful'

        else:
            data['response_status'] = 'failed'
            data['response_code'] = res_code
            data['response_message'] = response_message
            status_code = 'failed'

        trans = TransactionService.update(trans.id, status_code=status_code, **data)

        # Expire the request here
        # RequestService.update(request_obj.id, is_expired=True)

        return trans


class TransactionEntryService(BaseTransactionEntryService):
    @classmethod
    def create(cls, cart_item_id, **kwargs):
        cart_item = CartItemService.query.get(cart_item_id)

        obj = BaseTransactionEntryService.create(**kwargs)

        for extra in cart_item.extras:
            TransactionEntryExtraService.create(cart_item_extra_id=extra.id,
                                                transaction_entry_id=obj.id,
                                                **extra.as_dict(
                                                    exclude=["id", "last_updated", "date_created", "cart_item",
                                                             "protein", "side_dish"]))

        return obj


class SubscriptionService(BaseSubscriptionService):
    @classmethod
    def create_success(cls, user_id, **kwargs):
        BaseSubscriptionService.create(**kwargs)

        return kwargs


class RaveTransactionService(BaseRaveTransactionService):

    @classmethod
    def create_success(cls, **kwargs):
        cart_id = kwargs.get("cart_id")
        obj = BaseRaveTransactionService.create(**kwargs)

        cart = CartService.get(cart_id)

        user = cart.user
        user_first_name, user_last_name = user.first_name, user.last_name

        transaction_data = cart.as_dict(
            exclude=["id", "last_updated", "date_created", "user", "country", "state", "payment_option",
                     "delivery_option", "transaction_status"])
        transaction_data["cart_id"] = cart_id
        transaction_data["amount"] = cart._total
        transaction_data["discount_applied"] = cart.discount
        transaction_data["shipping_charge"] = cart.delivery_charge
        transaction_data["shipment_description"] = cart.additional_info
        transaction_data["shipment_address"] = cart.address
        transaction_data["first_name"] = user_first_name
        transaction_data["last_name"] = user_last_name
        transaction_data["name"] = "%s %s" % (user_first_name, user_last_name)
        transaction_data["shipment_name"] = "%s %s" % (user_first_name, user_last_name)
        transaction_data["shipment_phone"] = cart.phone
        transaction_data["shipment_city"] = cart.city
        transaction_data["shipment_state_code"] = cart.state_code
        transaction_data["shipment_country_code"] = cart.country_code
        transaction_data["transaction_status_code"] = "attempted"
        transaction_data["payment_status_code"] = "pending"
        transaction_data["checkout_type"] = kwargs.get("channel")
        transaction_data["rave_transaction_id"] = obj.id
        transaction_data["description"] = "Order By %s" % cart.name
        payment_success = True

        TransactionService.create(payment_success, **transaction_data)
        return obj

    @classmethod
    def create_failed(cls, **kwargs):
        cart_id = kwargs.get("cart_id")
        obj = BaseRaveTransactionService.create(**kwargs)

        cart = CartService.get(cart_id)

        user = cart.user
        user_first_name, user_last_name = user.first_name, user.last_name

        transaction_data = cart.as_dict(
            exclude=["id", "last_updated", "date_created", "user", "country", "state", "payment_option",
                     "delivery_option", "transaction_status"])
        transaction_data["cart_id"] = cart_id
        transaction_data["amount"] = cart._total
        transaction_data["discount_applied"] = cart.discount
        transaction_data["shipping_charge"] = cart.delivery_charge
        transaction_data["shipment_description"] = cart.additional_info
        transaction_data["shipment_address"] = cart.address
        transaction_data["first_name"] = user_first_name
        transaction_data["last_name"] = user_last_name
        transaction_data["name"] = "%s %s" % (user_first_name, user_last_name)
        transaction_data["shipment_name"] = "%s %s" % (user_first_name, user_last_name)
        transaction_data["shipment_phone"] = cart.phone
        transaction_data["shipment_city"] = cart.city
        transaction_data["shipment_state_code"] = cart.state_code
        transaction_data["shipment_country_code"] = cart.country_code
        transaction_data["transaction_status_code"] = "pending"
        transaction_data["payment_status_code"] = "pending"
        transaction_data["checkout_type"] = kwargs.get("channel")
        transaction_data["rave_transaction_id"] = obj.id
        transaction_data["description"] = "Order By %s" % cart.name

        payment_success = False

        TransactionService.create(payment_success, **transaction_data)

        return obj
