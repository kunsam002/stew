from stew.services import *
from stew.models import *
from stew.core.utils import code_generator

from stew.services.checkout import TransactionService
import bcrypt as byc
import json

BaseCurrencyService = ServiceFactory.create_service(Currency, db)
BaseCardService = ServiceFactory.create_service(RecurrentCard, db)


class CurrencyService(BaseCurrencyService):
    """service used to create a new customer"""

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseCurrencyService.get(obj_id)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseCurrencyService.get(obj_id)


class CardService(BaseCardService):
    """"""

    @classmethod
    def index_search_object(cls, obj_id, **kwargs):
        obj = BaseCardService.get(obj_id)

    @classmethod
    def delete_search_object(cls, obj_id, **kwargs):
        obj = BaseCardService.get(obj_id)

    @classmethod
    def charge(cls, card_id, **kwargs):
        """method used for a payment by a saved card"""

        card = cls.get(card_id)

        if not card:
            raise ObjectNotFoundException(RecurrentCard, card_id)

        kwargs['chargetoken'] = card.token
        kwargs['currency'] = "NGN"
        kwargs['custid'] = "ijektngkdl"

        res = flutterwave.card.charge(**kwargs)

        data = res.get('data')
        status = res.get('status')
        res_code = data.get('responsecode', None)

        # status_code = trans.status_code

        if status == "success" and res_code in ["0", "00"]:
            kwargs['response_status'] = status
            kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
            kwargs['response_code'] = res_code
            kwargs['transaction_reference'] = data.get('transactionreference', None)
            kwargs['status'] = 'successful'
            # trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)
            # return trans
        else:
            kwargs['response_status'] = status
            kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
            kwargs['response_code'] = res_code
            kwargs['status'] = 'failed'
            raise Exception
            # notifier.send()  # to.do: send out a notification here............

            # trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)

            # return trans
        return kwargs

    @classmethod
    def pay(cls, **kwargs):
        """method used for a payment by an unsaved card"""


        card_data = cls.check_card(**kwargs)
        brand = card_data.get('brand')
        cvv = card_data.get('cvv')
        pin = kwargs.get("pin", '1234')
        # pin = kwargs.get("pin", None)

        kwargs.update(card_data)

        authmodel = flutterwave.AUTHMODEL.choice(brand)
        validateoption = flutterwave.VALIDATEOPTION.SMS

        kwargs['code'] = code_generator()
        kwargs['method'] = "card"
        kwargs['mask'] = card_data.get('mask')
        kwargs['expiryyear'] = card_data.get('expiryyear')
        kwargs['exp_year'] = card_data.get('expiryyear')
        kwargs['expirymonth'] = card_data.get('expirymonth')
        kwargs['exp_month'] = card_data.get('expirymonth')
        kwargs['brand'] = brand
        kwargs['cvv'] = str(cvv)
        kwargs['pin'] = str(pin)
        # kwargs['pin'] = pin
        kwargs['currency'] = "NGN"
        kwargs['authmodel'] = authmodel
        kwargs['validateoption'] = validateoption

        # kwargs['narration'] = kwargs.get('narration', "payment for product")
        kwargs['customerId'] = 'ad4578'
        kwargs['custid'] = 'ad4578'

        kwargs['authmodel'] = authmodel

        trans = TransactionService.get(kwargs.get("trans_id"))

        if authmodel == flutterwave.AUTHMODEL.VBVSECURECODE:
            ""
            kwargs['responseurl'] = trans.response_url

        # check that the pin is required. This action will happen only when the authmodel is not vbvsecurecode
        # if authmodel != flutterwave.AUTHMODEL.VBVSECURECODE and not pin:
        #     # take in the pin number and start again
        #     raise FurtherActionException(kwargs)

        try:
            res = flutterwave.card.pay(**kwargs)

            data = res.get('data')
            status = res.get('status')
            res_code = data.get('responsecode', None)
        except Exception as e:
            raise ValidationFailed({"data": e})

        if status == "success" and res_code in ["02"]:
            kwargs['response_status'] = status
            kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
            kwargs['otptransactionidentifier'] = data.get('otptransactionidentifier')
            kwargs['response_code'] = res_code
            kwargs['transaction_reference'] = data.get('transactionreference', None)
            # trans = TransactionService.update(trans.id, status_code='validation_required', **kwargs)
            # data["code"] = trans.code
            raise FurtherActionException(data)

        elif status == "success" and res_code in ["00", "0"]:
            kwargs['response_status'] = status
            kwargs['status'] = "successful"
            kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
            kwargs['token'] = data.get('responsetoken', None)
            kwargs['response_code'] = res_code
            kwargs['transaction_reference'] = data.get('transactionreference', None)

        else:
            kwargs['response_status'] = status
            kwargs['status'] = "failed"
            kwargs['response_message'] = data.get('responsemessage', None)
            kwargs['response_code'] = res_code
            kwargs['transaction_reference'] = data.get('transactionreference', None)
            raise ValidationFailed({"cardno": ["Invalid or expired card"]})

        # return trans
        return kwargs

    @classmethod
    def validate(cls, code, **kwargs):

        # trans = Transaction.query.filter(Transaction.code == str(code.upper())).first()

        # if not trans:
        #     raise ObjectNotFoundException(Transaction, code)
        #
        # kwargs['trxreference'] = trans.transactionreference
        # kwargs['otptransactionidentifier'] = trans.otptransactionidentifier
        # kwargs['currency'] = trans.currency
        # kwargs['amount'] = trans.amount

        res = flutterwave.card.validate(**kwargs)

        data = res.get('data')
        status = res.get('status')
        res_code = data.get('responsecode', None)

        # status_code = trans.status_code
        status_code = ""
        # request_obj = Request.query.filter_by(request_key=trans.request_key).first()

        if status == "success" and res_code in ["0", "00", "200"]:
            kwargs['response_status'] = status
            kwargs['response_message'] = data.get('responsemessage', None)  # get response message from res
            kwargs['response_code'] = res_code
            kwargs['token'] = data.get("responsetoken", None)
            status_code = 'successful'
        else:
            kwargs['response_status'] = status
            kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
            kwargs['response_code'] = res_code
            status_code = 'failed'

            # trans = TransactionService.update(trans.id, status_code=status_code, **kwargs)
            # trans_data = trans.level_dict()
            # trans_data['merchant_key'] = request_obj.merchant_key

            # if trans.status_code == 'successful':
            #     send_payment_notification.delay(request_obj.id, 'pc', 'Payment Successful', **trans_data)
            # else:
            #     send_payment_notification.delay(request_obj.id, 'pf', 'Payment Failed', **trans_data)

            # return trans

    @classmethod
    def check_card(cls, **kwargs):
        """ Validate the card and return the required values for storage """

        cardno = kwargs.get("card_number")
        expiry_year = int(kwargs.get("expiry_year"))
        expiry_month = int(kwargs.get("expiry_month"))
        cvv = kwargs.get("cvv")
        pin = kwargs.get("pin")

        # clean the parameters of the card
        # expiry_month, expiry_year = expiry.split("/")
        # expiry_month = int(expiry_month)
        # expiry_year = int(expiry_year)
        cvv = cvv

        card = pycard.Card(number=cardno, month=expiry_month, year=expiry_year, cvc=cvv)

        if card.is_valid and not card.is_expired:
            return dict(cardno=card.number, expirymonth=card.exp_date.mm, expiryyear=card.exp_date.yy,
                        friendly_brand=card.friendly_brand,
                        cvv=card.cvc, brand=card.brand, mask=card.mask, is_expired=card.is_expired, pin=pin)
        else:
            raise ValidationFailed({"cardno": ["Invalid card supplied"]})

    @classmethod
    def register(cls, **kwargs):

        card_data = cls.check_card(**kwargs)
        request_key = kwargs.get('request_key')

        authmodel = flutterwave.AUTHMODEL.NOAUTH
        kwargs['authmodel'] = authmodel
        kwargs['currency'] = flutterwave.CURRENCYMODEL.NGN
        kwargs['validateoption'] = flutterwave.VALIDATEOPTION.SMS

        kwargs.update(card_data)

        res = flutterwave.card.tokenize(**kwargs)

        data = res.get('data')
        status = res.get('status')
        res_code = data.get('responsecode', None)

        if status == "success" and res_code in ["0", "00", "200"]:

            kwargs['token'] = data.get('responsetoken')
            kwargs['exp_month'] = kwargs.get('expirymonth', None)
            kwargs['exp_year'] = kwargs.get('expiryyear', None)
            kwargs['mask'] = card_data.get('mask')
            kwargs['brand'] = card_data.get('brand')
            card = cls.create(**kwargs)
            return card_data

        else:
            kwargs['response_status'] = status
            kwargs['response_message'] = res.get('responsemessage', None)  # get response message from res
            kwargs['response_code'] = res_code

            raise ValidationFailed({"cardno": ["Invalid or expired card"]})
