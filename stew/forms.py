from stew.core.exceptions import ValidationFailed
from wtforms import StringField, PasswordField, BooleanField, ValidationError, SelectField, \
    FloatField, IntegerField, widgets, Field, HiddenField, FormField, SelectMultipleField, RadioField
from flask_wtf import FlaskForm
from wtforms.validators import Optional, DataRequired, Email, EqualTo, NumberRange
from wtforms.ext.dateutil.fields import DateField, DateTimeField
from stew.models import *
from sqlalchemy import or_, func


class RequiredIf(DataRequired):
    # a validator which makes a field required if
    # another field is set and has a truthy value

    def __init__(self, other_field_name, *args, **kwargs):
        self.other_field_name = other_field_name
        super(RequiredIf, self).__init__(*args, **kwargs)

    def __call__(self, form, field):
        other_field = form._fields.get(self.other_field_name)
        if other_field is None:
            raise Exception('no field named "%s" in form' % self.other_field_name)
        if bool(other_field.data):
            super(RequiredIf, self).__call__(form, field)


class RequiredOption(DataRequired):
    # a validator which makes a field required if
    # another field is set and has a truthy value

    def __init__(self, other_field_name, option_value, *args, **kwargs):
        self.other_field_name = other_field_name
        self.option_value = option_value
        super(RequiredOption, self).__init__(*args, **kwargs)

    def __call__(self, form, field):
        other_field = form._fields.get(self.other_field_name)
        if other_field is None:
            raise Exception('no field named "%s" in form' % self.other_field_name)
        if bool(other_field.data == self.option_value):
            super(RequiredOption, self).__call__(form, field)


class IntegerListField(Field):
    """ Custom field to support sending in multiple integers separated by commas and returning the content as a list """

    widget = widgets.TextInput()

    def __init__(self, label='', validators=None, **kwargs):
        super(IntegerListField, self).__init__(label, validators, **kwargs)

    def _value(self):
        if self.data:
            string_data = [str(y) for y in self.data]
            return u','.join(string_data)

        else:
            return u''

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = [int(x.strip()) for x in valuelist[0].split(',')]
        else:
            self.data = []

    def process_data(self, valuelist):
        if valuelist:
            self.data = [int(x.strip()) for x in valuelist[0].split(',')]
        else:
            self.data = []


def check_chars(input):
    """ Checks if there's a special character in the text """

    chars = """ '"!@#$%^&*()+=]}_[{|\':;?/>,<\r\n\t """
    return any((c in chars) for c in input)


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember me', validators=[Optional()])

class ForgotPasswordForm(FlaskForm):
    username = StringField('Username or Email Address', validators=[DataRequired()])

class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    verify_password = PasswordField('Verify Password',
                                    validators=[DataRequired(), EqualTo("password", "Passwords are not equal")])

class SignUpForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired()])
    phone = StringField('Username', validators=[DataRequired()])
    # username = StringField('Username', validators=[Optional()])
    new_password = PasswordField('Password', validators=[DataRequired()])
    verify_password = PasswordField('Password', validators=[DataRequired(), EqualTo('new_password')])

    def validate_email(form,field):
        if User.query.filter(User.email==field.data).count() > 0:
            raise ValidationError("Email Address already exist.")

    def validate_username(form,field):
        if User.query.filter(User.username==field.data).count() > 0:
            raise ValidationError("Username already exist.")

    def validate_phone(form,field):
        if User.query.filter(User.phone==field.data).count() > 0:
            raise ValidationError("Phone Number already exist.")


class ContactForm(FlaskForm):
    name = StringField('Full Name', validators=[DataRequired()])
    subject = StringField('Subject', validators=[DataRequired()])
    email = StringField('Email Address', validators=[DataRequired(), Email()])
    phone = StringField('Phone Number', validators=[DataRequired()])
    body = StringField('Message', validators=[DataRequired()], widget=widgets.TextArea())


class ContactForm(FlaskForm):
    name = StringField('Full Name', validators=[DataRequired()])
    subject = StringField('Subject', validators=[DataRequired()])
    email = StringField('Email Address', validators=[DataRequired(), Email()])
    phone = StringField('Phone Number', validators=[DataRequired()])
    body = StringField('Message', validators=[DataRequired()], widget=widgets.TextArea())


class DeliveryOptionForm(FlaskForm):
    code = StringField("Code", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])


class PaymentOptionForm(FlaskForm):
    code = StringField("Code", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])


class PaymentStatusForm(FlaskForm):
    code = StringField("Code", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])


class DeliveryStatusForm(FlaskForm):
    code = StringField("Code", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])


class MenuSizeForm(FlaskForm):
    code = StringField("Code", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])
    value = IntegerField("Value", validators=[Optional()])
    cost_multiplier = FloatField("Cost Multiplier", validators=[DataRequired()])
    protein_cost_multiplier = FloatField("Protein Cost Multiplier", validators=[DataRequired()])
    side_dish_cost_multiplier = FloatField("Side Dish Cost Multiplier", validators=[DataRequired()])
    is_cooked = BooleanField("Support Cooked", validators=[Optional()])

class MenuFrequencyForm(FlaskForm):
    code = StringField("Code", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])
    value = IntegerField("Value", validators=[Optional()])
    cost_multiplier = FloatField("Cost Multiplier", validators=[Optional()])


class MenuCategoryForm(FlaskForm):
    code = StringField("Code", validators=[DataRequired()])
    name = StringField("Name", validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])
    enabled = BooleanField("Active", validators=[Optional()])
    is_special = BooleanField("Special", validators=[Optional()])

class MenuForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired()])
    caption = StringField("Caption", validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])
    menu_category_code = SelectField("Menu Category", validators=[DataRequired()], coerce=str)
    production_time = IntegerField("How Long It Takes to Make (in minutes)", validators=[DataRequired()])
    ingredient_ids = SelectMultipleField("Ingredients", validators=[DataRequired()], coerce=int)
    side_dish_ids = SelectMultipleField("Side Dishes", validators=[Optional()], coerce=int)

    menu_size_codes = SelectMultipleField("Menu Sizes", validators=[Optional()], coerce=str)
    cooked_menu_size_codes = SelectMultipleField("Cooked Menu Sizes", validators=[Optional()], coerce=str)
    menu_frequency_codes = SelectMultipleField("Menu Frequencies", validators=[Optional()], coerce=str)

    is_featured = BooleanField("Feature on Home", validators=[Optional()])
    featured_position = IntegerField("Feature Position", validators=[Optional()])

    is_visible = BooleanField("Visible", validators=[Optional()])
    is_special = BooleanField("Special", validators=[Optional()])
    is_vegetarian = BooleanField("Vegetarian Recipe", validators=[Optional()])

    support_cooked = BooleanField("Support Cooked", validators=[Optional()])
    support_uncooked = BooleanField("Support Uncooked", validators=[Optional()], default=True)

    allow_subscription = BooleanField("Available for Subscription", validators=[Optional()])
    allow_one_off = BooleanField("Available for One Offs", validators=[Optional()])

    default_cost= FloatField("Default Cooked Price", validators=[Optional()])
    uncooked_cost= FloatField("Default Uncooked Price", validators=[Optional()])

    def validate_featured_position(form, field):
        if form.is_featured.data == True and field.data in [""," ",0]:
            raise ValidationError("Kindly Specify a Featured Position or remove from being featured.")

    def validate_default_cost(form,field):
        if form.support_cooked.data ==True and field.data in [""," ",0]:
            raise ValidationError("Kindly Provide a valid Default Recipe cost for Cooked Condition.")

class UpdateMenuForm(MenuForm):
    cover_image_id = IntegerField('Cover Image', widget=widgets.HiddenInput(), validators=[DataRequired()])
    removables = SelectMultipleField(validators=[Optional()], coerce=int)
    ingredient_ids = SelectMultipleField("Ingredients", validators=[Optional()], coerce=int)

class IngredientForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])
    preservation = StringField("Preservation", validators=[DataRequired()])

    quantity = IntegerField("Available Quantity", validators=[Optional()])
    price = FloatField("Price", validators=[Optional()])
    expiration_date = DateTimeField("Expiration Date", validators=[DataRequired()])


class UpdateIngredientForm(IngredientForm):
    cover_image_id = IntegerField('Cover Image', widget=widgets.HiddenInput(), validators=[DataRequired()])


class ProteinForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])

    quantity = IntegerField("Available Quantity", validators=[Optional()])
    price = FloatField("Price", validators=[DataRequired()], default=0.0)


class UpdateProteinForm(ProteinForm):
    cover_image_id = IntegerField('Cover Image', widget=widgets.HiddenInput(), validators=[DataRequired()])


class SideDishForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])

    quantity = IntegerField("Quantity", validators=[Optional()])
    price = FloatField("Price", validators=[Optional()])


class UpdateSideDishForm(SideDishForm):
    cover_image_id = IntegerField('Cover Image', widget=widgets.HiddenInput(), validators=[DataRequired()])


class CartForm(FlaskForm):
    menu_id = IntegerField('Recipe', validators=[DataRequired()])
    protein_id = RadioField('Protein', validators=[Optional()], coerce=int)
    side_dish_id = SelectField('Side Dish', validators=[Optional()], coerce=int)
    menu_size_code = RadioField('Number of People', validators=[DataRequired()], coerce=str)
    menu_condition_code = RadioField('How We Serve You', validators=[DataRequired()], coerce=str)

class UpdateCartItemForm(FlaskForm):
    menu_size = SelectField("Menu Size", coerce=str, validators=[DataRequired()])
    item_id = IntegerField('Recipe', widget=widgets.HiddenInput(), validators=[DataRequired()])


class CheckoutForm(FlaskForm):
    address = StringField("Delivery Address", validators=[DataRequired()])
    country_code = SelectField("Country", coerce=str, validators=[DataRequired()])
    state_code = SelectField("State", coerce=str, validators=[DataRequired()])
    city = StringField("City", validators=[DataRequired()])
    additional_info = StringField("Additional Message", validators=[Optional()])
    toc = BooleanField("I Agree to Policy and Terms & Condition", validators=[DataRequired()])


class SubscriptionForm(CheckoutForm):
    menu_size_code = RadioField("Number of People", validators=[DataRequired()], coerce=str)
    menu_frequency_code = RadioField("Recipes per week", validators=[DataRequired()], coerce=str)
    menu_category_code = RadioField("Category", validators=[DataRequired()], coerce=str)

    menu_ids = SelectMultipleField("Recipes", validators=[DataRequired()], coerce=int)
    menu_option_ids = RadioField('Recipes', coerce=int, validators=[DataRequired()])

    is_vegetarian = BooleanField('Are You Vegetarian?', validators=[Optional()])


class RavePayCheckoutForm(FlaskForm):
    class Meta:
        csrf = False

    embed_token = StringField('Embed Token')
    user_token = StringField('User Token')
    amount = FloatField('Amount')
    appfee = FloatField('App Fee')
    charged_amount = FloatField('Charged Amount')
    narration = StringField('Narration')
    orderRef = StringField('OrderRef')
    txRef = StringField('Transaction Ref')
    chargeResponseCode = StringField('Charge Response Code')
    chargeResponseMessage = StringField('Charge Response Message')
    merchantbearsfee = StringField('merchantbearsfee')
    paymentId = StringField('paymentId')
    paymentType = StringField('paymentType')
    status = StringField('status')
    vbvrespcode = StringField('vbvrespcode')
    vbvrespmessage = StringField('vbvrespmessage')
    auth_model_used = StringField('authModelUsed')
    charge_type = StringField('charge_type')
    cycle = StringField('cycle')
    device_fingerprint = StringField('device_fingerprint')
    flwRef = StringField('flwRef')
    channel = StringField('channel')
    actor = IntegerField('actor')


class UpdateOrderStatusForm(FlaskForm):
    status_code = SelectField("Status", validators=[DataRequired()], coerce=str)


class MenuReviewForm(FlaskForm):
    subject = StringField('Subject', validators=[DataRequired()])
    rating = IntegerField('Rating', validators=[DataRequired()])
    message = StringField('Message', validators=[DataRequired()])


class BlankForm(FlaskForm): pass
