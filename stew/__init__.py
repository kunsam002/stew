# Imports
from flask import current_app as app
import os
from datetime import datetime, timedelta
from sqlalchemy.event import listens_for
from sqlalchemy.engine import Engine
from sqlalchemy.pool import Pool
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from PIL import Image as PImage


Base = declarative_base()

# Logger
logger = app.logger

# Bcrypt
bcrypt = app.bcrypt

# Database
db = app.db

# Alembic
alembic = app.alembic

# API
api = app.api

# Login Manager
login_manager = app.login_manager

# Authorization Principal
principal = app.principal

# Uploaders
photos = app.photos
archives = app.archives

# File Uploader
uploads = app.uploads

# Imagekit
imagekit = app.imagekit

# Redis
redis = app.redis

cache = app.cache

mail = app.mail
sg = app.sg

report_gen = app.report_gen

# JWT
jwt = app.jwt

subscription_complete = app.config.get("SUBSCRIPTION_COMPLETE")

def check_image_size(src, dimensions=(200, 200), square=True):
    """
    Check's image dimensions. If square is true,
    check that the image is a square,
    else check that it matches the dimensions

    """

    img = PImage.open(src)
    width, height = img.size
    d_width, d_height = dimensions

    return True



def handle_uploaded_photos(files, dimensions=(400, 400), square=True):
    """ Handles file uploads """

    uploaded_files = []
    errors = []

    for _f in files:
        # try:
        filename = photos.save(_f)
        path = photos.path(filename)

        if check_image_size(path, dimensions, square):
            data = {"name": filename, "filepath": path}
            uploaded_files.append(data)
        else:
            errors.append("%s is not the right dimension" % filename)
        # except Exception as e:
        #     logger.info(e)
        #     errors.append("Uploading %s is not allowed" % _f.filename)

    return uploaded_files, errors




def register_api(cls, *urls, **kwargs):
    """ A simple pass through class to add entities to the api registry """
    kwargs["endpoint"] = getattr(cls, 'resource_name', kwargs.get("endpoint", None))
    app.api_registry.append((cls, urls, kwargs))


@app.before_request
def build_up(exception=None):
    if exception:
        db.session.rollback()
    else:
        db.session.commit()
    db.session.close()
    db.session.remove()


@app.teardown_request
def shutdown_session(exception=None):
    if exception:
        db.session.rollback()
    else:
        db.session.commit()
    db.session.close()
    db.session.remove()


@app.teardown_appcontext
def teardown_db(exception=None):
    if exception:
        db.session.rollback()
    else:
        db.session.commit()
    db.session.close()
    db.session.remove()
