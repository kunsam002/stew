"""
management.py

@Author: Olukunle Ogunmokun
@Date: 11th Oct, 2018

Basic management resources for use with our api
"""
from stew.core.restful import fields, reqparse, to_datetime, to_boolean, ModelField, \
    ModelListField, Timestamp, to_datetime, to_boolean
from stew.core import utils
from stew import uploads
from stew.resources import AppBaseResource, AdminBaseResource
from stew.forms import *
from stew.schemas import *
from stew import register_api, logger
from stew.services.management import *
from stew.models import *
from flask_restful import abort, fields


class UserResource(AdminBaseResource):
    service_class = UserService
    validation_form = UserSchema
    resource_name = "users"

    resource_fields = {
        'username': fields.String,
        'email': fields.String,
        'roles': fields.String,
        'name': fields.String,
        'full_name': fields.String,
        'gender': fields.String,
        'phone': fields.String,
        'date_of_birth': fields.String,
        'is_enabled': fields.Boolean,
        'is_verified': fields.Boolean,
        'is_staff': fields.Boolean,
        'designation': fields.String,
        'residential_address': fields.String,
        'task_count': fields.Integer(default=0),
        'task_entry_count': fields.Integer(default=0),
        'approved_entry_count': fields.Integer(default=0),

        'zone_id': fields.Integer(default=None),
        "zone": fields.Nested({
            "id": fields.Integer,
            "name": fields.String,
            "code": fields.String
        }),
        'community_id': fields.Integer(default=None),
        "community": fields.Nested({
            "id": fields.Integer,
            "name": fields.String,
            "code": fields.String
        }),

    }

    def save(self, attrs, files=None):
        """ Register a user in the system """
        return self.service_class.register_user(**attrs)



