"""
auth.py
@Author: Olukunle Ogunmokun
@Date: 11th Oct, 2018

This module provides api resources to handle authentication.

"""

from flask_restful import Resource, fields, marshal, reqparse, abort
from flask import request, session, g

from stew.models import *
from stew.forms import *
from stew.schemas import *

# After schema imports
from stew import register_api, app, logger
from stew.core.restful import fields
from stew.services import authentication

from stew.core.exceptions import ValidationFailed
from stew.core import utils
from stew.resources import AppBaseResource, AdminBaseResource, require_agent_login
from stew.services.management import UserService


class LoginResource(AppBaseResource):
    service_class = UserService
    resource_name = 'login'

    resource_fields = {
        'username': fields.String,
        'auth_token': fields.String,
        'full_name': fields.String,
        'approx_name': fields.String,
    }

    validation_form = LoginSchema

    def query(self):
        abort(404)

    def save(self, attrs, files=None):
        user = authentication.authenticate(**attrs)
        if user:
            return user
        raise ValidationFailed({"username": ["Username or password is invalid"]})


class ProfileResource(AdminBaseResource):
    service_class = UserService
    resource_name = 'profile'
    resource_fields = {
        'username': fields.String,
        'email': fields.String,
        'phone': fields.String,
        'full_name': fields.String,
        'name': fields.String,
        'approx_name': fields.String,
        'auth_token': fields.String,
        'is_enabled': fields.Boolean,
        'is_verified': fields.Boolean,
        'roles': fields.String,
    }

    upload_validation_form = UploadSchema

    def save(self, attrs, files=None):
        """ override the way save works for updating user profile """
        user_id = self.get_user_id()

        return self.update(user_id, attrs, files=files)

    def get(self, obj_id=None, resource_name=None):
        user_id = self.get_user_id()

        return super(ProfileResource, self).get(obj_id=user_id, resource_name=None)

