from stew.resources.login import *
from stew.resources.management import *
from stew.resources.assets import *


register_api(LoginResource, '/login')
register_api(ProfileResource, '/profile')
register_api(CountryResource, '/countries', '/countries/<string:obj_id>', '/countries/<string:obj_id>/<string:resource_name>')
register_api(StateResource, '/states', '/states/<string:obj_id>', '/states/<string:obj_id>/<string:resource_name>')
register_api(PaymentOptionResource, '/payment-options', '/payment-options/<string:obj_id>',
    '/payment-options/<string:obj_id>/<string:resource_name>')
register_api(DeliveryOptionResource, '/delivery-options', '/delivery-options/<string:obj_id>',
    '/delivery-options/<string:obj_id>/<string:resource_name>')
register_api(PaymentStatusResource, '/payment-statuses', '/payment-statuses/<string:obj_id>',
    '/payment-statuses/<string:obj_id>/<string:resource_name>')
register_api(DeliveryStatusResource, '/delivery-statuses', '/delivery-statuses/<string:obj_id>',
    '/delivery-statuses/<string:obj_id>/<string:resource_name>')
