"""
__init__.py.py

@Author: Olukunle Ogunmokun
@Date: 11th Oct, 2018

"""
from functools import wraps
from flask_restful import fields
from stew.models import *
from stew.core.restful import BaseResource, Timestamp, ModelField
from flask import g, request
from flask_restful import abort
from stew.services.authentication import require_basic_auth, check_basic_auth, check_user_auth
import urllib, json
from stew import app, logger, jwt, models


def require_login(func):
    """ Authentication decorator that handles user login """

    @wraps(func)
    def decorated(*args, **kwargs):
        data = jwt.require_jwt()

        user_id = data.get('id', None)

        if not user_id:
            abort(403, status="Invalid Credentials", message="The provided user credentials are invalid.")

        g.user_id = user_id

        return func(*args, **kwargs)

    return decorated


def require_supervisor_login(func):
    """ Authentication decorator that handles user login """

    @wraps(func)
    def decorated(*args, **kwargs):
        data = jwt.require_jwt()

        user_id = data.get('id', None)
        roles = data.get('rol', '')

        # First check that the user id exists
        if not user_id:
            abort(403, status="Invalid Credentials", message="The provided user credentials are invalid.")
        print("", roles)
        # Then check that the role given to the specified user exists
        if 'supervisor' not in [r.strip() for r in roles.split(",")]:
            abort(403, status="Access Denied",
                  message="The current account does not have the required permission to access this resource")

        g.user_id = user_id

        return func(*args, **kwargs)

    return decorated


def require_admin_login(func):
    """ Authentication decorator that handles user login """

    @wraps(func)
    def decorated(*args, **kwargs):
        data = jwt.require_jwt()

        user_id = data.get('id', None)
        roles = data.get('rol', "")

        # First check that the user id exists
        if not user_id:
            abort(403, status="Invalid Credentials", message="The provided user credentials are invalid.")
        print("", roles)
        # Then check that the role given to the specified user exists
        if 'admin' not in [r.strip() for r in roles.split(",")]:
            abort(403, status="Access Denied",
                  message="The current account does not have the required permission to access this resource")

        g.user_id = user_id

        return func(*args, **kwargs)

    return decorated


def require_agent_login(func):
    """ Authentication decorator that handles user login """

    @wraps(func)
    def decorated(*args, **kwargs):
        data = jwt.require_jwt()

        user_id = data.get('id', None)
        roles = data.get('rol', "")

        # First check that the user id exists
        if not user_id:
            abort(403, status="Invalid Credentials", message="The provided user credentials are invalid.")
        # Then check that the role given to the specified user exists
        roles_array = [r.strip() for r in roles.split(",")]
        rules = [('agent' in roles_array), ('admin' in roles_array), ('super_admin' in roles_array)]
        allow = any(rules)

        if allow is False:
            abort(403, status="Access Denied",
                  message="The current account does not have the required permission to access this resource")

        g.user_id = user_id

        return func(*args, **kwargs)

    return decorated


class AppBaseResource(BaseResource):
    """ The base app class to handle all requests pertaining to the platform """

    # method_decorators = [require_login]
    # mappings_module = mappings
    db_module = models

    @property
    def output_fields(self):
        """ Property function to always generate a clean base value for output fields """
        return {
            'id': fields.Integer,
            'pk': fields.String(default=None),
            'date_created': Timestamp,
            'last_updated': Timestamp,
        }

    # def adjust_form_data(self, data):
    #     """ inject courier_id into the data before persisting """
    #
    #     _courier_id = self.get_courier_id()
    #     data["courier_id"] = _courier_id
    #
    #     return data
    #
    # def search_filters(self):
    #     """default search filter. should be typically overwritten in subclass"""
    #     return Q("match", courier_id=self.get_courier_id())
    #
    # def search_filter_queries(self):
    #     return [Q("term", courier_id=self.get_courier_id())]

    @staticmethod
    def get_user_id():
        _user_id = getattr(g, "user_id", None)

        if not _user_id:
            abort(401, status="Invalid credentials",
                  message="the user credentials provided is invalid for this resource")

        return _user_id

    @staticmethod
    def get_user():

        _user_id = getattr(g, "user_id", None)

        if not _user_id:
            abort(401, status="Invalid credentials",
                  message="the user credentials provided is invalid for this resource")

        return User.query.get(_user_id)

    def limit_query(self, query):
        """ Limit the query by the courier_id as well as role/user management here"""
        return query

    def is_permitted(self, obj=None, **kwargs):
        """ implement permission management here """
        return obj


class AdminBaseResource(AppBaseResource):
    method_decorators = [require_admin_login]

    @property
    def output_fields(self):
        """ Property function to always generate a clean base value for output fields """
        return {
            'pk': fields.String(default=None),
            'id': fields.Integer(default=None),
            'user_id': fields.String(default=None),
            'date_created': Timestamp,
            'last_updated': Timestamp,
        }

    def adjust_form_data(self, data={}):
        """ inject user_id into the data before persisting """
        data = {} if data is None else data

        _user_id = self.get_user_id()
        data["user_id"] = _user_id

        return data

    # def search_filters(self):
    #     """default search filter. should be typically overwritten in subclass"""
    #     return Q("match", user_id=self.get_user_id())
    #
    # def search_filter_queries(self):
    #     return [Q("term", user_id=self.get_user_id())]


    def get_user_id(self):
        _user_id = getattr(g, "user_id", None)

        if not _user_id:
            abort(403, status="Invalid credentials",
                  message="the user credentials provided is invalid for this resource")

        return _user_id

    def limit_query(self, query):
        """ Limit the query by the user_id as well as role/user management here"""
        return query

    def is_permitted(self, obj=None, **kwargs):
        """ implement permission management here """
        return obj


class AgentBaseResource(AppBaseResource):
    method_decorators = [require_agent_login]

    @property
    def output_fields(self):
        """ Property function to always generate a clean base value for output fields """
        return {
            'pk': fields.String(default=None),
            'id': fields.Integer(default=None),
            'user_id': fields.String(default=None),
            'date_created': Timestamp,
            'last_updated': Timestamp,
        }

    def adjust_form_data(self, data={}):
        """ inject user_id into the data before persisting """
        data = {} if data is None else data

        _user_id = self.get_user_id()
        data["user_id"] = _user_id

        return data

    # def search_filters(self):
    #     """default search filter. should be typically overwritten in subclass"""
    #     return Q("match", user_id=self.get_user_id())
    #
    # def search_filter_queries(self):
    #     return [Q("term", user_id=self.get_user_id())]


    def get_user_id(self):
        _user_id = getattr(g, "user_id", None)

        print("This is me getting the user ID", _user_id)

        if not _user_id:
            abort(403, status="Invalid credentials",
                  message="the user credentials provided is invalid for this resource")

        return _user_id

    def limit_query(self, query):
        """ Limit the query by the user_id as well as role/user management here"""
        return query

    def is_permitted(self, obj=None, **kwargs):
        """ implement permission management here """
        return obj
