"""
assets.py

@Author: Olukunle Ogunmokun
@Date: 4th Oct, 2018

Basic assets resources for use with our api
"""
from stew.core.restful import fields, reqparse, to_datetime, to_boolean, ModelField, ModelListField, Timestamp, to_datetime, to_boolean
from stew.resources import AppBaseResource, AdminBaseResource, require_admin_login
from stew.core import utils
from stew.forms import *
from stew.schemas import *
from stew import register_api
from stew.services.assets import *
from stew.services.management import UserService
from stew.models import *
from flask_restful import abort, fields



class CountryResource(AppBaseResource):
    service_class = CountryService
    resource_name = 'countries'
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'phone_code': fields.String,
        'enabled': fields.Boolean,
        'slug': fields.String
    }

    def query(self):
        return Country.query.filter(Country.code=="NG")


class StateResource(AppBaseResource):
    service_class = StateService
    resource_name = 'states'
    validation_form = StateSchema
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'numeric_code': fields.String,
        'country_code': fields.String,
        'slug': fields.String,
        'country': fields.Nested({
            'name': fields.String,
            'code': fields.String
        })
    }

    def query(self):
        return State.query.filter(State.country_code=="NG")



class DeliveryOptionResource(AppBaseResource):
    service_class = DeliveryOptionService
    resource_name = 'delivery-options'
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'description': fields.String
    }

    validation_form = DeliveryOptionForm

    def adjust_form_fields(self, form):
        form.discount_type.choices = [('flat', 'Flat'), ('percentage', 'Percentage')]
        return form


class PaymentOptionResource(AppBaseResource):
    service_class = PaymentOptionService
    resource_name = 'payment-options'
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'description': fields.String
    }

    validation_form = PaymentOptionForm


class PaymentStatusResource(AppBaseResource):
    service_class = PaymentStatusService
    resource_name = 'payment-statuses'
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'description': fields.String
    }

    validation_form = PaymentStatusForm


class DeliveryStatusResource(AppBaseResource):
    service_class = DeliveryStatusService
    resource_name = 'delivery-statuses'
    resource_fields = {
        'name': fields.String,
        'code': fields.String,
        'description': fields.String
    }

    validation_form = DeliveryStatusForm


