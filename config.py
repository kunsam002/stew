"""
config.py

The file that will be used to specify each flask application's config
"""
import os
import socket
from celery.schedules import crontab
from kombu import Queue, Exchange

"""
config.py

Flask configuration script

"""

import os
from celery.schedules import crontab


class Config(object):
    """ Default config object with specific parameters for flask. """

    DEBUG = True
    ASSETS_DEBUG = DEBUG
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = "postgresql://postgres:postgres@localhost/stew"
    SECRET_KEY = '\x91c~\xc0-\xe3\'f\xe19PxE\x93\xe8\x91`usu"\xd0\xb6\x01/\x0c\xed\\\xbd]nGtvH\x99ekw\xf8'
    SQLALCHEMY_ECHO = False

    SYSTEM_USERNAME = "admin"

    JWT_KEY = SECRET_KEY

    REPORT_DIR = os.path.join(os.path.dirname(os.path.abspath(__name__)), "reports")
    SETUP_DIR = os.path.join(os.path.dirname(os.path.abspath(__name__)), "setup")
    STATIC_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__name__)), "stew", "static")
    UPLOADS_DEFAULT_DEST = UPLOADS_DIR = os.path.join(os.path.dirname(os.path.abspath(__name__)), "uploads")

    # redis
    REDIS_URL = 'redis://localhost:6379'
    CACHE_TYPE = 'redis'
    CACHE_REDIS_URL = 'redis://localhost:6379'

    RAVE_PUB_KEY = "FLWPUBK_TEST-12eb26a6e57e36e240adb5fe3cbe5595-X"
    RAVE_SECRET_KEY = "FLWSECK_TEST-28d209ea211aa301be7e40a9e3eed395-X"

    SUBSCRIPTION_COMPLETE = False

    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'stewnigeria1@gmail.com'
    MAIL_PASSWORD = '07cm06461'

    SENDGRID_API_KEY = "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"

    DOMAIN = 'stew.ng'
    PROTOCOL = "http://"
    CANONICAL_URL = "%s%s" % (PROTOCOL, DOMAIN)

    CONTACT_EMAIL = "info@stew.ng"
    CONTACT_PHONE = dict(value="+2348136647819", display="+234.813.664.7819")

    # Imagekit Creds
    IMAGEKIT_API_KEY = "qa/5wi4I2MhGntQPznU8BNr7/nA="  # required
    IMAGEKIT_API_SECRET = "RoqRwL6dbeiWVJRPKjM7HevfNbQ="  # required and to be kept secret
    IMAGEKIT_ID = "stewng"  # required


class SiteDevConfig(Config):
    """ Default config object with specific parameters for flask. """
    pass


class ProdConfig(Config):
    """ Default config object with specific parameters for flask. """
    DEBUG = False
    ASSETS_DEBUG = False

    DOMAIN = 'stew.ng'
    PROTOCOL = "http://"
    CANONICAL_URL = "%s%s" % (PROTOCOL, DOMAIN)

    SENDGRID_API_KEY = "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"

    RAVE_PUB_KEY = "FLWPUBK-15057971eebbdd53711e5de7cea0a8e1-X"
    RAVE_SECRET_KEY = "FLWSECK-c9da6922af71a8ab69c99561db8c2bd1-X"

    CONTACT_EMAIL = "info@stew.ng"
