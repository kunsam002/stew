import os
import socket
from factories import create_app, initialize_api, initialize_blueprints

FLASK_CONFIG = os.getenv("FLASK_CONFIG", "config.Config")
FLASK_HOST = os.getenv("FLASK_HOST", "0.0.0.0")
FLASK_PORT = os.getenv("PORT", 5000)

app = create_app('stew', FLASK_CONFIG)

with app.app_context():
    from stew.views.public import www
    from stew.resources import endpoints
    from stew import api
    from stew.subscribers import notifications

    initialize_api(app, api)

    initialize_blueprints(app, www)

if __name__ == "__main__":
    app.run(host=FLASK_HOST, port=FLASK_PORT)
