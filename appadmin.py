import os
import socket
from factories import create_app, initialize_api, initialize_blueprints

FLASK_CONFIG = os.getenv("FLASK_CONFIG", "config.Config")
FLASK_HOST = os.getenv("FLASK_HOST", "0.0.0.0")
FLASK_PORT = os.getenv("PORT", 5050)

app = create_app('stew', FLASK_CONFIG)

with app.app_context():
    from stew.views.admin import control
    from stew import api

    initialize_api(app, api)

    initialize_blueprints(app, control)

if __name__ == "__main__":
    app.run(host=FLASK_HOST, port=FLASK_PORT)
