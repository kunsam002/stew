"""
factories.py

@Author: Olukunle Ogunmokun
@Date: 10th Dec, 2018

This module contains application factories.
"""

import os
from flask import current_app

from celery import Celery
from flask import Flask, send_from_directory
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
from flask_uploads import UploadSet, IMAGES, ARCHIVES, SCRIPTS, EXECUTABLES, AllExcept, ALL, configure_uploads, \
    patch_request_class
import redis
from flask_restful import Api
from flask_alembic import Alembic
from flask_caching import Cache
from flask_login import LoginManager
from flask_principal import Principal
import jwt
from flask import request, g, make_response, url_for, current_app
from flask_restful import abort
from functools import wraps
import json
import pprint
import tablib
from flask_assets import Environment
from assets_bundle import bundles
from flask_mail import Mail
from werkzeug.exceptions import HTTPException
import bugsnag
from bugsnag.flask import handle_exceptions
from bugsnag.handlers import BugsnagHandler

from sendgrid import SendGridAPIClient
from imagekitio.client import Imagekit

# Configure Bugsnag
bugsnag.configure(
    api_key="b6a14235d06f39f0165178d235ebafff",
    auto_capture_sessions=True,
    project_root=os.path.join(os.path.dirname(os.path.abspath(__name__)))
)

# custom status codes

VALIDATION_FAILED = 409


class ValidationFailed(HTTPException):
    """
    *34* `Further Action Exception`
    Custom exception thrown when further action is required by the user.
    This is only useful when making REST api calls
    """

    name = "Further Action Required"
    code = VALIDATION_FAILED
    description = (
        '<p>Further Action Required</p>'
    )

    def __init__(self, data):
        super(ValidationFailed, self).__init__()
        self.data = data
        self.status = 409


def initialize_api(app, api):
    """ Register all resources for the API """
    api.init_app(app=app)  # Initialize api first
    _resources = getattr(app, "api_registry", None)
    if _resources and isinstance(_resources, (list, tuple,)):
        for cls, args, kwargs in _resources:
            api.add_resource(cls, *args, **kwargs)


def initialize_blueprints(app, *blueprints):
    """Registers a set of blueprints to an application"""

    for blueprint in blueprints:
        app.register_blueprint(blueprint)


def make_celery(app):
    """ make celery inside the application possible """
    celery = Celery(app.import_name, backend=app.config.get("CELERY_RESULT_BACKEND"),
                    broker=app.config.get("CELERY_BROKER_URL"))

    celery.conf.update(app.config)
    task_base = celery.Task

    class ContextTask(task_base):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return task_base.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def apply_static_rule(app):
    """ Apply the static url endpoint based on the config file """

    static_folder = app.config.get("STATIC_FOLDER", os.path.dirname(os.path.abspath(__name__)))
    static_url_path = app.config.get("STATIC_URL_PATH", "/static")

    app.static_folder = static_folder
    app.static_url_path = static_url_path

    # rule to append to the app
    def fetch_from_static(path):
        return app.send_from_directory(static_folder, path)

    # app.add_url_rule('/static/<path:path>', 'static', fetch_from_static)

    return app


def apply_download_rule(app):
    """ Apply the download url endpoint based on the config file """

    downloads_folder = app.config.get("DOWNLOADS_FOLDER", os.path.dirname(os.path.abspath(__name__)))

    app.downloads_folder = downloads_folder

    # app.download_url_path = download_url_path
    # rule to append to the app
    def fetch_from_download(path):
        return send_from_directory(downloads_folder, path)

    app.add_url_rule('/download/<path:path>', 'download', fetch_from_download)

    return app


class StewJWT:
    """the custom JWT object to be used by all apps for authentication and authorization
    within the stew eco-system"""

    def __init__(self, app, ref=None, model=None):
        self.app = app if app else current_app
        self.ref = ref if ref else self.app.config.get("JWT_REF", None)
        self.permission = self.app.config.get("JWT_PERMISSION", None)

    def encode(self, data, algorithm='HS256'):
        """ wrapper for the encode function in JWT package"""
        key = self.app.config.get('JWT_KEY')
        token = jwt.encode(data, key, algorithm=algorithm)

        return token

    def decode(self, token, algorithm=None):
        """ wrapper for the decode function in JWT package"""
        key = self.app.config.get('JWT_KEY')
        data = jwt.decode(token, key, algorithm=algorithm)

        return data

    def require_jwt(self):
        token = request.headers.get("authorization", None)
        print(token)
        try:
            data = jwt.decode(token, self.app.config.get("JWT_KEY"))
            print(data, "==data==")
        except Exception as e:
            print(e.message)
            raise ValidationFailed({"status": "Failed", "message": e.message})

        return data

    def check_permission(self, permisson_name, permissions, **kwargs):
        """verify if a user has a required permission"""

        permission = permissions.get(permisson_name, None)

        if permission:
            return True
        return False


def require_basic_auth(realm="stew API"):
    """ Sends a 401 Authorization required response that enables basic auth """

    message = "Could not authorize your request. Provide the proper login credentials to continue."

    headers = {"WWW-Authenticate": "Basic realm='%s'" % realm}
    status = 401

    return message, status, headers  # body, status and headers


class ReportGen:
    def __init__(self, app=None, report_dir=None):
        self.app = app if app else current_app
        self.report_dir = report_dir if report_dir else self.app.config.get("REPORTS_DIRECTORY", report_dir)

    # For multiple worksheets pass the datasets to DataBook like tablib.Databook((dataset1, dataset2))
    def prepare_data(cls, headers, *raw_data, **kwargs):
        sheet_name = kwargs.get("sheet_name", None)
        if len(raw_data) > 0 and type(raw_data[0]) is dict:
            if sheet_name:
                data = tablib.Dataset(title=sheet_name)
            else:
                data = tablib.Dataset()
            data.dict = raw_data
        elif len(raw_data) > 0 and type(raw_data[0]) is tuple:
            if sheet_name:
                data = tablib.Dataset(*raw_data, headers=headers, title=sheet_name)
            else:
                data = tablib.Dataset(*raw_data, headers=headers)
        else:
            if sheet_name:
                data = tablib.Dataset(title=sheet_name)
            else:
                data = tablib.Dataset()

        return data

    def download_csv(self, filename, headers, *data):

        data = self.prepare_data(headers, *data)
        fullpath = "%s/%s.csv" % (self.report_dir, filename)
        with open(fullpath, 'wb') as f:
            f.write(data.csv)

        return data.csv

    def download_html(self, filename, headers, *data):
        data = self.prepare_data(headers, *data)
        return data.html

    def download_json(self, filename, headers, *data):
        data = self.prepare_data(headers, *data)
        fullpath = "%s/%s.json" % (self.report_dir, filename)
        with open(fullpath, 'wb') as f:
            f.write(data.json)
        return data.json

    def download_xlsx(self, filename, headers, *data, **kwargs):
        data = self.prepare_data(headers, *data, **kwargs)

        fullpath = "%s/%s.xlsx" % (self.report_dir, filename)
        with open(fullpath, 'wb') as f:
            f.write(data.xlsx)

        return data.xlsx

    def download_xls(self, filename, headers, *data, **kwargs):
        data = self.prepare_data(headers, *data, **kwargs)

        fullpath = "%s/%s.xls" % (self.report_dir, filename)
        with open(fullpath, 'wb') as f:
            f.write(data.xls)

        return data.xls

    def download_workbook(self, filename, data):
        book = tablib.Databook(data)
        fullpath = "%s/%s.xlsx" % (self.report_dir, filename)
        with open(fullpath, 'wb') as f:
            f.write(book.xlsx)

        return book.xlsx

    def prepare_excel_data(self, path):
        imported_data = tablib.Dataset().load(open(path).read())
        json_string = imported_data.export('json')
        json_data = json.loads(json_string)
        return json_data


def create_app(app_name, config_obj, api_prefix='/api/v1'):
    """ Generates and configures the main shop application. All additional """

    # API application
    # app = Eve(name, settings=api_settings)

    # Launching application
    app = Flask(app_name)  # So the engine would recognize the root package

    # Load Configuration
    app.config.from_object(config_obj)

    # Loading assets
    assets = Environment(app)
    # assets.from_yaml('assets.yaml')
    assets.register(bundles)
    app.assets = assets

    # Initializing bcrypt password encryption
    bcrypt = Bcrypt(app)
    app.bcrypt = bcrypt

    # Initializing Database
    db = SQLAlchemy(app)
    app.db = db

    app.mail = Mail(app)
    app.sg = SendGridAPIClient(app.config.get("SENDGRID_API_KEY"))

    app.report_gen = ReportGen(app)

    # Initializing login manager
    login_manager = LoginManager()
    login_manager.login_view = app.config.get('LOGIN_VIEW', '.login')
    login_manager.login_message = 'You need to be logged in to access this page'
    login_manager.session_protection = 'strong'
    login_manager.init_app(app)
    app.login_manager = login_manager

    # Initializing principal manager
    app.principal = Principal(app)

    # Initializing Alembic
    alembic = Alembic()
    alembic.init_app(app)
    app.alembic = alembic

    photos = UploadSet('photos', IMAGES)
    archives = UploadSet('archives', ARCHIVES)

    configure_uploads(app, (photos, archives))

    patch_request_class(app, 16 * 1024 * 1024)  # Patches to 16MB file uploads max.

    app.photos = photos
    app.archives = archives

    # ImageKit
    app.imagekit = Imagekit({"api_key": app.config.get("IMAGEKIT_API_KEY"),
                       "api_secret": app.config.get("IMAGEKIT_API_SECRET"),
                       "imagekit_id": app.config.get("IMAGEKIT_ID"),
                       "use_subdomain": True,
                       "use_secure": True})

    # Caching
    app.cache = Cache(app)

    # Redis
    app.redis = redis.StrictRedis().from_url(app.config.get('REDIS_URL'))

    uploads = UploadSet('uploads', AllExcept(SCRIPTS + EXECUTABLES))
    # configure_uploads(app, uploads)

    patch_request_class(app, 16 * 1024 * 1024)  # Patches to 2MB file uploads max.

    app.uploads = uploads

    app.jwt = StewJWT(app)

    # fixing links for static files
    app = apply_static_rule(app)
    app = apply_download_rule(app)

    # inject celery into the app
    app.celery = make_celery(app)

    api = Api(app, prefix=api_prefix)
    app.api = api

    # Initialize Logging
    if not app.debug:
        import logging
        from logging.handlers import RotatingFileHandler
        file_handler = RotatingFileHandler("/var/log/%s/%s.log" % (app_name, app.config.get("LOGFILE_NAME", app_name)),
                                           maxBytes=500 * 1024)
        file_handler.setLevel(logging.ERROR)
        from logging import Formatter
        file_handler.setFormatter(Formatter(
            '%(asctime)s %(levelname)s: %(message)s '
            '[in %(pathname)s:%(lineno)d]'
        ))
        app.logger.addHandler(file_handler)

        logger = logging.getLogger("test.logger")
        handler = BugsnagHandler()
        # send only ERROR-level logs and above
        handler.setLevel(logging.ERROR)
        logger.addHandler(handler)

    # include an api_registry to the application
    app.api_registry = []  # a simple list holding the values to be registered

    return app
